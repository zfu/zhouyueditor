//
//  Data.h
//  ZhouYuEditor
//
//  Created by zfu on 14/8/25.
//
//

#ifndef __ZhouYuEditor__Data__
#define __ZhouYuEditor__Data__
#include "ConfigINI.h"

#include <stdio.h>
#include <sys/stat.h>

class ActorData
{
public:
    string dName;
    string dName2;
    string resName;
    string wName;
    string battleResName;
    typedef enum{
        kWeaponLight=0,
        kWeaponFar,
        kWeaponLong,
        kWeaponHeavy,
        kWeaponDouble
    }WeaponType;
    WeaponType weapontype;
    unsigned char I;//智力
    unsigned char S;//武力
    unsigned char L;//指挥
    unsigned char A;//敏捷
    int index;
    
    //skill
    bool hasSk;
    string skName;
    string skComment;
    int skMin;
    int skMax;
    int skTime;
    string skDelay;
    string magic;
    string hpMax;
    
    //offset
    int Xoffset[8];
    int Yoffset[8];
};

class NPCData
{
public:
    string dName;
    string resName;
    string scriptName;
};

class ItemData
{
public:
    string dName;
    string comment;
    typedef enum{
        kTypeEquip,
        kTypeSupply,
        kTypeAttack,
        kTypeOther
    }ItemType;
    typedef enum{
        kEquipHat=0,
        kEquipGown,
        kEquipShoe,
        kEquipShield,
        kEquipHorse,
        kEquipSpecial,
        kInput,
        kOutput
    }EquipType;
    typedef enum{
        kWeaponLight,
        kWeaponHeavy,
        kWeaponLong,
        kWeaponLightLong
    }WeaponType;
    typedef enum{
        kEnvBattle,
        kEnvNoBattle,
        kEnvAll,
        kEnvEquip
    }ItemEnv;
    ItemEnv env;
    ItemType itemType;
    EquipType itemEquipType;
    WeaponType weaponType;
    bool supplyAll;
    bool attackAll;
    
    //equip
    int equipAttack;
    int equipDefend;
    int equipI;
    int equipS;
    int equipL;
    int equipA;
    int equipNeedI;
    int equipNeedS;
    int equipNeedL;
    int equipNeedA;
    
    //supply
    int supplyHP;
    int supplyHPRound;
    int supplyMP;
    int supplyMPRound;
    
    //attack
    int attackHP;
    int attackHPRound;
    int price;
};

class subStory {
public:
    string comment;
};

class segment {
public:
    string title;
    int start;
    int end;
};

class chapter {
public:
    string chapterName;
    string chapterComment;
    int segmentSize;
    vector<segment> _segments;
};

class Magic{
public:
    typedef enum {
        kMagic,
        kArray
    }MType;
    MType type;
    
    typedef enum{
        kMagicHeroOne=0,//我方单体
        kMagicHeroAll=1,//我方全体
        kMagicEnemyOne=2,//敌方单体
        kMagicEnemyAll=3//敌方全体
    }MagicTarget;
    MagicTarget target;
    
    typedef enum {
        kMagicWater,//水
        kMagicFire,//火
        kMagicEarth,//石
        kMagicWalk,//行军
        kMagicRun,//撤退
        kMagicAddition,//辅助all
        kMagicDecrease//减少辅助
    }MagicType;
    MagicType magicType;
    
    typedef enum {
        kEnvAll,
        kEnvMap,
        kEnvBattle
    }MagicEnv;
    MagicEnv env;
    
    string dName;//名称
    string comment;//附注
    string O;//输出伤害
    string OM;
    string D;//防御
    string DM;
    string A;//回避
    string AM;
    string F;//命中
    string FM;
    string AD;//攻击回避
    string ADM;
    string SD;//策略回避
    string SDM;
    int O_[5],OM_[5],D_[5],DM_[5],A_[5],AM_[5],F_[5],FM_[5],AD_[5],ADM_[5],SD_[5],SDM_[5];
    //string position;//阵型位置
    int pos[5];
    
    int HPp;
    int round;
    int roundM;
    int mp;
};

class Battle
{
public:
    string dName;
    //string enemies;
    //string level;
    string script;
    string comment;
    int enemies_[5];
    int level_[5];
};

class Map
{
public:
    string name;
    string resName;
    string comment;
};

class Data
{
public:
    string _path;
    string _connect;
    //actor
    vector<ActorData> _ActorDatas;
    ConfigINI *actorINI;
    void loadActor();
    void storeActor();
    
    vector<NPCData> _NPCDatas;
    ConfigINI *npcINI;
    void loadNPC();
    void storeNPC();
    
    vector<ItemData> _ItemDatas;
    ConfigINI *itemINI;
    void loadItem();
    void storeItem();
    
    ConfigINI *storyINI;
    string storyName;
    string storyComment;
    vector<chapter> _chapters;
    vector<subStory> _stories;
    void loadStory();
    void storeStory();
    
    ConfigINI *magicINI;
    vector<Magic> _magics;
    void loadMagic();
    void storeMagic();
    
    ConfigINI *mapINI;
    vector<Map> _maps;
    void loadMaps();
    void storeMaps();
    
    ConfigINI *battleINI;
    vector<Battle> _battles;
    void loadBattle();
    void storeBattle();
    
    bool reLoad()
    {
        struct stat info;
        int ret = stat((_path+_connect+"actors.ini").c_str(), &info);
        if (ret !=0){
            cout<<"file not exist"<<endl;
            return false;
        }
        if (actorINI!=NULL){freeLoad();}
        cout<<"load everthing from["<<_path<<"]"<<endl;
        actorINI = new ConfigINI((_path+_connect+"actors.ini").c_str());
        loadActor();                    
        npcINI = new ConfigINI((_path+_connect+"npc.ini").c_str());
        loadNPC();                    
        itemINI = new ConfigINI((_path+_connect+"items.ini").c_str());
        loadItem();                    
        storyINI = new ConfigINI((_path+_connect+"drama.ini").c_str());
        loadStory();                    
        magicINI = new ConfigINI((_path+_connect+"magic.ini").c_str());
        loadMagic();                    
        mapINI = new ConfigINI((_path+_connect+"mapName_cn.ini").c_str());
        loadMaps();                   
        battleINI = new ConfigINI((_path+_connect+"battleData.ini").c_str());
        loadBattle();                    
        return true;
    }
    void freeLoad()
    {
        cout<<"free every thing"<<endl;
        delete actorINI;
        actorINI=NULL;
        delete npcINI;
        npcINI=NULL;
        delete itemINI;
        itemINI=NULL;
        delete storyINI;
        storyINI=NULL;
        delete magicINI;
        magicINI=NULL;
        delete mapINI;
        mapINI=NULL;
        delete battleINI;
        battleINI=NULL;
    }
    //Data():_path("~/proj/cocos2dx_proj/proj_beta/zhouyu/Resources")
    Data():_path(""),
    actorINI(NULL),
    npcINI(NULL),
    itemINI(NULL),
    storyINI(NULL),
    magicINI(NULL),
    mapINI(NULL),
    battleINI(NULL)
    {
#if defined(Q_OS_MAC) || defined(Q_OS_DARWIN)
        cout<<"mac"<<endl;
        _path="/Users/zfu/Desktop/zhouyuData";
        _connect="/";
#else          
        cout<<"windows"<<endl;
        _path="..\\zhouyueditor\\test_resources";
        _connect="\\";
#endif
        //reLoad();
    }
    ~Data()
    {
        freeLoad();
    }
    void getMM(int *arr, string &str);
    void setMM(int *arr, string &str);
    void saveData();
private:
    void split(std::string& s, std::string delim,std::vector<std::string >* ret);
};
#endif /* defined(__ZhouYuEditor__Data__) */

/********************************************************************************
** Form generated from reading UI file 'ZhouYuEditor.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZHOUYUEDITOR_H
#define UI_ZHOUYUEDITOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ZhouYuEditor
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_72;
    QLineEdit *pathEdit;
    QPushButton *selectDirButton;
    QPushButton *loadButton;
    QPushButton *saveButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_4;
    QTableWidget *actorTableWidget;
    QWidget *tab_actor;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_18;
    QPushButton *actorAddButton;
    QPushButton *actorDelButton;
    QListWidget *actorListWidget;
    QHBoxLayout *horizontalLayout_19;
    QPushButton *actorUpButton;
    QPushButton *actorDownButton;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_4;
    QLineEdit *actorNameEdit;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QLineEdit *actorName2Edit;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_2;
    QLineEdit *actorResName;
    QHBoxLayout *horizontalLayout_90;
    QLabel *label_91;
    QLineEdit *actorBattleResName;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_3;
    QLineEdit *actorEquipName;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_5;
    QComboBox *actorWeaponType;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_6;
    QSlider *actorISlider;
    QLabel *actorI;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_7;
    QSlider *actorSSlider;
    QLabel *actorS;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_8;
    QSlider *actorLSlider;
    QLabel *actorL;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_9;
    QSlider *actorASlider;
    QLabel *actorA;
    QVBoxLayout *verticalLayout_8;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_9;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_20;
    QLabel *label_19;
    QLabel *label_21;
    QHBoxLayout *horizontalLayout_16;
    QComboBox *actorOffset;
    QLineEdit *actorOffsetX;
    QLineEdit *actorOffsetY;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_7;
    QCheckBox *actorHasSkill;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_14;
    QLineEdit *actorSPName;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_15;
    QSpacerItem *horizontalSpacer;
    QLineEdit *actorSPMin;
    QLabel *label_10;
    QLineEdit *actorSPMax;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_16;
    QSpacerItem *horizontalSpacer_6;
    QLineEdit *actorAttackTimes;
    QHBoxLayout *horizontalLayout_93;
    QLabel *label_92;
    QLineEdit *actorSPDelay;
    QHBoxLayout *horizontalLayout_91;
    QLabel *label_17;
    QPlainTextEdit *actorSPComment;
    QWidget *tab_npc;
    QHBoxLayout *horizontalLayout_23;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_21;
    QPushButton *npcAddButton;
    QPushButton *npcDelButton;
    QListWidget *npcListWidget;
    QHBoxLayout *horizontalLayout_22;
    QPushButton *npcUpButton;
    QPushButton *npcDownButton;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_12;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_14;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_23;
    QLineEdit *npcName;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_24;
    QLineEdit *npcResName;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_25;
    QLineEdit *npcScriptName;
    QSpacerItem *verticalSpacer_8;
    QSpacerItem *horizontalSpacer_2;
    QWidget *tab_Item;
    QHBoxLayout *horizontalLayout_29;
    QVBoxLayout *verticalLayout_15;
    QHBoxLayout *horizontalLayout_27;
    QPushButton *itemAddButton;
    QPushButton *itemDelButton;
    QListWidget *itemListWidget;
    QHBoxLayout *horizontalLayout_28;
    QPushButton *itemUpButton;
    QPushButton *itemDownButton;
    QVBoxLayout *verticalLayout_17;
    QHBoxLayout *horizontalLayout;
    QLabel *label_70;
    QLineEdit *itemName;
    QLabel *label_81;
    QComboBox *itemEnv;
    QHBoxLayout *horizontalLayout_92;
    QLabel *label_41;
    QTextEdit *itemComment;
    QHBoxLayout *horizontalLayout_88;
    QLabel *label_85;
    QLineEdit *itemPrice;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_18;
    QRadioButton *equipRadio;
    QGroupBox *groupBox_8;
    QVBoxLayout *verticalLayout_19;
    QHBoxLayout *horizontalLayout_30;
    QLabel *label_26;
    QComboBox *itemType;
    QHBoxLayout *horizontalLayout_31;
    QLabel *label_27;
    QLineEdit *equipAttack;
    QHBoxLayout *horizontalLayout_32;
    QLabel *label_28;
    QLineEdit *equipDefend;
    QHBoxLayout *horizontalLayout_86;
    QLabel *label_82;
    QLineEdit *equipI;
    QHBoxLayout *horizontalLayout_87;
    QLabel *label_83;
    QLineEdit *equipS;
    QHBoxLayout *horizontalLayout_34;
    QLabel *label_30;
    QLineEdit *equipL;
    QHBoxLayout *horizontalLayout_33;
    QLabel *label_29;
    QLineEdit *equipA;
    QLabel *label_84;
    QHBoxLayout *horizontalLayout_35;
    QLabel *label_31;
    QLineEdit *equipNeedI;
    QHBoxLayout *horizontalLayout_36;
    QLabel *label_32;
    QLineEdit *equipNeedS;
    QHBoxLayout *horizontalLayout_37;
    QLabel *label_33;
    QLineEdit *equipNeedL;
    QHBoxLayout *horizontalLayout_38;
    QLabel *label_34;
    QLineEdit *equipNeedA;
    QSpacerItem *verticalSpacer_5;
    QVBoxLayout *verticalLayout_16;
    QRadioButton *supplyRadio;
    QGroupBox *groupBox_7;
    QVBoxLayout *verticalLayout_21;
    QCheckBox *supplyCheckBox;
    QHBoxLayout *horizontalLayout_39;
    QLabel *label_35;
    QLineEdit *supplyHP;
    QLabel *label_37;
    QLineEdit *supplyHPRound;
    QHBoxLayout *horizontalLayout_40;
    QLabel *label_36;
    QLineEdit *supplyMP;
    QLabel *label_38;
    QLineEdit *supplyMPRound;
    QRadioButton *attackRadio;
    QGroupBox *groupBox_6;
    QVBoxLayout *verticalLayout_22;
    QCheckBox *attackCheckBox;
    QHBoxLayout *horizontalLayout_41;
    QLabel *label_39;
    QLineEdit *attackHP;
    QLabel *label_40;
    QLineEdit *attackHPRound;
    QRadioButton *otherRadio;
    QGroupBox *groupBox_16;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *horizontalSpacer_3;
    QWidget *tab_story;
    QHBoxLayout *horizontalLayout_54;
    QVBoxLayout *verticalLayout_29;
    QGroupBox *groupBox_12;
    QVBoxLayout *verticalLayout_30;
    QHBoxLayout *horizontalLayout_56;
    QLabel *label_50;
    QLineEdit *storyName;
    QHBoxLayout *horizontalLayout_57;
    QLabel *label_51;
    QTextEdit *storyComment;
    QHBoxLayout *horizontalLayout_59;
    QVBoxLayout *verticalLayout_31;
    QHBoxLayout *horizontalLayout_58;
    QPushButton *chapterAddButton;
    QPushButton *chapterDelButton;
    QListWidget *chapterListWidget;
    QHBoxLayout *horizontalLayout_55;
    QPushButton *chapterUpButton;
    QPushButton *chapterDownButton;
    QGroupBox *groupBox_14;
    QVBoxLayout *verticalLayout_32;
    QHBoxLayout *horizontalLayout_62;
    QLabel *label_54;
    QLineEdit *chapterName;
    QLabel *label_53;
    QTextEdit *chapterComment;
    QVBoxLayout *verticalLayout_13;
    QHBoxLayout *horizontalLayout_77;
    QVBoxLayout *verticalLayout_38;
    QHBoxLayout *horizontalLayout_79;
    QPushButton *segmentAddButton;
    QPushButton *segmentDelButton;
    QListWidget *segmentListWidget;
    QHBoxLayout *horizontalLayout_78;
    QPushButton *segmentUpButton;
    QPushButton *segmentDownButton;
    QGroupBox *groupBox_15;
    QVBoxLayout *verticalLayout_39;
    QHBoxLayout *horizontalLayout_61;
    QLabel *label_11;
    QLineEdit *segmentName;
    QHBoxLayout *horizontalLayout_80;
    QLabel *label_12;
    QLineEdit *segmentStart;
    QHBoxLayout *horizontalLayout_81;
    QLabel *label_13;
    QLineEdit *segmentEnd;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_20;
    QVBoxLayout *verticalLayout_28;
    QLabel *label_18;
    QHBoxLayout *horizontalLayout_52;
    QPushButton *storyAddButton;
    QPushButton *storyDelButton;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *storyUpButton;
    QPushButton *storyDownButton;
    QListWidget *subStoryListWidget;
    QHBoxLayout *horizontalLayout_53;
    QLabel *label_22;
    QLineEdit *subStoryDetail;
    QWidget *tab_skill;
    QHBoxLayout *horizontalLayout_44;
    QVBoxLayout *verticalLayout_23;
    QHBoxLayout *horizontalLayout_42;
    QPushButton *magicAddButton;
    QPushButton *magicDelButton;
    QListWidget *magicListWidget;
    QHBoxLayout *horizontalLayout_43;
    QPushButton *magicUpButton;
    QPushButton *magicDownButton;
    QVBoxLayout *verticalLayout_25;
    QGroupBox *groupBox_13;
    QVBoxLayout *verticalLayout_33;
    QHBoxLayout *horizontalLayout_47;
    QLabel *label_71;
    QLineEdit *magicName;
    QHBoxLayout *horizontalLayout_50;
    QLabel *label_47;
    QLineEdit *magicMP;
    QLabel *label_42;
    QTextEdit *magicComment;
    QSpacerItem *verticalSpacer_7;
    QVBoxLayout *verticalLayout_24;
    QRadioButton *magicRadio;
    QGroupBox *groupBox_10;
    QVBoxLayout *verticalLayout_26;
    QHBoxLayout *horizontalLayout_45;
    QLabel *label_43;
    QComboBox *magicTarget;
    QHBoxLayout *horizontalLayout_48;
    QLabel *label_45;
    QLineEdit *magicHP;
    QLabel *label_87;
    QLineEdit *lineEdit_3;
    QLabel *label_88;
    QLineEdit *lineEdit_4;
    QLabel *label_89;
    QLineEdit *lineEdit_5;
    QLabel *label_90;
    QLineEdit *lineEdit_6;
    QHBoxLayout *horizontalLayout_51;
    QLabel *label_48;
    QComboBox *magicType;
    QHBoxLayout *horizontalLayout_85;
    QLabel *label_74;
    QSpacerItem *horizontalSpacer_8;
    QLineEdit *magicRound1;
    QLabel *label_75;
    QLineEdit *magicRound2;
    QRadioButton *arrayRadio;
    QGroupBox *groupBox_11;
    QVBoxLayout *verticalLayout_27;
    QHBoxLayout *horizontalLayout_46;
    QLabel *label_44;
    QSlider *arraySlider0;
    QComboBox *arrayProp0;
    QLineEdit *arrayValue0;
    QLabel *label_73;
    QLineEdit *arrayValueMax0;
    QHBoxLayout *horizontalLayout_49;
    QLabel *label_46;
    QSlider *arraySlider1;
    QComboBox *arrayProp1;
    QLineEdit *arrayValue1;
    QLabel *label_76;
    QLineEdit *arrayValueMax1;
    QHBoxLayout *horizontalLayout_60;
    QLabel *label_49;
    QSlider *arraySlider2;
    QComboBox *arrayProp2;
    QLineEdit *arrayValue2;
    QLabel *label_77;
    QLineEdit *arrayValueMax2;
    QHBoxLayout *horizontalLayout_82;
    QLabel *label_52;
    QSlider *arraySlider3;
    QComboBox *arrayProp3;
    QLineEdit *arrayValue3;
    QLabel *label_78;
    QLineEdit *arrayValueMax3;
    QHBoxLayout *horizontalLayout_83;
    QLabel *label_55;
    QSlider *arraySlider4;
    QComboBox *arrayProp4;
    QLineEdit *arrayValue4;
    QLabel *label_79;
    QLineEdit *arrayValueMax4;
    QSpacerItem *verticalSpacer_4;
    QWidget *tab_map;
    QHBoxLayout *horizontalLayout_75;
    QVBoxLayout *verticalLayout_36;
    QHBoxLayout *horizontalLayout_73;
    QPushButton *mapAddButton;
    QPushButton *mapDelButton;
    QListWidget *mapListWidget;
    QHBoxLayout *horizontalLayout_74;
    QPushButton *mapUpButton;
    QPushButton *mapDownButton;
    QVBoxLayout *verticalLayout_37;
    QHBoxLayout *horizontalLayout_76;
    QLabel *label_67;
    QLineEdit *mapName;
    QHBoxLayout *horizontalLayout_84;
    QLabel *label_80;
    QLineEdit *mapResName;
    QLabel *label_69;
    QTextEdit *mapComment;
    QSpacerItem *verticalSpacer_6;
    QSpacerItem *horizontalSpacer_5;
    QWidget *tab_battle;
    QHBoxLayout *horizontalLayout_65;
    QVBoxLayout *verticalLayout_34;
    QHBoxLayout *horizontalLayout_63;
    QPushButton *battleAddButton;
    QPushButton *battleDelButton;
    QListWidget *battleListWidget;
    QHBoxLayout *horizontalLayout_64;
    QPushButton *battleUpButton;
    QPushButton *battleDownButton;
    QVBoxLayout *verticalLayout_35;
    QHBoxLayout *horizontalLayout_66;
    QLabel *label_56;
    QLineEdit *battleName;
    QHBoxLayout *horizontalLayout_89;
    QLabel *label_86;
    QLineEdit *battleScriptName;
    QHBoxLayout *horizontalLayout_67;
    QLabel *label_57;
    QComboBox *battleEnemiesComb0;
    QLabel *label_59;
    QLineEdit *battleLevel0;
    QHBoxLayout *horizontalLayout_68;
    QHBoxLayout *horizontalLayout_69;
    QLabel *label_58;
    QComboBox *battleEnemiesComb1;
    QLabel *label_60;
    QLineEdit *battleLevel1;
    QHBoxLayout *horizontalLayout_70;
    QLabel *label_62;
    QComboBox *battleEnemiesComb2;
    QLabel *label_61;
    QLineEdit *battleLevel2;
    QHBoxLayout *horizontalLayout_71;
    QLabel *label_64;
    QComboBox *battleEnemiesComb3;
    QLabel *label_63;
    QLineEdit *battleLevel3;
    QHBoxLayout *horizontalLayout_72;
    QLabel *label_66;
    QComboBox *battleEnemiesComb4;
    QLabel *label_65;
    QLineEdit *battleLevel4;
    QLabel *label_68;
    QTextEdit *battleComment;
    QSpacerItem *horizontalSpacer_4;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ZhouYuEditor)
    {
        if (ZhouYuEditor->objectName().isEmpty())
            ZhouYuEditor->setObjectName(QStringLiteral("ZhouYuEditor"));
        ZhouYuEditor->resize(725, 634);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ZhouYuEditor->sizePolicy().hasHeightForWidth());
        ZhouYuEditor->setSizePolicy(sizePolicy);
        ZhouYuEditor->setMaximumSize(QSize(10240, 10240));
        centralWidget = new QWidget(ZhouYuEditor);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_72 = new QLabel(centralWidget);
        label_72->setObjectName(QStringLiteral("label_72"));

        horizontalLayout_2->addWidget(label_72);

        pathEdit = new QLineEdit(centralWidget);
        pathEdit->setObjectName(QStringLiteral("pathEdit"));

        horizontalLayout_2->addWidget(pathEdit);

        selectDirButton = new QPushButton(centralWidget);
        selectDirButton->setObjectName(QStringLiteral("selectDirButton"));

        horizontalLayout_2->addWidget(selectDirButton);

        loadButton = new QPushButton(centralWidget);
        loadButton->setObjectName(QStringLiteral("loadButton"));

        horizontalLayout_2->addWidget(loadButton);

        saveButton = new QPushButton(centralWidget);
        saveButton->setObjectName(QStringLiteral("saveButton"));

        horizontalLayout_2->addWidget(saveButton);


        verticalLayout_2->addLayout(horizontalLayout_2);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_4 = new QVBoxLayout(tab);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        actorTableWidget = new QTableWidget(tab);
        if (actorTableWidget->columnCount() < 7)
            actorTableWidget->setColumnCount(7);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        actorTableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        actorTableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        actorTableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        actorTableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        actorTableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        actorTableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        actorTableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        actorTableWidget->setObjectName(QStringLiteral("actorTableWidget"));
        actorTableWidget->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        actorTableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        actorTableWidget->setSortingEnabled(true);
        actorTableWidget->horizontalHeader()->setVisible(false);
        actorTableWidget->horizontalHeader()->setCascadingSectionResizes(false);
        actorTableWidget->verticalHeader()->setVisible(false);

        verticalLayout_4->addWidget(actorTableWidget);

        tabWidget->addTab(tab, QString());
        tab_actor = new QWidget();
        tab_actor->setObjectName(QStringLiteral("tab_actor"));
        horizontalLayout_3 = new QHBoxLayout(tab_actor);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        actorAddButton = new QPushButton(tab_actor);
        actorAddButton->setObjectName(QStringLiteral("actorAddButton"));
        actorAddButton->setMinimumSize(QSize(0, 0));
        actorAddButton->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_18->addWidget(actorAddButton);

        actorDelButton = new QPushButton(tab_actor);
        actorDelButton->setObjectName(QStringLiteral("actorDelButton"));

        horizontalLayout_18->addWidget(actorDelButton);


        verticalLayout_10->addLayout(horizontalLayout_18);

        actorListWidget = new QListWidget(tab_actor);
        actorListWidget->setObjectName(QStringLiteral("actorListWidget"));
        actorListWidget->setMinimumSize(QSize(160, 0));
        actorListWidget->setMaximumSize(QSize(180, 16777215));

        verticalLayout_10->addWidget(actorListWidget);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        actorUpButton = new QPushButton(tab_actor);
        actorUpButton->setObjectName(QStringLiteral("actorUpButton"));

        horizontalLayout_19->addWidget(actorUpButton);

        actorDownButton = new QPushButton(tab_actor);
        actorDownButton->setObjectName(QStringLiteral("actorDownButton"));

        horizontalLayout_19->addWidget(actorDownButton);


        verticalLayout_10->addLayout(horizontalLayout_19);


        horizontalLayout_3->addLayout(verticalLayout_10);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBox_2 = new QGroupBox(tab_actor);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_5 = new QVBoxLayout(groupBox_2);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_7->addWidget(label_4);

        actorNameEdit = new QLineEdit(groupBox_2);
        actorNameEdit->setObjectName(QStringLiteral("actorNameEdit"));
        actorNameEdit->setMinimumSize(QSize(80, 0));
        actorNameEdit->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_7->addWidget(actorNameEdit);


        verticalLayout_5->addLayout(horizontalLayout_7);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_4->addWidget(label);

        actorName2Edit = new QLineEdit(groupBox_2);
        actorName2Edit->setObjectName(QStringLiteral("actorName2Edit"));
        actorName2Edit->setMinimumSize(QSize(80, 0));
        actorName2Edit->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_4->addWidget(actorName2Edit);


        verticalLayout_5->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_5->addWidget(label_2);

        actorResName = new QLineEdit(groupBox_2);
        actorResName->setObjectName(QStringLiteral("actorResName"));
        actorResName->setMinimumSize(QSize(80, 0));
        actorResName->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_5->addWidget(actorResName);


        verticalLayout_5->addLayout(horizontalLayout_5);

        horizontalLayout_90 = new QHBoxLayout();
        horizontalLayout_90->setSpacing(6);
        horizontalLayout_90->setObjectName(QStringLiteral("horizontalLayout_90"));
        label_91 = new QLabel(groupBox_2);
        label_91->setObjectName(QStringLiteral("label_91"));

        horizontalLayout_90->addWidget(label_91);

        actorBattleResName = new QLineEdit(groupBox_2);
        actorBattleResName->setObjectName(QStringLiteral("actorBattleResName"));
        actorBattleResName->setMinimumSize(QSize(80, 0));
        actorBattleResName->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_90->addWidget(actorBattleResName);


        verticalLayout_5->addLayout(horizontalLayout_90);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_6->addWidget(label_3);

        actorEquipName = new QLineEdit(groupBox_2);
        actorEquipName->setObjectName(QStringLiteral("actorEquipName"));
        actorEquipName->setMinimumSize(QSize(80, 0));
        actorEquipName->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_6->addWidget(actorEquipName);


        verticalLayout_5->addLayout(horizontalLayout_6);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_8->addWidget(label_5);

        actorWeaponType = new QComboBox(groupBox_2);
        actorWeaponType->setObjectName(QStringLiteral("actorWeaponType"));
        actorWeaponType->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_8->addWidget(actorWeaponType);


        verticalLayout_5->addLayout(horizontalLayout_8);


        verticalLayout_3->addWidget(groupBox_2);

        groupBox = new QGroupBox(tab_actor);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_6 = new QVBoxLayout(groupBox);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_9->addWidget(label_6);

        actorISlider = new QSlider(groupBox);
        actorISlider->setObjectName(QStringLiteral("actorISlider"));
        actorISlider->setOrientation(Qt::Horizontal);

        horizontalLayout_9->addWidget(actorISlider);

        actorI = new QLabel(groupBox);
        actorI->setObjectName(QStringLiteral("actorI"));
        actorI->setMinimumSize(QSize(28, 0));
        actorI->setMaximumSize(QSize(28, 16777215));

        horizontalLayout_9->addWidget(actorI);


        verticalLayout_6->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout_10->addWidget(label_7);

        actorSSlider = new QSlider(groupBox);
        actorSSlider->setObjectName(QStringLiteral("actorSSlider"));
        actorSSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_10->addWidget(actorSSlider);

        actorS = new QLabel(groupBox);
        actorS->setObjectName(QStringLiteral("actorS"));
        actorS->setMinimumSize(QSize(28, 0));
        actorS->setMaximumSize(QSize(28, 16777215));

        horizontalLayout_10->addWidget(actorS);


        verticalLayout_6->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_11->addWidget(label_8);

        actorLSlider = new QSlider(groupBox);
        actorLSlider->setObjectName(QStringLiteral("actorLSlider"));
        actorLSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_11->addWidget(actorLSlider);

        actorL = new QLabel(groupBox);
        actorL->setObjectName(QStringLiteral("actorL"));
        actorL->setMinimumSize(QSize(28, 0));
        actorL->setMaximumSize(QSize(28, 16777215));

        horizontalLayout_11->addWidget(actorL);


        verticalLayout_6->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_12->addWidget(label_9);

        actorASlider = new QSlider(groupBox);
        actorASlider->setObjectName(QStringLiteral("actorASlider"));
        actorASlider->setOrientation(Qt::Horizontal);

        horizontalLayout_12->addWidget(actorASlider);

        actorA = new QLabel(groupBox);
        actorA->setObjectName(QStringLiteral("actorA"));
        actorA->setMinimumSize(QSize(28, 0));
        actorA->setMaximumSize(QSize(28, 16777215));

        horizontalLayout_12->addWidget(actorA);


        verticalLayout_6->addLayout(horizontalLayout_12);


        verticalLayout_3->addWidget(groupBox);


        horizontalLayout_3->addLayout(verticalLayout_3);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        groupBox_4 = new QGroupBox(tab_actor);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        verticalLayout_9 = new QVBoxLayout(groupBox_4);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        label_20 = new QLabel(groupBox_4);
        label_20->setObjectName(QStringLiteral("label_20"));

        horizontalLayout_17->addWidget(label_20);

        label_19 = new QLabel(groupBox_4);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setMinimumSize(QSize(40, 0));
        label_19->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_17->addWidget(label_19);

        label_21 = new QLabel(groupBox_4);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setMinimumSize(QSize(40, 0));
        label_21->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_17->addWidget(label_21);


        verticalLayout_9->addLayout(horizontalLayout_17);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        actorOffset = new QComboBox(groupBox_4);
        actorOffset->setObjectName(QStringLiteral("actorOffset"));

        horizontalLayout_16->addWidget(actorOffset);

        actorOffsetX = new QLineEdit(groupBox_4);
        actorOffsetX->setObjectName(QStringLiteral("actorOffsetX"));
        actorOffsetX->setMinimumSize(QSize(40, 0));
        actorOffsetX->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_16->addWidget(actorOffsetX);

        actorOffsetY = new QLineEdit(groupBox_4);
        actorOffsetY->setObjectName(QStringLiteral("actorOffsetY"));
        actorOffsetY->setMinimumSize(QSize(40, 0));
        actorOffsetY->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_16->addWidget(actorOffsetY);


        verticalLayout_9->addLayout(horizontalLayout_16);


        verticalLayout_8->addWidget(groupBox_4);

        groupBox_3 = new QGroupBox(tab_actor);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_7 = new QVBoxLayout(groupBox_3);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        actorHasSkill = new QCheckBox(groupBox_3);
        actorHasSkill->setObjectName(QStringLiteral("actorHasSkill"));

        verticalLayout_7->addWidget(actorHasSkill);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_13->addWidget(label_14);

        actorSPName = new QLineEdit(groupBox_3);
        actorSPName->setObjectName(QStringLiteral("actorSPName"));
        actorSPName->setMinimumSize(QSize(120, 0));
        actorSPName->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_13->addWidget(actorSPName);


        verticalLayout_7->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        label_15 = new QLabel(groupBox_3);
        label_15->setObjectName(QStringLiteral("label_15"));

        horizontalLayout_14->addWidget(label_15);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer);

        actorSPMin = new QLineEdit(groupBox_3);
        actorSPMin->setObjectName(QStringLiteral("actorSPMin"));
        actorSPMin->setMinimumSize(QSize(40, 0));
        actorSPMin->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_14->addWidget(actorSPMin);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout_14->addWidget(label_10);

        actorSPMax = new QLineEdit(groupBox_3);
        actorSPMax->setObjectName(QStringLiteral("actorSPMax"));
        actorSPMax->setMinimumSize(QSize(40, 0));
        actorSPMax->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_14->addWidget(actorSPMax);


        verticalLayout_7->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        label_16 = new QLabel(groupBox_3);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_15->addWidget(label_16);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_6);

        actorAttackTimes = new QLineEdit(groupBox_3);
        actorAttackTimes->setObjectName(QStringLiteral("actorAttackTimes"));
        actorAttackTimes->setMinimumSize(QSize(40, 0));
        actorAttackTimes->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_15->addWidget(actorAttackTimes);


        verticalLayout_7->addLayout(horizontalLayout_15);

        horizontalLayout_93 = new QHBoxLayout();
        horizontalLayout_93->setSpacing(6);
        horizontalLayout_93->setObjectName(QStringLiteral("horizontalLayout_93"));
        label_92 = new QLabel(groupBox_3);
        label_92->setObjectName(QStringLiteral("label_92"));

        horizontalLayout_93->addWidget(label_92);

        actorSPDelay = new QLineEdit(groupBox_3);
        actorSPDelay->setObjectName(QStringLiteral("actorSPDelay"));

        horizontalLayout_93->addWidget(actorSPDelay);


        verticalLayout_7->addLayout(horizontalLayout_93);

        horizontalLayout_91 = new QHBoxLayout();
        horizontalLayout_91->setSpacing(6);
        horizontalLayout_91->setObjectName(QStringLiteral("horizontalLayout_91"));
        label_17 = new QLabel(groupBox_3);
        label_17->setObjectName(QStringLiteral("label_17"));

        horizontalLayout_91->addWidget(label_17);


        verticalLayout_7->addLayout(horizontalLayout_91);

        actorSPComment = new QPlainTextEdit(groupBox_3);
        actorSPComment->setObjectName(QStringLiteral("actorSPComment"));

        verticalLayout_7->addWidget(actorSPComment);


        verticalLayout_8->addWidget(groupBox_3);


        horizontalLayout_3->addLayout(verticalLayout_8);

        tabWidget->addTab(tab_actor, QString());
        tab_npc = new QWidget();
        tab_npc->setObjectName(QStringLiteral("tab_npc"));
        horizontalLayout_23 = new QHBoxLayout(tab_npc);
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_23->setObjectName(QStringLiteral("horizontalLayout_23"));
        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        verticalLayout_11->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        npcAddButton = new QPushButton(tab_npc);
        npcAddButton->setObjectName(QStringLiteral("npcAddButton"));
        npcAddButton->setMinimumSize(QSize(0, 0));
        npcAddButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_21->addWidget(npcAddButton);

        npcDelButton = new QPushButton(tab_npc);
        npcDelButton->setObjectName(QStringLiteral("npcDelButton"));
        npcDelButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_21->addWidget(npcDelButton);


        verticalLayout_11->addLayout(horizontalLayout_21);

        npcListWidget = new QListWidget(tab_npc);
        npcListWidget->setObjectName(QStringLiteral("npcListWidget"));
        npcListWidget->setMinimumSize(QSize(200, 0));
        npcListWidget->setMaximumSize(QSize(160, 16777215));

        verticalLayout_11->addWidget(npcListWidget);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        npcUpButton = new QPushButton(tab_npc);
        npcUpButton->setObjectName(QStringLiteral("npcUpButton"));
        npcUpButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_22->addWidget(npcUpButton);

        npcDownButton = new QPushButton(tab_npc);
        npcDownButton->setObjectName(QStringLiteral("npcDownButton"));
        npcDownButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_22->addWidget(npcDownButton);


        verticalLayout_11->addLayout(horizontalLayout_22);


        horizontalLayout_23->addLayout(verticalLayout_11);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setSpacing(6);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        groupBox_5 = new QGroupBox(tab_npc);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        verticalLayout_14 = new QVBoxLayout(groupBox_5);
        verticalLayout_14->setSpacing(6);
        verticalLayout_14->setContentsMargins(11, 11, 11, 11);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QStringLiteral("horizontalLayout_24"));
        label_23 = new QLabel(groupBox_5);
        label_23->setObjectName(QStringLiteral("label_23"));

        horizontalLayout_24->addWidget(label_23);

        npcName = new QLineEdit(groupBox_5);
        npcName->setObjectName(QStringLiteral("npcName"));
        npcName->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_24->addWidget(npcName);


        verticalLayout_14->addLayout(horizontalLayout_24);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setSpacing(6);
        horizontalLayout_25->setObjectName(QStringLiteral("horizontalLayout_25"));
        label_24 = new QLabel(groupBox_5);
        label_24->setObjectName(QStringLiteral("label_24"));

        horizontalLayout_25->addWidget(label_24);

        npcResName = new QLineEdit(groupBox_5);
        npcResName->setObjectName(QStringLiteral("npcResName"));
        npcResName->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_25->addWidget(npcResName);


        verticalLayout_14->addLayout(horizontalLayout_25);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setSpacing(6);
        horizontalLayout_26->setObjectName(QStringLiteral("horizontalLayout_26"));
        label_25 = new QLabel(groupBox_5);
        label_25->setObjectName(QStringLiteral("label_25"));

        horizontalLayout_26->addWidget(label_25);

        npcScriptName = new QLineEdit(groupBox_5);
        npcScriptName->setObjectName(QStringLiteral("npcScriptName"));
        npcScriptName->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_26->addWidget(npcScriptName);


        verticalLayout_14->addLayout(horizontalLayout_26);


        verticalLayout_12->addWidget(groupBox_5);


        verticalLayout->addLayout(verticalLayout_12);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_8);


        horizontalLayout_23->addLayout(verticalLayout);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_2);

        tabWidget->addTab(tab_npc, QString());
        tab_Item = new QWidget();
        tab_Item->setObjectName(QStringLiteral("tab_Item"));
        horizontalLayout_29 = new QHBoxLayout(tab_Item);
        horizontalLayout_29->setSpacing(6);
        horizontalLayout_29->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_29->setObjectName(QStringLiteral("horizontalLayout_29"));
        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        verticalLayout_15->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setSpacing(6);
        horizontalLayout_27->setObjectName(QStringLiteral("horizontalLayout_27"));
        itemAddButton = new QPushButton(tab_Item);
        itemAddButton->setObjectName(QStringLiteral("itemAddButton"));
        itemAddButton->setMinimumSize(QSize(0, 0));
        itemAddButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_27->addWidget(itemAddButton);

        itemDelButton = new QPushButton(tab_Item);
        itemDelButton->setObjectName(QStringLiteral("itemDelButton"));
        itemDelButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_27->addWidget(itemDelButton);


        verticalLayout_15->addLayout(horizontalLayout_27);

        itemListWidget = new QListWidget(tab_Item);
        itemListWidget->setObjectName(QStringLiteral("itemListWidget"));
        itemListWidget->setMinimumSize(QSize(120, 0));
        itemListWidget->setMaximumSize(QSize(160, 16777215));

        verticalLayout_15->addWidget(itemListWidget);

        horizontalLayout_28 = new QHBoxLayout();
        horizontalLayout_28->setSpacing(6);
        horizontalLayout_28->setObjectName(QStringLiteral("horizontalLayout_28"));
        itemUpButton = new QPushButton(tab_Item);
        itemUpButton->setObjectName(QStringLiteral("itemUpButton"));
        itemUpButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_28->addWidget(itemUpButton);

        itemDownButton = new QPushButton(tab_Item);
        itemDownButton->setObjectName(QStringLiteral("itemDownButton"));
        itemDownButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_28->addWidget(itemDownButton);


        verticalLayout_15->addLayout(horizontalLayout_28);


        horizontalLayout_29->addLayout(verticalLayout_15);

        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setSpacing(6);
        verticalLayout_17->setObjectName(QStringLiteral("verticalLayout_17"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_70 = new QLabel(tab_Item);
        label_70->setObjectName(QStringLiteral("label_70"));

        horizontalLayout->addWidget(label_70);

        itemName = new QLineEdit(tab_Item);
        itemName->setObjectName(QStringLiteral("itemName"));

        horizontalLayout->addWidget(itemName);


        verticalLayout_17->addLayout(horizontalLayout);

        label_81 = new QLabel(tab_Item);
        label_81->setObjectName(QStringLiteral("label_81"));

        verticalLayout_17->addWidget(label_81);

        itemEnv = new QComboBox(tab_Item);
        itemEnv->setObjectName(QStringLiteral("itemEnv"));

        verticalLayout_17->addWidget(itemEnv);

        horizontalLayout_92 = new QHBoxLayout();
        horizontalLayout_92->setSpacing(6);
        horizontalLayout_92->setObjectName(QStringLiteral("horizontalLayout_92"));
        label_41 = new QLabel(tab_Item);
        label_41->setObjectName(QStringLiteral("label_41"));

        horizontalLayout_92->addWidget(label_41);


        verticalLayout_17->addLayout(horizontalLayout_92);

        itemComment = new QTextEdit(tab_Item);
        itemComment->setObjectName(QStringLiteral("itemComment"));

        verticalLayout_17->addWidget(itemComment);

        horizontalLayout_88 = new QHBoxLayout();
        horizontalLayout_88->setSpacing(6);
        horizontalLayout_88->setObjectName(QStringLiteral("horizontalLayout_88"));
        label_85 = new QLabel(tab_Item);
        label_85->setObjectName(QStringLiteral("label_85"));

        horizontalLayout_88->addWidget(label_85);

        itemPrice = new QLineEdit(tab_Item);
        itemPrice->setObjectName(QStringLiteral("itemPrice"));
        itemPrice->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_88->addWidget(itemPrice);


        verticalLayout_17->addLayout(horizontalLayout_88);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_17->addItem(verticalSpacer);


        horizontalLayout_29->addLayout(verticalLayout_17);

        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setObjectName(QStringLiteral("verticalLayout_18"));
        equipRadio = new QRadioButton(tab_Item);
        equipRadio->setObjectName(QStringLiteral("equipRadio"));

        verticalLayout_18->addWidget(equipRadio);

        groupBox_8 = new QGroupBox(tab_Item);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        verticalLayout_19 = new QVBoxLayout(groupBox_8);
        verticalLayout_19->setSpacing(6);
        verticalLayout_19->setContentsMargins(11, 11, 11, 11);
        verticalLayout_19->setObjectName(QStringLiteral("verticalLayout_19"));
        horizontalLayout_30 = new QHBoxLayout();
        horizontalLayout_30->setSpacing(6);
        horizontalLayout_30->setObjectName(QStringLiteral("horizontalLayout_30"));
        label_26 = new QLabel(groupBox_8);
        label_26->setObjectName(QStringLiteral("label_26"));

        horizontalLayout_30->addWidget(label_26);

        itemType = new QComboBox(groupBox_8);
        itemType->setObjectName(QStringLiteral("itemType"));
        itemType->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_30->addWidget(itemType);


        verticalLayout_19->addLayout(horizontalLayout_30);

        horizontalLayout_31 = new QHBoxLayout();
        horizontalLayout_31->setSpacing(6);
        horizontalLayout_31->setObjectName(QStringLiteral("horizontalLayout_31"));
        label_27 = new QLabel(groupBox_8);
        label_27->setObjectName(QStringLiteral("label_27"));

        horizontalLayout_31->addWidget(label_27);

        equipAttack = new QLineEdit(groupBox_8);
        equipAttack->setObjectName(QStringLiteral("equipAttack"));
        equipAttack->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_31->addWidget(equipAttack);


        verticalLayout_19->addLayout(horizontalLayout_31);

        horizontalLayout_32 = new QHBoxLayout();
        horizontalLayout_32->setSpacing(6);
        horizontalLayout_32->setObjectName(QStringLiteral("horizontalLayout_32"));
        label_28 = new QLabel(groupBox_8);
        label_28->setObjectName(QStringLiteral("label_28"));

        horizontalLayout_32->addWidget(label_28);

        equipDefend = new QLineEdit(groupBox_8);
        equipDefend->setObjectName(QStringLiteral("equipDefend"));
        equipDefend->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_32->addWidget(equipDefend);


        verticalLayout_19->addLayout(horizontalLayout_32);

        horizontalLayout_86 = new QHBoxLayout();
        horizontalLayout_86->setSpacing(6);
        horizontalLayout_86->setObjectName(QStringLiteral("horizontalLayout_86"));
        label_82 = new QLabel(groupBox_8);
        label_82->setObjectName(QStringLiteral("label_82"));

        horizontalLayout_86->addWidget(label_82);

        equipI = new QLineEdit(groupBox_8);
        equipI->setObjectName(QStringLiteral("equipI"));
        equipI->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_86->addWidget(equipI);


        verticalLayout_19->addLayout(horizontalLayout_86);

        horizontalLayout_87 = new QHBoxLayout();
        horizontalLayout_87->setSpacing(6);
        horizontalLayout_87->setObjectName(QStringLiteral("horizontalLayout_87"));
        label_83 = new QLabel(groupBox_8);
        label_83->setObjectName(QStringLiteral("label_83"));

        horizontalLayout_87->addWidget(label_83);

        equipS = new QLineEdit(groupBox_8);
        equipS->setObjectName(QStringLiteral("equipS"));
        equipS->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_87->addWidget(equipS);


        verticalLayout_19->addLayout(horizontalLayout_87);

        horizontalLayout_34 = new QHBoxLayout();
        horizontalLayout_34->setSpacing(6);
        horizontalLayout_34->setObjectName(QStringLiteral("horizontalLayout_34"));
        label_30 = new QLabel(groupBox_8);
        label_30->setObjectName(QStringLiteral("label_30"));

        horizontalLayout_34->addWidget(label_30);

        equipL = new QLineEdit(groupBox_8);
        equipL->setObjectName(QStringLiteral("equipL"));
        equipL->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_34->addWidget(equipL);


        verticalLayout_19->addLayout(horizontalLayout_34);

        horizontalLayout_33 = new QHBoxLayout();
        horizontalLayout_33->setSpacing(6);
        horizontalLayout_33->setObjectName(QStringLiteral("horizontalLayout_33"));
        label_29 = new QLabel(groupBox_8);
        label_29->setObjectName(QStringLiteral("label_29"));

        horizontalLayout_33->addWidget(label_29);

        equipA = new QLineEdit(groupBox_8);
        equipA->setObjectName(QStringLiteral("equipA"));
        equipA->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_33->addWidget(equipA);


        verticalLayout_19->addLayout(horizontalLayout_33);

        label_84 = new QLabel(groupBox_8);
        label_84->setObjectName(QStringLiteral("label_84"));

        verticalLayout_19->addWidget(label_84);

        horizontalLayout_35 = new QHBoxLayout();
        horizontalLayout_35->setSpacing(6);
        horizontalLayout_35->setObjectName(QStringLiteral("horizontalLayout_35"));
        label_31 = new QLabel(groupBox_8);
        label_31->setObjectName(QStringLiteral("label_31"));

        horizontalLayout_35->addWidget(label_31);

        equipNeedI = new QLineEdit(groupBox_8);
        equipNeedI->setObjectName(QStringLiteral("equipNeedI"));
        equipNeedI->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_35->addWidget(equipNeedI);


        verticalLayout_19->addLayout(horizontalLayout_35);

        horizontalLayout_36 = new QHBoxLayout();
        horizontalLayout_36->setSpacing(6);
        horizontalLayout_36->setObjectName(QStringLiteral("horizontalLayout_36"));
        label_32 = new QLabel(groupBox_8);
        label_32->setObjectName(QStringLiteral("label_32"));

        horizontalLayout_36->addWidget(label_32);

        equipNeedS = new QLineEdit(groupBox_8);
        equipNeedS->setObjectName(QStringLiteral("equipNeedS"));
        equipNeedS->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_36->addWidget(equipNeedS);


        verticalLayout_19->addLayout(horizontalLayout_36);

        horizontalLayout_37 = new QHBoxLayout();
        horizontalLayout_37->setSpacing(6);
        horizontalLayout_37->setObjectName(QStringLiteral("horizontalLayout_37"));
        label_33 = new QLabel(groupBox_8);
        label_33->setObjectName(QStringLiteral("label_33"));

        horizontalLayout_37->addWidget(label_33);

        equipNeedL = new QLineEdit(groupBox_8);
        equipNeedL->setObjectName(QStringLiteral("equipNeedL"));
        equipNeedL->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_37->addWidget(equipNeedL);


        verticalLayout_19->addLayout(horizontalLayout_37);

        horizontalLayout_38 = new QHBoxLayout();
        horizontalLayout_38->setSpacing(6);
        horizontalLayout_38->setObjectName(QStringLiteral("horizontalLayout_38"));
        label_34 = new QLabel(groupBox_8);
        label_34->setObjectName(QStringLiteral("label_34"));

        horizontalLayout_38->addWidget(label_34);

        equipNeedA = new QLineEdit(groupBox_8);
        equipNeedA->setObjectName(QStringLiteral("equipNeedA"));
        equipNeedA->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_38->addWidget(equipNeedA);


        verticalLayout_19->addLayout(horizontalLayout_38);


        verticalLayout_18->addWidget(groupBox_8);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_18->addItem(verticalSpacer_5);


        horizontalLayout_29->addLayout(verticalLayout_18);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setSpacing(6);
        verticalLayout_16->setObjectName(QStringLiteral("verticalLayout_16"));
        supplyRadio = new QRadioButton(tab_Item);
        supplyRadio->setObjectName(QStringLiteral("supplyRadio"));

        verticalLayout_16->addWidget(supplyRadio);

        groupBox_7 = new QGroupBox(tab_Item);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        verticalLayout_21 = new QVBoxLayout(groupBox_7);
        verticalLayout_21->setSpacing(6);
        verticalLayout_21->setContentsMargins(11, 11, 11, 11);
        verticalLayout_21->setObjectName(QStringLiteral("verticalLayout_21"));
        supplyCheckBox = new QCheckBox(groupBox_7);
        supplyCheckBox->setObjectName(QStringLiteral("supplyCheckBox"));

        verticalLayout_21->addWidget(supplyCheckBox);

        horizontalLayout_39 = new QHBoxLayout();
        horizontalLayout_39->setSpacing(6);
        horizontalLayout_39->setObjectName(QStringLiteral("horizontalLayout_39"));
        label_35 = new QLabel(groupBox_7);
        label_35->setObjectName(QStringLiteral("label_35"));

        horizontalLayout_39->addWidget(label_35);

        supplyHP = new QLineEdit(groupBox_7);
        supplyHP->setObjectName(QStringLiteral("supplyHP"));
        supplyHP->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_39->addWidget(supplyHP);

        label_37 = new QLabel(groupBox_7);
        label_37->setObjectName(QStringLiteral("label_37"));

        horizontalLayout_39->addWidget(label_37);

        supplyHPRound = new QLineEdit(groupBox_7);
        supplyHPRound->setObjectName(QStringLiteral("supplyHPRound"));
        supplyHPRound->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_39->addWidget(supplyHPRound);


        verticalLayout_21->addLayout(horizontalLayout_39);

        horizontalLayout_40 = new QHBoxLayout();
        horizontalLayout_40->setSpacing(6);
        horizontalLayout_40->setObjectName(QStringLiteral("horizontalLayout_40"));
        label_36 = new QLabel(groupBox_7);
        label_36->setObjectName(QStringLiteral("label_36"));

        horizontalLayout_40->addWidget(label_36);

        supplyMP = new QLineEdit(groupBox_7);
        supplyMP->setObjectName(QStringLiteral("supplyMP"));
        supplyMP->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_40->addWidget(supplyMP);

        label_38 = new QLabel(groupBox_7);
        label_38->setObjectName(QStringLiteral("label_38"));

        horizontalLayout_40->addWidget(label_38);

        supplyMPRound = new QLineEdit(groupBox_7);
        supplyMPRound->setObjectName(QStringLiteral("supplyMPRound"));
        supplyMPRound->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_40->addWidget(supplyMPRound);


        verticalLayout_21->addLayout(horizontalLayout_40);


        verticalLayout_16->addWidget(groupBox_7);

        attackRadio = new QRadioButton(tab_Item);
        attackRadio->setObjectName(QStringLiteral("attackRadio"));

        verticalLayout_16->addWidget(attackRadio);

        groupBox_6 = new QGroupBox(tab_Item);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        verticalLayout_22 = new QVBoxLayout(groupBox_6);
        verticalLayout_22->setSpacing(6);
        verticalLayout_22->setContentsMargins(11, 11, 11, 11);
        verticalLayout_22->setObjectName(QStringLiteral("verticalLayout_22"));
        attackCheckBox = new QCheckBox(groupBox_6);
        attackCheckBox->setObjectName(QStringLiteral("attackCheckBox"));

        verticalLayout_22->addWidget(attackCheckBox);

        horizontalLayout_41 = new QHBoxLayout();
        horizontalLayout_41->setSpacing(6);
        horizontalLayout_41->setObjectName(QStringLiteral("horizontalLayout_41"));
        label_39 = new QLabel(groupBox_6);
        label_39->setObjectName(QStringLiteral("label_39"));

        horizontalLayout_41->addWidget(label_39);

        attackHP = new QLineEdit(groupBox_6);
        attackHP->setObjectName(QStringLiteral("attackHP"));
        attackHP->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_41->addWidget(attackHP);

        label_40 = new QLabel(groupBox_6);
        label_40->setObjectName(QStringLiteral("label_40"));

        horizontalLayout_41->addWidget(label_40);

        attackHPRound = new QLineEdit(groupBox_6);
        attackHPRound->setObjectName(QStringLiteral("attackHPRound"));
        attackHPRound->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_41->addWidget(attackHPRound);


        verticalLayout_22->addLayout(horizontalLayout_41);


        verticalLayout_16->addWidget(groupBox_6);

        otherRadio = new QRadioButton(tab_Item);
        otherRadio->setObjectName(QStringLiteral("otherRadio"));

        verticalLayout_16->addWidget(otherRadio);

        groupBox_16 = new QGroupBox(tab_Item);
        groupBox_16->setObjectName(QStringLiteral("groupBox_16"));

        verticalLayout_16->addWidget(groupBox_16);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_16->addItem(verticalSpacer_2);


        horizontalLayout_29->addLayout(verticalLayout_16);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_29->addItem(horizontalSpacer_3);

        tabWidget->addTab(tab_Item, QString());
        tab_story = new QWidget();
        tab_story->setObjectName(QStringLiteral("tab_story"));
        horizontalLayout_54 = new QHBoxLayout(tab_story);
        horizontalLayout_54->setSpacing(6);
        horizontalLayout_54->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_54->setObjectName(QStringLiteral("horizontalLayout_54"));
        verticalLayout_29 = new QVBoxLayout();
        verticalLayout_29->setSpacing(6);
        verticalLayout_29->setObjectName(QStringLiteral("verticalLayout_29"));
        groupBox_12 = new QGroupBox(tab_story);
        groupBox_12->setObjectName(QStringLiteral("groupBox_12"));
        verticalLayout_30 = new QVBoxLayout(groupBox_12);
        verticalLayout_30->setSpacing(6);
        verticalLayout_30->setContentsMargins(11, 11, 11, 11);
        verticalLayout_30->setObjectName(QStringLiteral("verticalLayout_30"));
        horizontalLayout_56 = new QHBoxLayout();
        horizontalLayout_56->setSpacing(6);
        horizontalLayout_56->setObjectName(QStringLiteral("horizontalLayout_56"));
        label_50 = new QLabel(groupBox_12);
        label_50->setObjectName(QStringLiteral("label_50"));

        horizontalLayout_56->addWidget(label_50);

        storyName = new QLineEdit(groupBox_12);
        storyName->setObjectName(QStringLiteral("storyName"));

        horizontalLayout_56->addWidget(storyName);


        verticalLayout_30->addLayout(horizontalLayout_56);

        horizontalLayout_57 = new QHBoxLayout();
        horizontalLayout_57->setSpacing(6);
        horizontalLayout_57->setObjectName(QStringLiteral("horizontalLayout_57"));
        label_51 = new QLabel(groupBox_12);
        label_51->setObjectName(QStringLiteral("label_51"));

        horizontalLayout_57->addWidget(label_51);

        storyComment = new QTextEdit(groupBox_12);
        storyComment->setObjectName(QStringLiteral("storyComment"));

        horizontalLayout_57->addWidget(storyComment);


        verticalLayout_30->addLayout(horizontalLayout_57);

        horizontalLayout_59 = new QHBoxLayout();
        horizontalLayout_59->setSpacing(6);
        horizontalLayout_59->setObjectName(QStringLiteral("horizontalLayout_59"));
        verticalLayout_31 = new QVBoxLayout();
        verticalLayout_31->setSpacing(6);
        verticalLayout_31->setObjectName(QStringLiteral("verticalLayout_31"));
        horizontalLayout_58 = new QHBoxLayout();
        horizontalLayout_58->setSpacing(6);
        horizontalLayout_58->setObjectName(QStringLiteral("horizontalLayout_58"));
        chapterAddButton = new QPushButton(groupBox_12);
        chapterAddButton->setObjectName(QStringLiteral("chapterAddButton"));

        horizontalLayout_58->addWidget(chapterAddButton);

        chapterDelButton = new QPushButton(groupBox_12);
        chapterDelButton->setObjectName(QStringLiteral("chapterDelButton"));

        horizontalLayout_58->addWidget(chapterDelButton);


        verticalLayout_31->addLayout(horizontalLayout_58);

        chapterListWidget = new QListWidget(groupBox_12);
        chapterListWidget->setObjectName(QStringLiteral("chapterListWidget"));

        verticalLayout_31->addWidget(chapterListWidget);

        horizontalLayout_55 = new QHBoxLayout();
        horizontalLayout_55->setSpacing(6);
        horizontalLayout_55->setObjectName(QStringLiteral("horizontalLayout_55"));
        chapterUpButton = new QPushButton(groupBox_12);
        chapterUpButton->setObjectName(QStringLiteral("chapterUpButton"));

        horizontalLayout_55->addWidget(chapterUpButton);

        chapterDownButton = new QPushButton(groupBox_12);
        chapterDownButton->setObjectName(QStringLiteral("chapterDownButton"));

        horizontalLayout_55->addWidget(chapterDownButton);


        verticalLayout_31->addLayout(horizontalLayout_55);


        horizontalLayout_59->addLayout(verticalLayout_31);

        groupBox_14 = new QGroupBox(groupBox_12);
        groupBox_14->setObjectName(QStringLiteral("groupBox_14"));
        verticalLayout_32 = new QVBoxLayout(groupBox_14);
        verticalLayout_32->setSpacing(6);
        verticalLayout_32->setContentsMargins(11, 11, 11, 11);
        verticalLayout_32->setObjectName(QStringLiteral("verticalLayout_32"));
        horizontalLayout_62 = new QHBoxLayout();
        horizontalLayout_62->setSpacing(6);
        horizontalLayout_62->setObjectName(QStringLiteral("horizontalLayout_62"));
        label_54 = new QLabel(groupBox_14);
        label_54->setObjectName(QStringLiteral("label_54"));

        horizontalLayout_62->addWidget(label_54);

        chapterName = new QLineEdit(groupBox_14);
        chapterName->setObjectName(QStringLiteral("chapterName"));

        horizontalLayout_62->addWidget(chapterName);


        verticalLayout_32->addLayout(horizontalLayout_62);

        label_53 = new QLabel(groupBox_14);
        label_53->setObjectName(QStringLiteral("label_53"));

        verticalLayout_32->addWidget(label_53);

        chapterComment = new QTextEdit(groupBox_14);
        chapterComment->setObjectName(QStringLiteral("chapterComment"));

        verticalLayout_32->addWidget(chapterComment);


        horizontalLayout_59->addWidget(groupBox_14);


        verticalLayout_30->addLayout(horizontalLayout_59);


        verticalLayout_29->addWidget(groupBox_12);


        horizontalLayout_54->addLayout(verticalLayout_29);

        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setSpacing(6);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        horizontalLayout_77 = new QHBoxLayout();
        horizontalLayout_77->setSpacing(6);
        horizontalLayout_77->setObjectName(QStringLiteral("horizontalLayout_77"));
        verticalLayout_38 = new QVBoxLayout();
        verticalLayout_38->setSpacing(6);
        verticalLayout_38->setObjectName(QStringLiteral("verticalLayout_38"));
        horizontalLayout_79 = new QHBoxLayout();
        horizontalLayout_79->setSpacing(6);
        horizontalLayout_79->setObjectName(QStringLiteral("horizontalLayout_79"));
        segmentAddButton = new QPushButton(tab_story);
        segmentAddButton->setObjectName(QStringLiteral("segmentAddButton"));
        segmentAddButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_79->addWidget(segmentAddButton);

        segmentDelButton = new QPushButton(tab_story);
        segmentDelButton->setObjectName(QStringLiteral("segmentDelButton"));
        segmentDelButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_79->addWidget(segmentDelButton);


        verticalLayout_38->addLayout(horizontalLayout_79);

        segmentListWidget = new QListWidget(tab_story);
        segmentListWidget->setObjectName(QStringLiteral("segmentListWidget"));

        verticalLayout_38->addWidget(segmentListWidget);

        horizontalLayout_78 = new QHBoxLayout();
        horizontalLayout_78->setSpacing(6);
        horizontalLayout_78->setObjectName(QStringLiteral("horizontalLayout_78"));
        segmentUpButton = new QPushButton(tab_story);
        segmentUpButton->setObjectName(QStringLiteral("segmentUpButton"));
        segmentUpButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_78->addWidget(segmentUpButton);

        segmentDownButton = new QPushButton(tab_story);
        segmentDownButton->setObjectName(QStringLiteral("segmentDownButton"));
        segmentDownButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_78->addWidget(segmentDownButton);


        verticalLayout_38->addLayout(horizontalLayout_78);


        horizontalLayout_77->addLayout(verticalLayout_38);

        groupBox_15 = new QGroupBox(tab_story);
        groupBox_15->setObjectName(QStringLiteral("groupBox_15"));
        verticalLayout_39 = new QVBoxLayout(groupBox_15);
        verticalLayout_39->setSpacing(6);
        verticalLayout_39->setContentsMargins(11, 11, 11, 11);
        verticalLayout_39->setObjectName(QStringLiteral("verticalLayout_39"));
        horizontalLayout_61 = new QHBoxLayout();
        horizontalLayout_61->setSpacing(6);
        horizontalLayout_61->setObjectName(QStringLiteral("horizontalLayout_61"));
        label_11 = new QLabel(groupBox_15);
        label_11->setObjectName(QStringLiteral("label_11"));

        horizontalLayout_61->addWidget(label_11);

        segmentName = new QLineEdit(groupBox_15);
        segmentName->setObjectName(QStringLiteral("segmentName"));
        segmentName->setMinimumSize(QSize(120, 0));

        horizontalLayout_61->addWidget(segmentName);


        verticalLayout_39->addLayout(horizontalLayout_61);

        horizontalLayout_80 = new QHBoxLayout();
        horizontalLayout_80->setSpacing(6);
        horizontalLayout_80->setObjectName(QStringLiteral("horizontalLayout_80"));
        label_12 = new QLabel(groupBox_15);
        label_12->setObjectName(QStringLiteral("label_12"));

        horizontalLayout_80->addWidget(label_12);

        segmentStart = new QLineEdit(groupBox_15);
        segmentStart->setObjectName(QStringLiteral("segmentStart"));
        segmentStart->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_80->addWidget(segmentStart);


        verticalLayout_39->addLayout(horizontalLayout_80);

        horizontalLayout_81 = new QHBoxLayout();
        horizontalLayout_81->setSpacing(6);
        horizontalLayout_81->setObjectName(QStringLiteral("horizontalLayout_81"));
        label_13 = new QLabel(groupBox_15);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout_81->addWidget(label_13);

        segmentEnd = new QLineEdit(groupBox_15);
        segmentEnd->setObjectName(QStringLiteral("segmentEnd"));
        segmentEnd->setMinimumSize(QSize(0, 0));
        segmentEnd->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_81->addWidget(segmentEnd);


        verticalLayout_39->addLayout(horizontalLayout_81);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_39->addItem(verticalSpacer_3);


        horizontalLayout_77->addWidget(groupBox_15);


        verticalLayout_13->addLayout(horizontalLayout_77);

        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        verticalLayout_28 = new QVBoxLayout();
        verticalLayout_28->setSpacing(6);
        verticalLayout_28->setObjectName(QStringLiteral("verticalLayout_28"));
        verticalLayout_28->setSizeConstraint(QLayout::SetDefaultConstraint);
        label_18 = new QLabel(tab_story);
        label_18->setObjectName(QStringLiteral("label_18"));

        verticalLayout_28->addWidget(label_18);

        horizontalLayout_52 = new QHBoxLayout();
        horizontalLayout_52->setSpacing(6);
        horizontalLayout_52->setObjectName(QStringLiteral("horizontalLayout_52"));
        storyAddButton = new QPushButton(tab_story);
        storyAddButton->setObjectName(QStringLiteral("storyAddButton"));
        storyAddButton->setMinimumSize(QSize(0, 0));
        storyAddButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_52->addWidget(storyAddButton);

        storyDelButton = new QPushButton(tab_story);
        storyDelButton->setObjectName(QStringLiteral("storyDelButton"));
        storyDelButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_52->addWidget(storyDelButton);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_52->addItem(horizontalSpacer_7);

        storyUpButton = new QPushButton(tab_story);
        storyUpButton->setObjectName(QStringLiteral("storyUpButton"));
        storyUpButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_52->addWidget(storyUpButton);

        storyDownButton = new QPushButton(tab_story);
        storyDownButton->setObjectName(QStringLiteral("storyDownButton"));
        storyDownButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_52->addWidget(storyDownButton);


        verticalLayout_28->addLayout(horizontalLayout_52);

        subStoryListWidget = new QListWidget(tab_story);
        subStoryListWidget->setObjectName(QStringLiteral("subStoryListWidget"));
        subStoryListWidget->setMinimumSize(QSize(120, 160));
        subStoryListWidget->setMaximumSize(QSize(16777215, 16777215));

        verticalLayout_28->addWidget(subStoryListWidget);

        horizontalLayout_53 = new QHBoxLayout();
        horizontalLayout_53->setSpacing(6);
        horizontalLayout_53->setObjectName(QStringLiteral("horizontalLayout_53"));
        label_22 = new QLabel(tab_story);
        label_22->setObjectName(QStringLiteral("label_22"));

        horizontalLayout_53->addWidget(label_22);

        subStoryDetail = new QLineEdit(tab_story);
        subStoryDetail->setObjectName(QStringLiteral("subStoryDetail"));

        horizontalLayout_53->addWidget(subStoryDetail);


        verticalLayout_28->addLayout(horizontalLayout_53);


        horizontalLayout_20->addLayout(verticalLayout_28);


        verticalLayout_13->addLayout(horizontalLayout_20);


        horizontalLayout_54->addLayout(verticalLayout_13);

        tabWidget->addTab(tab_story, QString());
        tab_skill = new QWidget();
        tab_skill->setObjectName(QStringLiteral("tab_skill"));
        horizontalLayout_44 = new QHBoxLayout(tab_skill);
        horizontalLayout_44->setSpacing(6);
        horizontalLayout_44->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_44->setObjectName(QStringLiteral("horizontalLayout_44"));
        verticalLayout_23 = new QVBoxLayout();
        verticalLayout_23->setSpacing(6);
        verticalLayout_23->setObjectName(QStringLiteral("verticalLayout_23"));
        horizontalLayout_42 = new QHBoxLayout();
        horizontalLayout_42->setSpacing(6);
        horizontalLayout_42->setObjectName(QStringLiteral("horizontalLayout_42"));
        magicAddButton = new QPushButton(tab_skill);
        magicAddButton->setObjectName(QStringLiteral("magicAddButton"));
        magicAddButton->setMinimumSize(QSize(0, 0));
        magicAddButton->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_42->addWidget(magicAddButton);

        magicDelButton = new QPushButton(tab_skill);
        magicDelButton->setObjectName(QStringLiteral("magicDelButton"));

        horizontalLayout_42->addWidget(magicDelButton);


        verticalLayout_23->addLayout(horizontalLayout_42);

        magicListWidget = new QListWidget(tab_skill);
        magicListWidget->setObjectName(QStringLiteral("magicListWidget"));
        magicListWidget->setMinimumSize(QSize(120, 0));
        magicListWidget->setMaximumSize(QSize(180, 16777215));

        verticalLayout_23->addWidget(magicListWidget);

        horizontalLayout_43 = new QHBoxLayout();
        horizontalLayout_43->setSpacing(6);
        horizontalLayout_43->setObjectName(QStringLiteral("horizontalLayout_43"));
        magicUpButton = new QPushButton(tab_skill);
        magicUpButton->setObjectName(QStringLiteral("magicUpButton"));

        horizontalLayout_43->addWidget(magicUpButton);

        magicDownButton = new QPushButton(tab_skill);
        magicDownButton->setObjectName(QStringLiteral("magicDownButton"));

        horizontalLayout_43->addWidget(magicDownButton);


        verticalLayout_23->addLayout(horizontalLayout_43);


        horizontalLayout_44->addLayout(verticalLayout_23);

        verticalLayout_25 = new QVBoxLayout();
        verticalLayout_25->setSpacing(6);
        verticalLayout_25->setObjectName(QStringLiteral("verticalLayout_25"));
        groupBox_13 = new QGroupBox(tab_skill);
        groupBox_13->setObjectName(QStringLiteral("groupBox_13"));
        verticalLayout_33 = new QVBoxLayout(groupBox_13);
        verticalLayout_33->setSpacing(6);
        verticalLayout_33->setContentsMargins(11, 11, 11, 11);
        verticalLayout_33->setObjectName(QStringLiteral("verticalLayout_33"));
        horizontalLayout_47 = new QHBoxLayout();
        horizontalLayout_47->setSpacing(6);
        horizontalLayout_47->setObjectName(QStringLiteral("horizontalLayout_47"));
        label_71 = new QLabel(groupBox_13);
        label_71->setObjectName(QStringLiteral("label_71"));

        horizontalLayout_47->addWidget(label_71);

        magicName = new QLineEdit(groupBox_13);
        magicName->setObjectName(QStringLiteral("magicName"));
        magicName->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_47->addWidget(magicName);


        verticalLayout_33->addLayout(horizontalLayout_47);

        horizontalLayout_50 = new QHBoxLayout();
        horizontalLayout_50->setSpacing(6);
        horizontalLayout_50->setObjectName(QStringLiteral("horizontalLayout_50"));
        label_47 = new QLabel(groupBox_13);
        label_47->setObjectName(QStringLiteral("label_47"));

        horizontalLayout_50->addWidget(label_47);

        magicMP = new QLineEdit(groupBox_13);
        magicMP->setObjectName(QStringLiteral("magicMP"));
        magicMP->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_50->addWidget(magicMP);


        verticalLayout_33->addLayout(horizontalLayout_50);

        label_42 = new QLabel(groupBox_13);
        label_42->setObjectName(QStringLiteral("label_42"));

        verticalLayout_33->addWidget(label_42);

        magicComment = new QTextEdit(groupBox_13);
        magicComment->setObjectName(QStringLiteral("magicComment"));

        verticalLayout_33->addWidget(magicComment);


        verticalLayout_25->addWidget(groupBox_13);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_25->addItem(verticalSpacer_7);


        horizontalLayout_44->addLayout(verticalLayout_25);

        verticalLayout_24 = new QVBoxLayout();
        verticalLayout_24->setSpacing(6);
        verticalLayout_24->setObjectName(QStringLiteral("verticalLayout_24"));
        magicRadio = new QRadioButton(tab_skill);
        magicRadio->setObjectName(QStringLiteral("magicRadio"));

        verticalLayout_24->addWidget(magicRadio);

        groupBox_10 = new QGroupBox(tab_skill);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        verticalLayout_26 = new QVBoxLayout(groupBox_10);
        verticalLayout_26->setSpacing(6);
        verticalLayout_26->setContentsMargins(11, 11, 11, 11);
        verticalLayout_26->setObjectName(QStringLiteral("verticalLayout_26"));
        horizontalLayout_45 = new QHBoxLayout();
        horizontalLayout_45->setSpacing(6);
        horizontalLayout_45->setObjectName(QStringLiteral("horizontalLayout_45"));
        label_43 = new QLabel(groupBox_10);
        label_43->setObjectName(QStringLiteral("label_43"));

        horizontalLayout_45->addWidget(label_43);

        magicTarget = new QComboBox(groupBox_10);
        magicTarget->setObjectName(QStringLiteral("magicTarget"));

        horizontalLayout_45->addWidget(magicTarget);


        verticalLayout_26->addLayout(horizontalLayout_45);

        horizontalLayout_48 = new QHBoxLayout();
        horizontalLayout_48->setSpacing(6);
        horizontalLayout_48->setObjectName(QStringLiteral("horizontalLayout_48"));
        label_45 = new QLabel(groupBox_10);
        label_45->setObjectName(QStringLiteral("label_45"));

        horizontalLayout_48->addWidget(label_45);

        magicHP = new QLineEdit(groupBox_10);
        magicHP->setObjectName(QStringLiteral("magicHP"));
        magicHP->setMinimumSize(QSize(30, 0));
        magicHP->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_48->addWidget(magicHP);

        label_87 = new QLabel(groupBox_10);
        label_87->setObjectName(QStringLiteral("label_87"));

        horizontalLayout_48->addWidget(label_87);

        lineEdit_3 = new QLineEdit(groupBox_10);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setMinimumSize(QSize(30, 0));
        lineEdit_3->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_48->addWidget(lineEdit_3);

        label_88 = new QLabel(groupBox_10);
        label_88->setObjectName(QStringLiteral("label_88"));

        horizontalLayout_48->addWidget(label_88);

        lineEdit_4 = new QLineEdit(groupBox_10);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setMinimumSize(QSize(30, 0));
        lineEdit_4->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_48->addWidget(lineEdit_4);

        label_89 = new QLabel(groupBox_10);
        label_89->setObjectName(QStringLiteral("label_89"));

        horizontalLayout_48->addWidget(label_89);

        lineEdit_5 = new QLineEdit(groupBox_10);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setMinimumSize(QSize(30, 0));
        lineEdit_5->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_48->addWidget(lineEdit_5);

        label_90 = new QLabel(groupBox_10);
        label_90->setObjectName(QStringLiteral("label_90"));

        horizontalLayout_48->addWidget(label_90);

        lineEdit_6 = new QLineEdit(groupBox_10);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        lineEdit_6->setMinimumSize(QSize(30, 0));
        lineEdit_6->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_48->addWidget(lineEdit_6);


        verticalLayout_26->addLayout(horizontalLayout_48);

        horizontalLayout_51 = new QHBoxLayout();
        horizontalLayout_51->setSpacing(6);
        horizontalLayout_51->setObjectName(QStringLiteral("horizontalLayout_51"));
        label_48 = new QLabel(groupBox_10);
        label_48->setObjectName(QStringLiteral("label_48"));

        horizontalLayout_51->addWidget(label_48);

        magicType = new QComboBox(groupBox_10);
        magicType->setObjectName(QStringLiteral("magicType"));

        horizontalLayout_51->addWidget(magicType);


        verticalLayout_26->addLayout(horizontalLayout_51);

        horizontalLayout_85 = new QHBoxLayout();
        horizontalLayout_85->setSpacing(6);
        horizontalLayout_85->setObjectName(QStringLiteral("horizontalLayout_85"));
        label_74 = new QLabel(groupBox_10);
        label_74->setObjectName(QStringLiteral("label_74"));

        horizontalLayout_85->addWidget(label_74);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_85->addItem(horizontalSpacer_8);

        magicRound1 = new QLineEdit(groupBox_10);
        magicRound1->setObjectName(QStringLiteral("magicRound1"));
        magicRound1->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_85->addWidget(magicRound1);

        label_75 = new QLabel(groupBox_10);
        label_75->setObjectName(QStringLiteral("label_75"));

        horizontalLayout_85->addWidget(label_75);

        magicRound2 = new QLineEdit(groupBox_10);
        magicRound2->setObjectName(QStringLiteral("magicRound2"));
        magicRound2->setMaximumSize(QSize(40, 16777215));

        horizontalLayout_85->addWidget(magicRound2);


        verticalLayout_26->addLayout(horizontalLayout_85);


        verticalLayout_24->addWidget(groupBox_10);

        arrayRadio = new QRadioButton(tab_skill);
        arrayRadio->setObjectName(QStringLiteral("arrayRadio"));

        verticalLayout_24->addWidget(arrayRadio);

        groupBox_11 = new QGroupBox(tab_skill);
        groupBox_11->setObjectName(QStringLiteral("groupBox_11"));
        verticalLayout_27 = new QVBoxLayout(groupBox_11);
        verticalLayout_27->setSpacing(6);
        verticalLayout_27->setContentsMargins(11, 11, 11, 11);
        verticalLayout_27->setObjectName(QStringLiteral("verticalLayout_27"));
        horizontalLayout_46 = new QHBoxLayout();
        horizontalLayout_46->setSpacing(6);
        horizontalLayout_46->setObjectName(QStringLiteral("horizontalLayout_46"));
        label_44 = new QLabel(groupBox_11);
        label_44->setObjectName(QStringLiteral("label_44"));

        horizontalLayout_46->addWidget(label_44);

        arraySlider0 = new QSlider(groupBox_11);
        arraySlider0->setObjectName(QStringLiteral("arraySlider0"));
        arraySlider0->setMinimumSize(QSize(90, 0));
        arraySlider0->setMaximumSize(QSize(90, 16777215));
        arraySlider0->setOrientation(Qt::Horizontal);

        horizontalLayout_46->addWidget(arraySlider0);

        arrayProp0 = new QComboBox(groupBox_11);
        arrayProp0->setObjectName(QStringLiteral("arrayProp0"));
        arrayProp0->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_46->addWidget(arrayProp0);

        arrayValue0 = new QLineEdit(groupBox_11);
        arrayValue0->setObjectName(QStringLiteral("arrayValue0"));
        arrayValue0->setMinimumSize(QSize(25, 0));
        arrayValue0->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_46->addWidget(arrayValue0);

        label_73 = new QLabel(groupBox_11);
        label_73->setObjectName(QStringLiteral("label_73"));

        horizontalLayout_46->addWidget(label_73);

        arrayValueMax0 = new QLineEdit(groupBox_11);
        arrayValueMax0->setObjectName(QStringLiteral("arrayValueMax0"));
        arrayValueMax0->setMinimumSize(QSize(25, 0));
        arrayValueMax0->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_46->addWidget(arrayValueMax0);


        verticalLayout_27->addLayout(horizontalLayout_46);

        horizontalLayout_49 = new QHBoxLayout();
        horizontalLayout_49->setSpacing(6);
        horizontalLayout_49->setObjectName(QStringLiteral("horizontalLayout_49"));
        label_46 = new QLabel(groupBox_11);
        label_46->setObjectName(QStringLiteral("label_46"));

        horizontalLayout_49->addWidget(label_46);

        arraySlider1 = new QSlider(groupBox_11);
        arraySlider1->setObjectName(QStringLiteral("arraySlider1"));
        arraySlider1->setMinimumSize(QSize(90, 0));
        arraySlider1->setMaximumSize(QSize(80, 16777215));
        arraySlider1->setOrientation(Qt::Horizontal);

        horizontalLayout_49->addWidget(arraySlider1);

        arrayProp1 = new QComboBox(groupBox_11);
        arrayProp1->setObjectName(QStringLiteral("arrayProp1"));
        arrayProp1->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_49->addWidget(arrayProp1);

        arrayValue1 = new QLineEdit(groupBox_11);
        arrayValue1->setObjectName(QStringLiteral("arrayValue1"));
        arrayValue1->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_49->addWidget(arrayValue1);

        label_76 = new QLabel(groupBox_11);
        label_76->setObjectName(QStringLiteral("label_76"));

        horizontalLayout_49->addWidget(label_76);

        arrayValueMax1 = new QLineEdit(groupBox_11);
        arrayValueMax1->setObjectName(QStringLiteral("arrayValueMax1"));
        arrayValueMax1->setMinimumSize(QSize(25, 0));
        arrayValueMax1->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_49->addWidget(arrayValueMax1);


        verticalLayout_27->addLayout(horizontalLayout_49);

        horizontalLayout_60 = new QHBoxLayout();
        horizontalLayout_60->setSpacing(6);
        horizontalLayout_60->setObjectName(QStringLiteral("horizontalLayout_60"));
        label_49 = new QLabel(groupBox_11);
        label_49->setObjectName(QStringLiteral("label_49"));

        horizontalLayout_60->addWidget(label_49);

        arraySlider2 = new QSlider(groupBox_11);
        arraySlider2->setObjectName(QStringLiteral("arraySlider2"));
        arraySlider2->setMinimumSize(QSize(90, 0));
        arraySlider2->setOrientation(Qt::Horizontal);

        horizontalLayout_60->addWidget(arraySlider2);

        arrayProp2 = new QComboBox(groupBox_11);
        arrayProp2->setObjectName(QStringLiteral("arrayProp2"));
        arrayProp2->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_60->addWidget(arrayProp2);

        arrayValue2 = new QLineEdit(groupBox_11);
        arrayValue2->setObjectName(QStringLiteral("arrayValue2"));
        arrayValue2->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_60->addWidget(arrayValue2);

        label_77 = new QLabel(groupBox_11);
        label_77->setObjectName(QStringLiteral("label_77"));

        horizontalLayout_60->addWidget(label_77);

        arrayValueMax2 = new QLineEdit(groupBox_11);
        arrayValueMax2->setObjectName(QStringLiteral("arrayValueMax2"));
        arrayValueMax2->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_60->addWidget(arrayValueMax2);


        verticalLayout_27->addLayout(horizontalLayout_60);

        horizontalLayout_82 = new QHBoxLayout();
        horizontalLayout_82->setSpacing(6);
        horizontalLayout_82->setObjectName(QStringLiteral("horizontalLayout_82"));
        label_52 = new QLabel(groupBox_11);
        label_52->setObjectName(QStringLiteral("label_52"));

        horizontalLayout_82->addWidget(label_52);

        arraySlider3 = new QSlider(groupBox_11);
        arraySlider3->setObjectName(QStringLiteral("arraySlider3"));
        arraySlider3->setMinimumSize(QSize(90, 0));
        arraySlider3->setOrientation(Qt::Horizontal);

        horizontalLayout_82->addWidget(arraySlider3);

        arrayProp3 = new QComboBox(groupBox_11);
        arrayProp3->setObjectName(QStringLiteral("arrayProp3"));
        arrayProp3->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_82->addWidget(arrayProp3);

        arrayValue3 = new QLineEdit(groupBox_11);
        arrayValue3->setObjectName(QStringLiteral("arrayValue3"));
        arrayValue3->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_82->addWidget(arrayValue3);

        label_78 = new QLabel(groupBox_11);
        label_78->setObjectName(QStringLiteral("label_78"));

        horizontalLayout_82->addWidget(label_78);

        arrayValueMax3 = new QLineEdit(groupBox_11);
        arrayValueMax3->setObjectName(QStringLiteral("arrayValueMax3"));
        arrayValueMax3->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_82->addWidget(arrayValueMax3);


        verticalLayout_27->addLayout(horizontalLayout_82);

        horizontalLayout_83 = new QHBoxLayout();
        horizontalLayout_83->setSpacing(6);
        horizontalLayout_83->setObjectName(QStringLiteral("horizontalLayout_83"));
        label_55 = new QLabel(groupBox_11);
        label_55->setObjectName(QStringLiteral("label_55"));

        horizontalLayout_83->addWidget(label_55);

        arraySlider4 = new QSlider(groupBox_11);
        arraySlider4->setObjectName(QStringLiteral("arraySlider4"));
        arraySlider4->setMinimumSize(QSize(90, 0));
        arraySlider4->setOrientation(Qt::Horizontal);

        horizontalLayout_83->addWidget(arraySlider4);

        arrayProp4 = new QComboBox(groupBox_11);
        arrayProp4->setObjectName(QStringLiteral("arrayProp4"));
        arrayProp4->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_83->addWidget(arrayProp4);

        arrayValue4 = new QLineEdit(groupBox_11);
        arrayValue4->setObjectName(QStringLiteral("arrayValue4"));
        arrayValue4->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_83->addWidget(arrayValue4);

        label_79 = new QLabel(groupBox_11);
        label_79->setObjectName(QStringLiteral("label_79"));

        horizontalLayout_83->addWidget(label_79);

        arrayValueMax4 = new QLineEdit(groupBox_11);
        arrayValueMax4->setObjectName(QStringLiteral("arrayValueMax4"));
        arrayValueMax4->setMaximumSize(QSize(25, 16777215));

        horizontalLayout_83->addWidget(arrayValueMax4);


        verticalLayout_27->addLayout(horizontalLayout_83);


        verticalLayout_24->addWidget(groupBox_11);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_24->addItem(verticalSpacer_4);


        horizontalLayout_44->addLayout(verticalLayout_24);

        tabWidget->addTab(tab_skill, QString());
        tab_map = new QWidget();
        tab_map->setObjectName(QStringLiteral("tab_map"));
        horizontalLayout_75 = new QHBoxLayout(tab_map);
        horizontalLayout_75->setSpacing(6);
        horizontalLayout_75->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_75->setObjectName(QStringLiteral("horizontalLayout_75"));
        verticalLayout_36 = new QVBoxLayout();
        verticalLayout_36->setSpacing(6);
        verticalLayout_36->setObjectName(QStringLiteral("verticalLayout_36"));
        verticalLayout_36->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_73 = new QHBoxLayout();
        horizontalLayout_73->setSpacing(6);
        horizontalLayout_73->setObjectName(QStringLiteral("horizontalLayout_73"));
        mapAddButton = new QPushButton(tab_map);
        mapAddButton->setObjectName(QStringLiteral("mapAddButton"));
        mapAddButton->setMinimumSize(QSize(0, 0));
        mapAddButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_73->addWidget(mapAddButton);

        mapDelButton = new QPushButton(tab_map);
        mapDelButton->setObjectName(QStringLiteral("mapDelButton"));
        mapDelButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_73->addWidget(mapDelButton);


        verticalLayout_36->addLayout(horizontalLayout_73);

        mapListWidget = new QListWidget(tab_map);
        mapListWidget->setObjectName(QStringLiteral("mapListWidget"));
        mapListWidget->setMinimumSize(QSize(120, 0));
        mapListWidget->setMaximumSize(QSize(190, 16777215));

        verticalLayout_36->addWidget(mapListWidget);

        horizontalLayout_74 = new QHBoxLayout();
        horizontalLayout_74->setSpacing(6);
        horizontalLayout_74->setObjectName(QStringLiteral("horizontalLayout_74"));
        mapUpButton = new QPushButton(tab_map);
        mapUpButton->setObjectName(QStringLiteral("mapUpButton"));
        mapUpButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_74->addWidget(mapUpButton);

        mapDownButton = new QPushButton(tab_map);
        mapDownButton->setObjectName(QStringLiteral("mapDownButton"));
        mapDownButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_74->addWidget(mapDownButton);


        verticalLayout_36->addLayout(horizontalLayout_74);


        horizontalLayout_75->addLayout(verticalLayout_36);

        verticalLayout_37 = new QVBoxLayout();
        verticalLayout_37->setSpacing(6);
        verticalLayout_37->setObjectName(QStringLiteral("verticalLayout_37"));
        horizontalLayout_76 = new QHBoxLayout();
        horizontalLayout_76->setSpacing(6);
        horizontalLayout_76->setObjectName(QStringLiteral("horizontalLayout_76"));
        label_67 = new QLabel(tab_map);
        label_67->setObjectName(QStringLiteral("label_67"));

        horizontalLayout_76->addWidget(label_67);

        mapName = new QLineEdit(tab_map);
        mapName->setObjectName(QStringLiteral("mapName"));
        mapName->setMaximumSize(QSize(160, 16777215));

        horizontalLayout_76->addWidget(mapName);


        verticalLayout_37->addLayout(horizontalLayout_76);

        horizontalLayout_84 = new QHBoxLayout();
        horizontalLayout_84->setSpacing(6);
        horizontalLayout_84->setObjectName(QStringLiteral("horizontalLayout_84"));
        label_80 = new QLabel(tab_map);
        label_80->setObjectName(QStringLiteral("label_80"));

        horizontalLayout_84->addWidget(label_80);

        mapResName = new QLineEdit(tab_map);
        mapResName->setObjectName(QStringLiteral("mapResName"));
        mapResName->setMaximumSize(QSize(160, 16777215));

        horizontalLayout_84->addWidget(mapResName);


        verticalLayout_37->addLayout(horizontalLayout_84);

        label_69 = new QLabel(tab_map);
        label_69->setObjectName(QStringLiteral("label_69"));

        verticalLayout_37->addWidget(label_69);

        mapComment = new QTextEdit(tab_map);
        mapComment->setObjectName(QStringLiteral("mapComment"));

        verticalLayout_37->addWidget(mapComment);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_37->addItem(verticalSpacer_6);


        horizontalLayout_75->addLayout(verticalLayout_37);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_75->addItem(horizontalSpacer_5);

        tabWidget->addTab(tab_map, QString());
        tab_battle = new QWidget();
        tab_battle->setObjectName(QStringLiteral("tab_battle"));
        horizontalLayout_65 = new QHBoxLayout(tab_battle);
        horizontalLayout_65->setSpacing(6);
        horizontalLayout_65->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_65->setObjectName(QStringLiteral("horizontalLayout_65"));
        verticalLayout_34 = new QVBoxLayout();
        verticalLayout_34->setSpacing(6);
        verticalLayout_34->setObjectName(QStringLiteral("verticalLayout_34"));
        verticalLayout_34->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_63 = new QHBoxLayout();
        horizontalLayout_63->setSpacing(6);
        horizontalLayout_63->setObjectName(QStringLiteral("horizontalLayout_63"));
        battleAddButton = new QPushButton(tab_battle);
        battleAddButton->setObjectName(QStringLiteral("battleAddButton"));
        battleAddButton->setMinimumSize(QSize(0, 0));
        battleAddButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_63->addWidget(battleAddButton);

        battleDelButton = new QPushButton(tab_battle);
        battleDelButton->setObjectName(QStringLiteral("battleDelButton"));
        battleDelButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_63->addWidget(battleDelButton);


        verticalLayout_34->addLayout(horizontalLayout_63);

        battleListWidget = new QListWidget(tab_battle);
        battleListWidget->setObjectName(QStringLiteral("battleListWidget"));
        battleListWidget->setMinimumSize(QSize(180, 0));
        battleListWidget->setMaximumSize(QSize(160, 16777215));

        verticalLayout_34->addWidget(battleListWidget);

        horizontalLayout_64 = new QHBoxLayout();
        horizontalLayout_64->setSpacing(6);
        horizontalLayout_64->setObjectName(QStringLiteral("horizontalLayout_64"));
        battleUpButton = new QPushButton(tab_battle);
        battleUpButton->setObjectName(QStringLiteral("battleUpButton"));
        battleUpButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_64->addWidget(battleUpButton);

        battleDownButton = new QPushButton(tab_battle);
        battleDownButton->setObjectName(QStringLiteral("battleDownButton"));
        battleDownButton->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_64->addWidget(battleDownButton);


        verticalLayout_34->addLayout(horizontalLayout_64);


        horizontalLayout_65->addLayout(verticalLayout_34);

        verticalLayout_35 = new QVBoxLayout();
        verticalLayout_35->setSpacing(6);
        verticalLayout_35->setObjectName(QStringLiteral("verticalLayout_35"));
        horizontalLayout_66 = new QHBoxLayout();
        horizontalLayout_66->setSpacing(6);
        horizontalLayout_66->setObjectName(QStringLiteral("horizontalLayout_66"));
        label_56 = new QLabel(tab_battle);
        label_56->setObjectName(QStringLiteral("label_56"));

        horizontalLayout_66->addWidget(label_56);

        battleName = new QLineEdit(tab_battle);
        battleName->setObjectName(QStringLiteral("battleName"));
        battleName->setMaximumSize(QSize(160, 16777215));

        horizontalLayout_66->addWidget(battleName);


        verticalLayout_35->addLayout(horizontalLayout_66);

        horizontalLayout_89 = new QHBoxLayout();
        horizontalLayout_89->setSpacing(6);
        horizontalLayout_89->setObjectName(QStringLiteral("horizontalLayout_89"));
        label_86 = new QLabel(tab_battle);
        label_86->setObjectName(QStringLiteral("label_86"));

        horizontalLayout_89->addWidget(label_86);

        battleScriptName = new QLineEdit(tab_battle);
        battleScriptName->setObjectName(QStringLiteral("battleScriptName"));
        battleScriptName->setMaximumSize(QSize(160, 16777215));

        horizontalLayout_89->addWidget(battleScriptName);


        verticalLayout_35->addLayout(horizontalLayout_89);

        horizontalLayout_67 = new QHBoxLayout();
        horizontalLayout_67->setSpacing(6);
        horizontalLayout_67->setObjectName(QStringLiteral("horizontalLayout_67"));
        label_57 = new QLabel(tab_battle);
        label_57->setObjectName(QStringLiteral("label_57"));

        horizontalLayout_67->addWidget(label_57);

        battleEnemiesComb0 = new QComboBox(tab_battle);
        battleEnemiesComb0->setObjectName(QStringLiteral("battleEnemiesComb0"));

        horizontalLayout_67->addWidget(battleEnemiesComb0);

        label_59 = new QLabel(tab_battle);
        label_59->setObjectName(QStringLiteral("label_59"));

        horizontalLayout_67->addWidget(label_59);

        battleLevel0 = new QLineEdit(tab_battle);
        battleLevel0->setObjectName(QStringLiteral("battleLevel0"));
        battleLevel0->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_67->addWidget(battleLevel0);


        verticalLayout_35->addLayout(horizontalLayout_67);

        horizontalLayout_68 = new QHBoxLayout();
        horizontalLayout_68->setSpacing(6);
        horizontalLayout_68->setObjectName(QStringLiteral("horizontalLayout_68"));
        horizontalLayout_69 = new QHBoxLayout();
        horizontalLayout_69->setSpacing(6);
        horizontalLayout_69->setObjectName(QStringLiteral("horizontalLayout_69"));
        label_58 = new QLabel(tab_battle);
        label_58->setObjectName(QStringLiteral("label_58"));

        horizontalLayout_69->addWidget(label_58);

        battleEnemiesComb1 = new QComboBox(tab_battle);
        battleEnemiesComb1->setObjectName(QStringLiteral("battleEnemiesComb1"));

        horizontalLayout_69->addWidget(battleEnemiesComb1);

        label_60 = new QLabel(tab_battle);
        label_60->setObjectName(QStringLiteral("label_60"));

        horizontalLayout_69->addWidget(label_60);

        battleLevel1 = new QLineEdit(tab_battle);
        battleLevel1->setObjectName(QStringLiteral("battleLevel1"));
        battleLevel1->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_69->addWidget(battleLevel1);


        horizontalLayout_68->addLayout(horizontalLayout_69);


        verticalLayout_35->addLayout(horizontalLayout_68);

        horizontalLayout_70 = new QHBoxLayout();
        horizontalLayout_70->setSpacing(6);
        horizontalLayout_70->setObjectName(QStringLiteral("horizontalLayout_70"));
        label_62 = new QLabel(tab_battle);
        label_62->setObjectName(QStringLiteral("label_62"));

        horizontalLayout_70->addWidget(label_62);

        battleEnemiesComb2 = new QComboBox(tab_battle);
        battleEnemiesComb2->setObjectName(QStringLiteral("battleEnemiesComb2"));

        horizontalLayout_70->addWidget(battleEnemiesComb2);

        label_61 = new QLabel(tab_battle);
        label_61->setObjectName(QStringLiteral("label_61"));

        horizontalLayout_70->addWidget(label_61);

        battleLevel2 = new QLineEdit(tab_battle);
        battleLevel2->setObjectName(QStringLiteral("battleLevel2"));
        battleLevel2->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_70->addWidget(battleLevel2);


        verticalLayout_35->addLayout(horizontalLayout_70);

        horizontalLayout_71 = new QHBoxLayout();
        horizontalLayout_71->setSpacing(6);
        horizontalLayout_71->setObjectName(QStringLiteral("horizontalLayout_71"));
        label_64 = new QLabel(tab_battle);
        label_64->setObjectName(QStringLiteral("label_64"));

        horizontalLayout_71->addWidget(label_64);

        battleEnemiesComb3 = new QComboBox(tab_battle);
        battleEnemiesComb3->setObjectName(QStringLiteral("battleEnemiesComb3"));

        horizontalLayout_71->addWidget(battleEnemiesComb3);

        label_63 = new QLabel(tab_battle);
        label_63->setObjectName(QStringLiteral("label_63"));

        horizontalLayout_71->addWidget(label_63);

        battleLevel3 = new QLineEdit(tab_battle);
        battleLevel3->setObjectName(QStringLiteral("battleLevel3"));
        battleLevel3->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_71->addWidget(battleLevel3);


        verticalLayout_35->addLayout(horizontalLayout_71);

        horizontalLayout_72 = new QHBoxLayout();
        horizontalLayout_72->setSpacing(6);
        horizontalLayout_72->setObjectName(QStringLiteral("horizontalLayout_72"));
        label_66 = new QLabel(tab_battle);
        label_66->setObjectName(QStringLiteral("label_66"));

        horizontalLayout_72->addWidget(label_66);

        battleEnemiesComb4 = new QComboBox(tab_battle);
        battleEnemiesComb4->setObjectName(QStringLiteral("battleEnemiesComb4"));

        horizontalLayout_72->addWidget(battleEnemiesComb4);

        label_65 = new QLabel(tab_battle);
        label_65->setObjectName(QStringLiteral("label_65"));

        horizontalLayout_72->addWidget(label_65);

        battleLevel4 = new QLineEdit(tab_battle);
        battleLevel4->setObjectName(QStringLiteral("battleLevel4"));
        battleLevel4->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_72->addWidget(battleLevel4);


        verticalLayout_35->addLayout(horizontalLayout_72);

        label_68 = new QLabel(tab_battle);
        label_68->setObjectName(QStringLiteral("label_68"));

        verticalLayout_35->addWidget(label_68);

        battleComment = new QTextEdit(tab_battle);
        battleComment->setObjectName(QStringLiteral("battleComment"));

        verticalLayout_35->addWidget(battleComment);


        horizontalLayout_65->addLayout(verticalLayout_35);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_65->addItem(horizontalSpacer_4);

        tabWidget->addTab(tab_battle, QString());

        verticalLayout_2->addWidget(tabWidget);

        ZhouYuEditor->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(ZhouYuEditor);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ZhouYuEditor->setStatusBar(statusBar);

        retranslateUi(ZhouYuEditor);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(ZhouYuEditor);
    } // setupUi

    void retranslateUi(QMainWindow *ZhouYuEditor)
    {
        ZhouYuEditor->setWindowTitle(QApplication::translate("ZhouYuEditor", "\345\221\250\347\221\234\344\274\240\347\274\226\350\276\221\345\231\250", 0));
        label_72->setText(QApplication::translate("ZhouYuEditor", "\350\265\204\346\226\231\346\226\207\344\273\266\345\244\271\350\267\257\345\276\204", 0));
        selectDirButton->setText(QApplication::translate("ZhouYuEditor", "\351\200\211\346\213\251\346\226\207\344\273\266\345\244\271\350\267\257\345\276\204", 0));
        loadButton->setText(QApplication::translate("ZhouYuEditor", "\345\212\240\350\275\275", 0));
        saveButton->setText(QApplication::translate("ZhouYuEditor", "\344\277\235\345\255\230", 0));
        QTableWidgetItem *___qtablewidgetitem = actorTableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("ZhouYuEditor", "\347\274\226\345\217\267", 0));
        QTableWidgetItem *___qtablewidgetitem1 = actorTableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("ZhouYuEditor", "\345\247\223\345\220\215", 0));
        QTableWidgetItem *___qtablewidgetitem2 = actorTableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("ZhouYuEditor", "\345\255\227", 0));
        QTableWidgetItem *___qtablewidgetitem3 = actorTableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("ZhouYuEditor", "\346\231\272\345\212\233", 0));
        QTableWidgetItem *___qtablewidgetitem4 = actorTableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("ZhouYuEditor", "\346\255\246\345\212\233", 0));
        QTableWidgetItem *___qtablewidgetitem5 = actorTableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("ZhouYuEditor", "\346\214\207\346\214\245", 0));
        QTableWidgetItem *___qtablewidgetitem6 = actorTableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("ZhouYuEditor", "\346\225\217\346\215\267", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ZhouYuEditor", "\346\216\222\345\220\215", 0));
        actorAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        actorDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        actorUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        actorDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        groupBox_2->setTitle(QApplication::translate("ZhouYuEditor", "\345\237\272\346\234\254\344\277\241\346\201\257", 0));
        label_4->setText(QApplication::translate("ZhouYuEditor", "\345\247\223\345\220\215", 0));
        label->setText(QApplication::translate("ZhouYuEditor", "\345\255\227", 0));
        label_2->setText(QApplication::translate("ZhouYuEditor", "\350\265\204\346\272\220\345\220\215", 0));
        label_91->setText(QApplication::translate("ZhouYuEditor", "\346\210\230\346\226\227\350\265\204\346\272\220\345\220\215", 0));
        label_3->setText(QApplication::translate("ZhouYuEditor", "\350\243\205\345\244\207\345\220\215\347\247\260", 0));
        label_5->setText(QApplication::translate("ZhouYuEditor", "\346\255\246\345\231\250\347\261\273\345\210\253", 0));
        actorWeaponType->clear();
        actorWeaponType->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\350\275\273", 0)
         << QApplication::translate("ZhouYuEditor", "\350\277\234", 0)
         << QApplication::translate("ZhouYuEditor", "\351\225\277", 0)
         << QApplication::translate("ZhouYuEditor", "\351\207\215", 0)
         << QApplication::translate("ZhouYuEditor", "\345\217\214", 0)
        );
        groupBox->setTitle(QApplication::translate("ZhouYuEditor", "\350\203\275\345\212\233", 0));
        label_6->setText(QApplication::translate("ZhouYuEditor", "\346\231\272\345\212\233", 0));
        actorI->setText(QApplication::translate("ZhouYuEditor", "100", 0));
        label_7->setText(QApplication::translate("ZhouYuEditor", "\346\255\246\345\212\233", 0));
        actorS->setText(QApplication::translate("ZhouYuEditor", "100", 0));
        label_8->setText(QApplication::translate("ZhouYuEditor", "\346\214\207\346\214\245", 0));
        actorL->setText(QApplication::translate("ZhouYuEditor", "100", 0));
        label_9->setText(QApplication::translate("ZhouYuEditor", "\346\225\217\346\215\267", 0));
        actorA->setText(QApplication::translate("ZhouYuEditor", "100", 0));
        groupBox_4->setTitle(QApplication::translate("ZhouYuEditor", "\345\212\250\347\224\273\344\275\215\347\275\256\347\247\273\345\212\250", 0));
        label_20->setText(QApplication::translate("ZhouYuEditor", "\345\201\217\347\247\273\351\241\271", 0));
        label_19->setText(QApplication::translate("ZhouYuEditor", "\345\201\217\347\247\273X", 0));
        label_21->setText(QApplication::translate("ZhouYuEditor", "\345\201\217\347\247\273Y", 0));
        actorOffset->clear();
        actorOffset->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\351\235\231\346\255\242", 0)
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273", 0)
         << QApplication::translate("ZhouYuEditor", "\347\211\271\346\212\200", 0)
         << QApplication::translate("ZhouYuEditor", "\346\255\273\344\272\241", 0)
         << QApplication::translate("ZhouYuEditor", "\345\217\227\344\274\244\345\256\263", 0)
         << QApplication::translate("ZhouYuEditor", "\350\267\221\345\212\250", 0)
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\350\267\235\347\246\273", 0)
         << QApplication::translate("ZhouYuEditor", "\350\242\253\345\207\273\350\267\235\347\246\273", 0)
        );
        groupBox_3->setTitle(QApplication::translate("ZhouYuEditor", "\347\211\271\346\256\212\346\212\200\350\203\275", 0));
        actorHasSkill->setText(QApplication::translate("ZhouYuEditor", "\346\213\245\346\234\211\347\211\271\346\256\212\346\212\200\350\203\275", 0));
        label_14->setText(QApplication::translate("ZhouYuEditor", "\345\220\215\347\247\260", 0));
        label_15->setText(QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\345\212\233\350\214\203\345\233\264", 0));
        label_10->setText(QApplication::translate("ZhouYuEditor", "~", 0));
        label_16->setText(QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\346\254\241\346\225\260", 0));
        label_92->setText(QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\350\212\202\345\245\217\345\273\266\350\277\237", 0));
        label_17->setText(QApplication::translate("ZhouYuEditor", "\346\212\200\350\203\275\350\257\264\346\230\216", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_actor), QApplication::translate("ZhouYuEditor", "\350\247\222\350\211\262", 0));
        npcAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        npcDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        npcUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        npcDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        groupBox_5->setTitle(QApplication::translate("ZhouYuEditor", "\344\277\241\346\201\257", 0));
        label_23->setText(QApplication::translate("ZhouYuEditor", "\345\247\223\345\220\215", 0));
        label_24->setText(QApplication::translate("ZhouYuEditor", "\350\265\204\346\272\220\345\220\215", 0));
        label_25->setText(QApplication::translate("ZhouYuEditor", "\350\204\232\346\234\254\345\220\215", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_npc), QApplication::translate("ZhouYuEditor", "NPC", 0));
        itemAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        itemDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        itemUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        itemDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        label_70->setText(QApplication::translate("ZhouYuEditor", "\347\211\251\345\223\201\345\220\215\347\247\260", 0));
        label_81->setText(QApplication::translate("ZhouYuEditor", "\344\275\277\347\224\250\347\216\257\345\242\203", 0));
        itemEnv->clear();
        itemEnv->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\346\210\230\346\226\227", 0)
         << QApplication::translate("ZhouYuEditor", "\351\235\236\346\210\230\346\226\227", 0)
         << QApplication::translate("ZhouYuEditor", "\346\211\200\346\234\211", 0)
         << QApplication::translate("ZhouYuEditor", "\350\243\205\345\244\207", 0)
        );
        label_41->setText(QApplication::translate("ZhouYuEditor", "\347\211\251\345\223\201\350\257\264\346\230\216", 0));
        label_85->setText(QApplication::translate("ZhouYuEditor", "\345\224\256\344\273\267", 0));
        equipRadio->setText(QApplication::translate("ZhouYuEditor", "\350\243\205\345\244\207", 0));
        groupBox_8->setTitle(QApplication::translate("ZhouYuEditor", "\350\243\205\345\244\207\347\211\251\345\223\201\345\242\236\345\212\240\345\261\236\346\200\247", 0));
        label_26->setText(QApplication::translate("ZhouYuEditor", "\347\261\273\345\210\253", 0));
        itemType->clear();
        itemType->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\345\206\240", 0)
         << QApplication::translate("ZhouYuEditor", "\350\242\215", 0)
         << QApplication::translate("ZhouYuEditor", "\351\235\264", 0)
         << QApplication::translate("ZhouYuEditor", "\347\233\276", 0)
         << QApplication::translate("ZhouYuEditor", "\351\251\254", 0)
         << QApplication::translate("ZhouYuEditor", "\347\211\271\346\256\212", 0)
        );
        label_27->setText(QApplication::translate("ZhouYuEditor", "\346\224\273", 0));
        label_28->setText(QApplication::translate("ZhouYuEditor", "\351\230\262", 0));
        label_82->setText(QApplication::translate("ZhouYuEditor", "\346\231\272", 0));
        label_83->setText(QApplication::translate("ZhouYuEditor", "\346\255\246", 0));
        label_30->setText(QApplication::translate("ZhouYuEditor", "\346\214\207", 0));
        label_29->setText(QApplication::translate("ZhouYuEditor", "\346\225\217", 0));
        label_84->setText(QApplication::translate("ZhouYuEditor", "\350\243\205\345\244\207\351\234\200\346\261\202", 0));
        label_31->setText(QApplication::translate("ZhouYuEditor", "\351\234\200\346\231\272", 0));
        label_32->setText(QApplication::translate("ZhouYuEditor", "\351\234\200\346\255\246", 0));
        label_33->setText(QApplication::translate("ZhouYuEditor", "\351\234\200\346\225\217", 0));
        label_34->setText(QApplication::translate("ZhouYuEditor", "\351\234\200\346\214\207", 0));
        supplyRadio->setText(QApplication::translate("ZhouYuEditor", "\350\241\245\345\205\205\347\211\251\345\223\201", 0));
        groupBox_7->setTitle(QApplication::translate("ZhouYuEditor", "\350\241\245\345\205\205\347\211\251\345\223\201\345\261\236\346\200\247", 0));
        supplyCheckBox->setText(QApplication::translate("ZhouYuEditor", "\346\230\257\345\220\246\345\205\250\344\275\223", 0));
        label_35->setText(QApplication::translate("ZhouYuEditor", "HP", 0));
        label_37->setText(QApplication::translate("ZhouYuEditor", "\345\233\236\345\220\210", 0));
        label_36->setText(QApplication::translate("ZhouYuEditor", "MP", 0));
        label_38->setText(QApplication::translate("ZhouYuEditor", "\345\233\236\345\220\210", 0));
        attackRadio->setText(QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\347\211\251\345\223\201", 0));
        groupBox_6->setTitle(QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\347\211\251\345\223\201\345\261\236\346\200\247", 0));
        attackCheckBox->setText(QApplication::translate("ZhouYuEditor", "\346\230\257\345\220\246\345\205\250\344\275\223", 0));
        label_39->setText(QApplication::translate("ZhouYuEditor", "HP", 0));
        label_40->setText(QApplication::translate("ZhouYuEditor", "\345\233\236\345\220\210", 0));
        otherRadio->setText(QApplication::translate("ZhouYuEditor", "\345\205\266\345\256\203\347\211\251\345\223\201", 0));
        groupBox_16->setTitle(QApplication::translate("ZhouYuEditor", "\345\205\266\345\256\203\347\211\251\345\223\201\345\261\236\346\200\247", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Item), QApplication::translate("ZhouYuEditor", "\347\211\251\345\223\201", 0));
        groupBox_12->setTitle(QApplication::translate("ZhouYuEditor", "\345\211\247\346\234\254\346\200\273\350\247\210", 0));
        label_50->setText(QApplication::translate("ZhouYuEditor", "\345\220\215\347\247\260", 0));
        label_51->setText(QApplication::translate("ZhouYuEditor", "\350\257\246\346\203\205", 0));
        chapterAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        chapterDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        chapterUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        chapterDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        groupBox_14->setTitle(QApplication::translate("ZhouYuEditor", "\347\253\240\350\212\202\345\261\236\346\200\247", 0));
        label_54->setText(QApplication::translate("ZhouYuEditor", "\347\253\240\350\212\202\345\220\215", 0));
        label_53->setText(QApplication::translate("ZhouYuEditor", "\347\253\240\350\212\202\346\217\217\350\277\260", 0));
        segmentAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        segmentDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        segmentUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        segmentDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        groupBox_15->setTitle(QApplication::translate("ZhouYuEditor", "\346\256\265\350\220\275\345\261\236\346\200\247", 0));
        label_11->setText(QApplication::translate("ZhouYuEditor", "\346\256\265\350\220\275\345\220\215", 0));
        label_12->setText(QApplication::translate("ZhouYuEditor", "\350\265\267\345\247\213\346\225\205\344\272\213", 0));
        label_13->setText(QApplication::translate("ZhouYuEditor", "\347\273\210\346\255\242\346\225\205\344\272\213", 0));
        label_18->setText(QApplication::translate("ZhouYuEditor", "\346\225\205\344\272\213\344\273\254", 0));
        storyAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        storyDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        storyUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        storyDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        label_22->setText(QApplication::translate("ZhouYuEditor", "\350\257\246\346\203\205", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_story), QApplication::translate("ZhouYuEditor", "\345\211\247\346\234\254", 0));
        magicAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        magicDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        magicUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        magicDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        groupBox_13->setTitle(QApplication::translate("ZhouYuEditor", "\345\205\261\346\234\211\345\261\236\346\200\247", 0));
        label_71->setText(QApplication::translate("ZhouYuEditor", "\345\220\215\347\247\260", 0));
        label_47->setText(QApplication::translate("ZhouYuEditor", "MP\346\266\210\350\200\227", 0));
        label_42->setText(QApplication::translate("ZhouYuEditor", "\346\217\217\350\277\260", 0));
        magicRadio->setText(QApplication::translate("ZhouYuEditor", "\347\255\226\347\225\245", 0));
        groupBox_10->setTitle(QApplication::translate("ZhouYuEditor", "\347\255\226\347\225\245\345\261\236\346\200\247", 0));
        label_43->setText(QApplication::translate("ZhouYuEditor", "\347\233\256\346\240\207", 0));
        magicTarget->clear();
        magicTarget->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\346\210\221\346\226\271\345\215\225\344\272\272", 0)
         << QApplication::translate("ZhouYuEditor", "\346\210\221\346\226\271\345\205\250\344\275\223", 0)
         << QApplication::translate("ZhouYuEditor", "\346\225\214\346\226\271\345\215\225\344\272\272", 0)
         << QApplication::translate("ZhouYuEditor", "\346\225\214\346\226\271\345\205\250\344\275\223", 0)
        );
        label_45->setText(QApplication::translate("ZhouYuEditor", "HP", 0));
        label_87->setText(QApplication::translate("ZhouYuEditor", "\346\224\273", 0));
        label_88->setText(QApplication::translate("ZhouYuEditor", "\351\230\262", 0));
        label_89->setText(QApplication::translate("ZhouYuEditor", "\346\225\217", 0));
        label_90->setText(QApplication::translate("ZhouYuEditor", "\345\221\275\344\270\255", 0));
        label_48->setText(QApplication::translate("ZhouYuEditor", "\347\261\273\345\236\213", 0));
        magicType->clear();
        magicType->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\346\260\264\350\256\241", 0)
         << QApplication::translate("ZhouYuEditor", "\347\201\253\350\256\241", 0)
         << QApplication::translate("ZhouYuEditor", "\347\237\263\350\256\241", 0)
         << QApplication::translate("ZhouYuEditor", "\350\241\214\345\206\233", 0)
         << QApplication::translate("ZhouYuEditor", "\346\222\244\351\200\200", 0)
         << QApplication::translate("ZhouYuEditor", "\345\242\236\345\212\240\350\276\205\345\212\251", 0)
         << QApplication::translate("ZhouYuEditor", "\345\207\217\345\260\221\350\276\205\345\212\251", 0)
         << QApplication::translate("ZhouYuEditor", "\345\201\234\346\255\242\346\224\273\345\207\273", 0)
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\350\207\252\346\210\221", 0)
        );
        label_74->setText(QApplication::translate("ZhouYuEditor", "\346\214\201\347\273\255\345\233\236\345\220\210", 0));
        label_75->setText(QApplication::translate("ZhouYuEditor", "~", 0));
        arrayRadio->setText(QApplication::translate("ZhouYuEditor", "\351\230\265", 0));
        groupBox_11->setTitle(QApplication::translate("ZhouYuEditor", "\351\230\265\345\236\213\345\261\236\346\200\247(\345\242\236\345\212\240\347\231\276\345\210\206\346\257\224)", 0));
        label_44->setText(QApplication::translate("ZhouYuEditor", "\344\275\215\347\275\2560", 0));
        arrayProp0->clear();
        arrayProp0->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273+", 0)
         << QApplication::translate("ZhouYuEditor", "\351\230\262\345\276\241+", 0)
         << QApplication::translate("ZhouYuEditor", "\345\221\275\344\270\255+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\225\217\346\215\267+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\345\233\236\351\201\277+", 0)
         << QApplication::translate("ZhouYuEditor", "\347\255\226\347\225\245\345\233\236\351\201\277+", 0)
        );
        label_73->setText(QApplication::translate("ZhouYuEditor", "~", 0));
        label_46->setText(QApplication::translate("ZhouYuEditor", "\344\275\215\347\275\2561", 0));
        arrayProp1->clear();
        arrayProp1->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273+", 0)
         << QApplication::translate("ZhouYuEditor", "\351\230\262\345\276\241+", 0)
         << QApplication::translate("ZhouYuEditor", "\345\221\275\344\270\255+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\225\217\346\215\267+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\345\233\236\351\201\277+", 0)
         << QApplication::translate("ZhouYuEditor", "\347\255\226\347\225\245\345\233\236\351\201\277+", 0)
        );
        label_76->setText(QApplication::translate("ZhouYuEditor", "~", 0));
        label_49->setText(QApplication::translate("ZhouYuEditor", "\344\275\215\347\275\2562", 0));
        arrayProp2->clear();
        arrayProp2->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273+", 0)
         << QApplication::translate("ZhouYuEditor", "\351\230\262\345\276\241+", 0)
         << QApplication::translate("ZhouYuEditor", "\345\221\275\344\270\255+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\225\217\346\215\267+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\345\233\236\351\201\277+", 0)
         << QApplication::translate("ZhouYuEditor", "\347\255\226\347\225\245\345\233\236\351\201\277+", 0)
        );
        label_77->setText(QApplication::translate("ZhouYuEditor", "~", 0));
        label_52->setText(QApplication::translate("ZhouYuEditor", "\344\275\215\347\275\2563", 0));
        arrayProp3->clear();
        arrayProp3->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273+", 0)
         << QApplication::translate("ZhouYuEditor", "\351\230\262\345\276\241+", 0)
         << QApplication::translate("ZhouYuEditor", "\345\221\275\344\270\255+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\225\217\346\215\267+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\345\233\236\351\201\277+", 0)
         << QApplication::translate("ZhouYuEditor", "\351\230\262\345\276\241\345\233\236\351\201\277+", 0)
        );
        label_78->setText(QApplication::translate("ZhouYuEditor", "~", 0));
        label_55->setText(QApplication::translate("ZhouYuEditor", "\344\275\215\347\275\2564", 0));
        arrayProp4->clear();
        arrayProp4->insertItems(0, QStringList()
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273+", 0)
         << QApplication::translate("ZhouYuEditor", "\351\230\262\345\276\241+", 0)
         << QApplication::translate("ZhouYuEditor", "\345\221\275\344\270\255+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\225\217\346\215\267+", 0)
         << QApplication::translate("ZhouYuEditor", "\346\224\273\345\207\273\345\233\236\351\201\277+", 0)
         << QApplication::translate("ZhouYuEditor", "\347\255\226\347\225\245\345\233\236\351\201\277+", 0)
        );
        label_79->setText(QApplication::translate("ZhouYuEditor", "~", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_skill), QApplication::translate("ZhouYuEditor", "\347\255\226\347\225\245\344\270\216\351\230\265", 0));
        mapAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        mapDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        mapUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        mapDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        label_67->setText(QApplication::translate("ZhouYuEditor", "\345\234\260\345\233\276\345\220\215\347\247\260", 0));
        label_80->setText(QApplication::translate("ZhouYuEditor", "\345\234\260\345\233\276\350\265\204\346\272\220\345\220\215", 0));
        label_69->setText(QApplication::translate("ZhouYuEditor", "\345\234\260\345\233\276\344\273\213\347\273\215", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_map), QApplication::translate("ZhouYuEditor", "\345\234\260\345\233\276", 0));
        battleAddButton->setText(QApplication::translate("ZhouYuEditor", "\345\242\236", 0));
        battleDelButton->setText(QApplication::translate("ZhouYuEditor", "\345\210\240", 0));
        battleUpButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\212", 0));
        battleDownButton->setText(QApplication::translate("ZhouYuEditor", "\344\270\213", 0));
        label_56->setText(QApplication::translate("ZhouYuEditor", "\346\210\230\345\275\271\345\220\215\347\247\260", 0));
        label_86->setText(QApplication::translate("ZhouYuEditor", "\350\204\232\346\234\254\345\220\215", 0));
        label_57->setText(QApplication::translate("ZhouYuEditor", "\346\225\214\346\226\2710", 0));
        label_59->setText(QApplication::translate("ZhouYuEditor", "\347\255\211\347\272\247", 0));
        label_58->setText(QApplication::translate("ZhouYuEditor", "\346\225\214\346\226\2711", 0));
        label_60->setText(QApplication::translate("ZhouYuEditor", "\347\255\211\347\272\247", 0));
        label_62->setText(QApplication::translate("ZhouYuEditor", "\346\225\214\346\226\2712", 0));
        label_61->setText(QApplication::translate("ZhouYuEditor", "\347\255\211\347\272\247", 0));
        label_64->setText(QApplication::translate("ZhouYuEditor", "\346\225\214\346\226\2713", 0));
        label_63->setText(QApplication::translate("ZhouYuEditor", "\347\255\211\347\272\247", 0));
        label_66->setText(QApplication::translate("ZhouYuEditor", "\346\225\214\346\226\2714", 0));
        label_65->setText(QApplication::translate("ZhouYuEditor", "\347\255\211\347\272\247", 0));
        label_68->setText(QApplication::translate("ZhouYuEditor", "\346\210\230\345\275\271\350\257\264\346\230\216", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_battle), QApplication::translate("ZhouYuEditor", "\346\210\230\345\275\271", 0));
    } // retranslateUi

};

namespace Ui {
    class ZhouYuEditor: public Ui_ZhouYuEditor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZHOUYUEDITOR_H

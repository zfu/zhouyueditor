/****************************************************************************
** Meta object code from reading C++ file 'ZhouYuEditor.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "ZhouYuEditor.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ZhouYuEditor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ZhouYuEditor_t {
    QByteArrayData data[39];
    char stringdata[609];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ZhouYuEditor_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ZhouYuEditor_t qt_meta_stringdata_ZhouYuEditor = {
    {
QT_MOC_LITERAL(0, 0, 12),
QT_MOC_LITERAL(1, 13, 11),
QT_MOC_LITERAL(2, 25, 0),
QT_MOC_LITERAL(3, 26, 16),
QT_MOC_LITERAL(4, 43, 19),
QT_MOC_LITERAL(5, 63, 3),
QT_MOC_LITERAL(6, 67, 17),
QT_MOC_LITERAL(7, 85, 18),
QT_MOC_LITERAL(8, 104, 19),
QT_MOC_LITERAL(9, 124, 17),
QT_MOC_LITERAL(10, 142, 17),
QT_MOC_LITERAL(11, 160, 21),
QT_MOC_LITERAL(12, 182, 21),
QT_MOC_LITERAL(13, 204, 21),
QT_MOC_LITERAL(14, 226, 19),
QT_MOC_LITERAL(15, 246, 20),
QT_MOC_LITERAL(16, 267, 19),
QT_MOC_LITERAL(17, 287, 16),
QT_MOC_LITERAL(18, 304, 11),
QT_MOC_LITERAL(19, 316, 24),
QT_MOC_LITERAL(20, 341, 7),
QT_MOC_LITERAL(21, 349, 9),
QT_MOC_LITERAL(22, 359, 9),
QT_MOC_LITERAL(23, 369, 8),
QT_MOC_LITERAL(24, 378, 10),
QT_MOC_LITERAL(25, 389, 16),
QT_MOC_LITERAL(26, 406, 16),
QT_MOC_LITERAL(27, 423, 15),
QT_MOC_LITERAL(28, 439, 17),
QT_MOC_LITERAL(29, 457, 16),
QT_MOC_LITERAL(30, 474, 16),
QT_MOC_LITERAL(31, 491, 15),
QT_MOC_LITERAL(32, 507, 17),
QT_MOC_LITERAL(33, 525, 14),
QT_MOC_LITERAL(34, 540, 14),
QT_MOC_LITERAL(35, 555, 13),
QT_MOC_LITERAL(36, 569, 15),
QT_MOC_LITERAL(37, 585, 16),
QT_MOC_LITERAL(38, 602, 6)
    },
    "ZhouYuEditor\0itemClicked\0\0QListWidgetItem*\0"
    "actorListRowChanged\0row\0npcListRowChanged\0"
    "itemListRowChanged\0magicListRowChanged\0"
    "mapListRowChanged\0loadButtonClicked\0"
    "actorOffsetRowChanged\0chapterListRowChanged\0"
    "segmentListRowChanged\0storyListRowChanged\0"
    "battleListRowChanged\0arrayPropRowChanged\0"
    "editingFinished0\0storeConfig\0"
    "editingFinished_noupdate\0saveAll\0"
    "addButton\0delButton\0upButton\0downButton\0"
    "addChapterButton\0delChapterButton\0"
    "upChapterButton\0downChapterButton\0"
    "addSegmentButton\0delSegmentButton\0"
    "upSegmentButton\0downSegmentButton\0"
    "addStoryButton\0delStoryButton\0"
    "upStoryButton\0downStoryButton\0"
    "updateActorValue\0getDir"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ZhouYuEditor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  189,    2, 0x0a /* Public */,
       4,    1,  192,    2, 0x0a /* Public */,
       6,    1,  195,    2, 0x0a /* Public */,
       7,    1,  198,    2, 0x0a /* Public */,
       8,    1,  201,    2, 0x0a /* Public */,
       9,    1,  204,    2, 0x0a /* Public */,
      10,    1,  207,    2, 0x0a /* Public */,
      11,    1,  210,    2, 0x0a /* Public */,
      12,    1,  213,    2, 0x0a /* Public */,
      13,    1,  216,    2, 0x0a /* Public */,
      14,    1,  219,    2, 0x0a /* Public */,
      15,    1,  222,    2, 0x0a /* Public */,
      16,    1,  225,    2, 0x0a /* Public */,
      17,    0,  228,    2, 0x0a /* Public */,
      18,    0,  229,    2, 0x0a /* Public */,
      19,    0,  230,    2, 0x0a /* Public */,
      20,    0,  231,    2, 0x0a /* Public */,
      21,    0,  232,    2, 0x0a /* Public */,
      22,    0,  233,    2, 0x0a /* Public */,
      23,    0,  234,    2, 0x0a /* Public */,
      24,    0,  235,    2, 0x0a /* Public */,
      25,    0,  236,    2, 0x0a /* Public */,
      26,    0,  237,    2, 0x0a /* Public */,
      27,    0,  238,    2, 0x0a /* Public */,
      28,    0,  239,    2, 0x0a /* Public */,
      29,    0,  240,    2, 0x0a /* Public */,
      30,    0,  241,    2, 0x0a /* Public */,
      31,    0,  242,    2, 0x0a /* Public */,
      32,    0,  243,    2, 0x0a /* Public */,
      33,    0,  244,    2, 0x0a /* Public */,
      34,    0,  245,    2, 0x0a /* Public */,
      35,    0,  246,    2, 0x0a /* Public */,
      36,    0,  247,    2, 0x0a /* Public */,
      37,    0,  248,    2, 0x0a /* Public */,
      38,    0,  249,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ZhouYuEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ZhouYuEditor *_t = static_cast<ZhouYuEditor *>(_o);
        switch (_id) {
        case 0: _t->itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 1: _t->actorListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->npcListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->itemListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->magicListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->mapListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->loadButtonClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->actorOffsetRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->chapterListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->segmentListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->storyListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->battleListRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->arrayPropRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->editingFinished0(); break;
        case 14: _t->storeConfig(); break;
        case 15: _t->editingFinished_noupdate(); break;
        case 16: _t->saveAll(); break;
        case 17: _t->addButton(); break;
        case 18: _t->delButton(); break;
        case 19: _t->upButton(); break;
        case 20: _t->downButton(); break;
        case 21: _t->addChapterButton(); break;
        case 22: _t->delChapterButton(); break;
        case 23: _t->upChapterButton(); break;
        case 24: _t->downChapterButton(); break;
        case 25: _t->addSegmentButton(); break;
        case 26: _t->delSegmentButton(); break;
        case 27: _t->upSegmentButton(); break;
        case 28: _t->downSegmentButton(); break;
        case 29: _t->addStoryButton(); break;
        case 30: _t->delStoryButton(); break;
        case 31: _t->upStoryButton(); break;
        case 32: _t->downStoryButton(); break;
        case 33: _t->updateActorValue(); break;
        case 34: _t->getDir(); break;
        default: ;
        }
    }
}

const QMetaObject ZhouYuEditor::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_ZhouYuEditor.data,
      qt_meta_data_ZhouYuEditor,  qt_static_metacall, 0, 0}
};


const QMetaObject *ZhouYuEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ZhouYuEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ZhouYuEditor.stringdata))
        return static_cast<void*>(const_cast< ZhouYuEditor*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int ZhouYuEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 35)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 35;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

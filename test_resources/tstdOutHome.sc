#本文件由手工编写来设计剧情与控制地图切换
ENTER:

setGameState("changeMap");
setEnableMeetBattle(false);

MAP:
ifeq($doorName,0,"DOOR0");
ifeq($doorName,1,"DOOR1");
ifeq($doorName,2,"DOOR2");

#游戏开始
DOOR0:
loadMap("tstdOutHome.tmx");
setMapName("小野村庄");
#setMapNameWithID(3);
loadHero("down",6,8);
fadeInWithTime(1.0);
setGameState("game");
call("begin.sc");
goto("EXIT");

#从小房子出来
DOOR1:
loadMap("tstdOutHome.tmx");
setMapName("小野村庄");
#setMapNameWithID(3);
loadHero("down",6,3);
fadeInWithTime(1.0);
setGameState("game");
goto("EXIT");

#从大地图回村
DOOR2:
loadMap("tstdOutHome.tmx");
setMapName("小野村庄");
#setMapNameWithID(3);
loadHero("up",7,29);
fadeInWithTime(1.0);
setGameState("game");
goto("EXIT");

EXIT:

END:

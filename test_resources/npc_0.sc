setGameState("invalid");

ifnot($2,0,"AGAIN");
setGameState("dialog");
talk("孙坚", right, "先来场战斗吧");
goto("BATTLE");

AGAIN:
setGameState("dialog");
talk("孙坚", right, "再来场战斗吧");

BATTLE:
fadeOutWithTime(0.5);
startBattle(0);
setGameState("invalid");
fadeInWithTime(0.5);

ifeq($battleWin,1,"WIN");

FAILED:
setGameState("dialog");
talk("孙坚", right, "唉战斗失败，再试一次吧");
goto("BATTLE");

WIN:
setGameState("dialog");
talk("孙坚", right, "哈哈，你胜了", "谨记");
moveNPCTo(0,13,20,true);
setNPCDirection(0,"right");
goto("EXIT");

EXIT:
setValue($2,1);
setGameState("game");

#说明:
#本文件由手工编写来设计剧情与控制地图切换
#这种                以井号开头的为注释，不会被执行
#LABEL:             这个带冒号的表示标签,用于跳转
#ifeq(A,B,"LABEL")  用于条件跳转,表示如果A=B则跳转到标签LABEL执行
#ifne(A,B,"LABEL")  用于条件跳转,表示如果A!=B则跳转到标签LABEL执行
#setGameState       为改变游戏的状态，如game游戏状态，dialog为对话状态，在对话状态下不能移动主角
#fadeOut            表示在多少秒内淡出
#fadeIn             表示在多少秒内淡入
#talk               表示-->对话(人物, 头像方向, 对话内容1, 对话内容2)
#goto               表示无条件直接跳转到标签处执行

ENTRY:
###########   新游戏的开始   ###############
setGameState("invalid");
#$2仅用于临时检查战斗用
setValue($2,0);
setEnableMeetBattle(false);
fadeInWithTime(0.5);
loadMap("home_0_0.tmx");
setMapName("孙坚家中");
#setMapNameWithID(0);
addHero(15);
activeHero(15);
setMapHero(15);
setAdviser(-1);
setPoineer(-1);
loadHero("right",14,10);
loadNPC(0,"left",15,10);
loadNPC(1,"down",34,6);
fadeInWithTime(2.0);

setGameState("dialog");
talk("孙坚", right, "策儿，为父将带兵与十七路诸侯讨伐逆贼董卓", "你已长大成人，在家照顾好母亲和弟弟们.", "还有，这里有我前些年请名匠打造的利器一把，就交给你了");
setGameState("info");
info("得到[霸王戟]");
info("得到[1000两]");
#往右移动孙坚,移动完后才能走
setGameState("scriptMove");
moveNPCTo(0,15,11,true);
moveNPCTo(0,14,11,true);
moveNPCTo(0,14,20,true);
moveNPCTo(1,34,7,false);
moveHeroTo(14,13);
moveHeroTo(33,13);
moveHeroTo(33,7);
setNPCDirection(0,"up");
setNPCDirection(1,"left");
setHeroDirection("right");

#和孙母对话
setGameState("dialog");
talk("孙母", right, "孙儿啊，我们刚搬来舒城，你应该出门走走，拜访当地名人，请教学识", "这些物品务必好好利用");
setGameState("info");
info("得到[皮弁]");
addItem(3,1);
info("得到[护心兜]");
addItem(9,1);
info("得到[短靴]");
addItem(15,1);
setValue($0,1);
setGameState("game");
goto("EXIT");

EXIT:

END:

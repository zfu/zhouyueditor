
ENTER:

setGameState("changeMap");
setEnableMeetBattle(true);

MAP:
ifeq($doorName, 0, "DOOR0");
ifeq($doorName, 1, "DOOR1");
ifeq($doorName, 2, "DOOR2");

DOOR0:

load:
fadeOutWithTime(1.0);
loadMap("tstd_world.tmx");
setMapName("舒城外郊");
#setMapNameWithID(2);
loadHero("down",26,43);
setGameState("game");
fadeInWithTime(1.0);
goto("EXIT");

DOOR1:
fadeOutWithTime(1.0);
loadMap("tstd_world.tmx");
setMapName("舒城外郊");
#setMapNameWithID(2);
loadHero("down",59,35);
setGameState("game");
fadeInWithTime(1.0);
goto("EXIT");

DOOR2:
fadeOutWithTime(1.0);
loadMap("tstd_world.tmx");
setMapName("舒城外郊");
#setMapNameWithID(2);
loadHero("down",39,33);
setGameState("game");
fadeInWithTime(1.0);
goto("EXIT");

EXIT:

END:


ENTER:

#从下面的门进入
DOOR:
setGameState("invalid");
fadeOutWithTime(1.0);
setEnableMeetBattle(false);
loadMap("home_0_0.tmx");
setMapName("孙坚家中");
#setMapNameWithID(0);
loadHero("up",14,23);
#再次进入就只有孙母在家了
loadNPC(1,"down",34,6);
setGameState("game");
fadeInWithTime(1.0);

EXIT:

END:

#include "ZhouYuEditor.h"
#include "ui_ZhouYuEditor.h"

#include <QtWidgets/QMessageBox>
#include <QtWidgets/QFileDialog>

ZhouYuEditor::ZhouYuEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ZhouYuEditor)
{
    needSave=false;
    ui->setupUi(this);
    _data = new Data();
    arrayComb.push_back(ui->arrayProp0);
    arrayComb.push_back(ui->arrayProp1);
    arrayComb.push_back(ui->arrayProp2);
    arrayComb.push_back(ui->arrayProp3);
    arrayComb.push_back(ui->arrayProp4);
    arrayMin.push_back(ui->arrayValue0);
    arrayMin.push_back(ui->arrayValue1);
    arrayMin.push_back(ui->arrayValue2);
    arrayMin.push_back(ui->arrayValue3);
    arrayMin.push_back(ui->arrayValue4);
    arrayMax.push_back(ui->arrayValueMax0);
    arrayMax.push_back(ui->arrayValueMax1);
    arrayMax.push_back(ui->arrayValueMax2);
    arrayMax.push_back(ui->arrayValueMax3);
    arrayMax.push_back(ui->arrayValueMax4);
    ui->pathEdit->setText(_data->_path.c_str());
    connect(ui->selectDirButton, SIGNAL(clicked(bool)), this, SLOT(getDir()));
    connect(ui->actorListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(actorListRowChanged(int)));
    connect(ui->npcListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(npcListRowChanged(int)));
    
    connect(ui->itemListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(itemListRowChanged(int)));
    
    connect(ui->magicListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(magicListRowChanged(int)));
    
    connect(ui->mapListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(mapListRowChanged(int)));
    connect(ui->loadButton, SIGNAL(clicked(bool)), this, SLOT(loadButtonClicked(bool)));
    loadButtonClicked(true);

    connect(ui->actorOffset, SIGNAL(currentIndexChanged(int)), this, SLOT(actorOffsetRowChanged(int)));
    
    connect(ui->chapterListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(chapterListRowChanged(int)));
    connect(ui->segmentListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(segmentListRowChanged(int)));
    connect(ui->subStoryListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(storyListRowChanged(int)));
    connect(ui->battleListWidget, SIGNAL(currentRowChanged(int)), this, SLOT(battleListRowChanged(int)));
    connect(ui->arrayProp0, SIGNAL(currentIndexChanged(int)), this, SLOT(arrayPropRowChanged(int)));
    connect(ui->arrayProp1, SIGNAL(currentIndexChanged(int)), this, SLOT(arrayPropRowChanged(int)));
    connect(ui->arrayProp2, SIGNAL(currentIndexChanged(int)), this, SLOT(arrayPropRowChanged(int)));
    connect(ui->arrayProp3, SIGNAL(currentIndexChanged(int)), this, SLOT(arrayPropRowChanged(int)));
    connect(ui->arrayProp4, SIGNAL(currentIndexChanged(int)), this, SLOT(arrayPropRowChanged(int)));
    connect(ui->saveButton, SIGNAL(clicked(bool)), this, SLOT(saveAll()));
    connectFinished();
    connectButton();
    connect(ui->actorISlider, SIGNAL(valueChanged(int)), this, SLOT(updateActorValue()));
    connect(ui->actorLSlider, SIGNAL(valueChanged(int)), this, SLOT(updateActorValue()));
    connect(ui->actorSSlider, SIGNAL(valueChanged(int)), this, SLOT(updateActorValue()));
    connect(ui->actorASlider, SIGNAL(valueChanged(int)), this, SLOT(updateActorValue()));
    
    loadBattle(true, true);
    ui->actorListWidget->setCurrentRow(0);
    ui->actorOffset->setCurrentIndex(0);
    ui->itemListWidget->setCurrentRow(0);
    ui->magicListWidget->setCurrentRow(0);
    ui->npcListWidget->setCurrentRow(0);
    ui->mapListWidget->setCurrentRow(0);
    ui->chapterListWidget->setCurrentRow(0);
    ui->segmentListWidget->setCurrentRow(0);
    ui->subStoryListWidget->setCurrentRow(0);
    ui->battleListWidget->setCurrentRow(0);
    cout<<"________________APP start_______________"<<endl;
}

ZhouYuEditor::~ZhouYuEditor()
{
    delete ui;
    delete _data;
}

void ZhouYuEditor::connectButton()
{
    //add
    connect(ui->actorAddButton, SIGNAL(clicked(bool)), this, SLOT(addButton()));
    connect(ui->npcAddButton, SIGNAL(clicked(bool)), this, SLOT(addButton()));
    connect(ui->itemAddButton, SIGNAL(clicked(bool)), this, SLOT(addButton()));
    connect(ui->magicAddButton, SIGNAL(clicked(bool)), this, SLOT(addButton()));
    connect(ui->mapAddButton, SIGNAL(clicked(bool)), this, SLOT(addButton()));
    connect(ui->battleAddButton, SIGNAL(clicked(bool)), this, SLOT(addButton()));
    
    //del
    connect(ui->actorDelButton, SIGNAL(clicked(bool)), this, SLOT(delButton()));
    connect(ui->npcDelButton, SIGNAL(clicked(bool)), this, SLOT(delButton()));
    connect(ui->itemDelButton, SIGNAL(clicked(bool)), this, SLOT(delButton()));
    connect(ui->magicDelButton, SIGNAL(clicked(bool)), this, SLOT(delButton()));
    connect(ui->mapDelButton, SIGNAL(clicked(bool)), this, SLOT(delButton()));
    connect(ui->battleDelButton, SIGNAL(clicked(bool)), this, SLOT(delButton()));
    
    //up
    connect(ui->actorUpButton, SIGNAL(clicked(bool)), this, SLOT(upButton()));
    connect(ui->npcUpButton, SIGNAL(clicked(bool)), this, SLOT(upButton()));
    connect(ui->itemUpButton, SIGNAL(clicked(bool)), this, SLOT(upButton()));
    connect(ui->magicUpButton, SIGNAL(clicked(bool)), this, SLOT(upButton()));
    connect(ui->mapUpButton, SIGNAL(clicked(bool)), this, SLOT(upButton()));
    connect(ui->battleUpButton, SIGNAL(clicked(bool)), this, SLOT(upButton()));
    
    //down
    connect(ui->actorDownButton, SIGNAL(clicked(bool)), this, SLOT(downButton()));
    connect(ui->npcDownButton, SIGNAL(clicked(bool)), this, SLOT(downButton()));
    connect(ui->itemDownButton, SIGNAL(clicked(bool)), this, SLOT(downButton()));
    connect(ui->magicDownButton, SIGNAL(clicked(bool)), this, SLOT(downButton()));
    connect(ui->mapDownButton, SIGNAL(clicked(bool)), this, SLOT(downButton()));
    connect(ui->battleDownButton, SIGNAL(clicked(bool)), this, SLOT(downButton()));
    
    //drama
    connect(ui->chapterAddButton, SIGNAL(clicked(bool)), this, SLOT(addChapterButton()));
    connect(ui->chapterDelButton, SIGNAL(clicked(bool)), this, SLOT(delChapterButton()));
    connect(ui->chapterUpButton, SIGNAL(clicked(bool)), this, SLOT(upChapterButton()));
    connect(ui->chapterDownButton, SIGNAL(clicked(bool)), this, SLOT(downChapterButton()));
                                                                         
    connect(ui->segmentAddButton, SIGNAL(clicked(bool)), this, SLOT(addSegmentButton()));
    connect(ui->segmentDelButton, SIGNAL(clicked(bool)), this, SLOT(delSegmentButton()));
    connect(ui->segmentUpButton, SIGNAL(clicked(bool)), this, SLOT(upSegmentButton()));
    connect(ui->segmentDownButton, SIGNAL(clicked(bool)), this, SLOT(downSegmentButton()));
                                                                         
    connect(ui->storyAddButton, SIGNAL(clicked(bool)), this, SLOT(addStoryButton()));
    connect(ui->storyDelButton, SIGNAL(clicked(bool)), this, SLOT(delStoryButton()));
    connect(ui->storyUpButton, SIGNAL(clicked(bool)), this, SLOT(upStoryButton()));
    connect(ui->storyDownButton, SIGNAL(clicked(bool)), this, SLOT(downStoryButton()));
}                                                                      
void ZhouYuEditor::connectFinished()
{
    //connect(ui->pathEdit, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorNameEdit, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorName2Edit, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorResName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorBattleResName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorEquipName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorOffsetX, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorOffsetY, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorSPName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorSPMin, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorSPMax, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->actorAttackTimes, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->npcName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->npcResName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->npcScriptName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->itemName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipAttack, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipDefend, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipI, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipS, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipL, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipA, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipNeedI, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipNeedS, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipNeedL, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->equipNeedA, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->supplyHP, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->supplyHPRound, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->supplyMP, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->supplyMPRound, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->attackHP, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->attackHPRound, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->storyName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->chapterName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->segmentName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->segmentStart, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->segmentEnd, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->subStoryDetail, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->magicName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->magicMP, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->magicHP, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->magicRound1, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->magicRound2, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValue0, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValueMax0, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValue1, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValueMax1, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValue2, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValueMax2, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValue3, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValueMax3, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValue4, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->arrayValueMax4, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->mapName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->mapResName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->battleName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->battleScriptName, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->battleLevel0, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->battleLevel1, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->battleLevel2, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->battleLevel3, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    connect(ui->battleLevel4, SIGNAL(textEdited(QString)), this, SLOT(editingFinished0()));
    
    //comments
    connect(ui->itemComment, SIGNAL(textChanged()), this, SLOT(editingFinished_noupdate()));
    connect(ui->storyComment, SIGNAL(textChanged()), this, SLOT(editingFinished_noupdate()));
    connect(ui->chapterComment, SIGNAL(textChanged()), this, SLOT(editingFinished_noupdate()));
    connect(ui->magicComment, SIGNAL(textChanged()), this, SLOT(editingFinished_noupdate()));
    connect(ui->mapComment, SIGNAL(textChanged()), this, SLOT(editingFinished_noupdate()));
    connect(ui->battleComment, SIGNAL(textChanged()), this, SLOT(editingFinished_noupdate()));
    connect(ui->actorSPComment, SIGNAL(textChanged()), this, SLOT(editingFinished_noupdate()));
}
void ZhouYuEditor::editingFinished0()
{
    setNeedSave(true);
}
void ZhouYuEditor::storeConfig()
{
    int tabIndex = ui->tabWidget->currentIndex();
    cout<<"tab index: "<<tabIndex<<endl;
    if (tabIndex==1){storeActorData();loadTable();}
    else if (tabIndex==2){storeNPCData();loadTable();}
    else if (tabIndex==3){storeItemData();}
    else if (tabIndex==4){storeStory();}
    else if (tabIndex==5){storeMagic();}
    else if (tabIndex==6){storeMap();}
    else if (tabIndex==7){storeBattle();}
}
void ZhouYuEditor::editingFinished_noupdate()
{
    setNeedSave(true);
}

void ZhouYuEditor::setNeedSave(bool save)
{
    needSave=save;
    if (needSave){
        this->setWindowTitle("周郎传编辑器[未保存]");
    }else{
        this->setWindowTitle("周郞传编辑器");
    }
}
void ZhouYuEditor::loadTable()
{
    ui->actorTableWidget->clearContents();
    ui->actorTableWidget->setRowCount(_data->_ActorDatas.size());
    ui->actorTableWidget->verticalHeader()->setVisible(false);
    ui->actorTableWidget->horizontalHeader()->setVisible(true);
    ui->actorTableWidget->resizeColumnToContents(0);
    ui->actorTableWidget->resizeColumnToContents(3);
    ui->actorTableWidget->resizeColumnToContents(4);
    ui->actorTableWidget->resizeColumnToContents(5);
    ui->actorTableWidget->resizeColumnToContents(6);
    for (size_t i=0; i<_data->_ActorDatas.size(); i++){
        QTableWidgetItem *item0 = new QTableWidgetItem();
        item0->setData(Qt::DisplayRole, _data->_ActorDatas[i].index);
        ui->actorTableWidget->setItem(i, 0, item0);
        
        ui->actorTableWidget->setItem(i, 1,
                                      new QTableWidgetItem(QString("%1").arg(_data->_ActorDatas[i].dName.c_str())));
        ui->actorTableWidget->setItem(i, 2,
                                      new QTableWidgetItem(QString("%1").arg(_data->_ActorDatas[i].dName2.c_str())));
        
        QTableWidgetItem *item3 = new QTableWidgetItem();
        item3->setData(Qt::DisplayRole, _data->_ActorDatas[i].I);
        ui->actorTableWidget->setItem(i, 3, item3);
        
        QTableWidgetItem *item4 = new QTableWidgetItem();
        item4->setData(Qt::DisplayRole, _data->_ActorDatas[i].S);
        ui->actorTableWidget->setItem(i, 4, item4);
        
        QTableWidgetItem *item5 = new QTableWidgetItem();
        item5->setData(Qt::DisplayRole, _data->_ActorDatas[i].L);
        ui->actorTableWidget->setItem(i, 5, item5);
        
        QTableWidgetItem *item6 = new QTableWidgetItem();
        item6->setData(Qt::DisplayRole, _data->_ActorDatas[i].A);
        ui->actorTableWidget->setItem(i, 6, item6);
    }
}
void ZhouYuEditor::loadActorData(bool loadList, size_t offsetindex)
{
    //load list
    int index = ui->actorListWidget->currentRow();
    if (loadList){
        ui->actorListWidget->clear();
        for (size_t i=0; i<_data->_ActorDatas.size(); i++){
            QString item = QString("%1\t%2")
                            .arg(_data->_ActorDatas[i].index)
                            .arg(_data->_ActorDatas[i].dName.c_str());
            ui->actorListWidget->addItem(item);
        }
    }
    ui->actorISlider->setMaximum(100);
    ui->actorSSlider->setMaximum(100);
    ui->actorLSlider->setMaximum(100);
    ui->actorASlider->setMaximum(100);
    if (index != -1){
        ui->actorNameEdit->setText(_data->_ActorDatas[index].dName.c_str());
        ui->actorName2Edit->setText(_data->_ActorDatas[index].dName2.c_str());
        ui->actorResName->setText(_data->_ActorDatas[index].resName.c_str());
        ui->actorEquipName->setText(_data->_ActorDatas[index].wName.c_str());
        ui->actorBattleResName->setText(_data->_ActorDatas[index].battleResName.c_str());
        
        ui->actorISlider->setValue(_data->_ActorDatas[index].I);
        ui->actorSSlider->setValue(_data->_ActorDatas[index].S);
        ui->actorLSlider->setValue(_data->_ActorDatas[index].L);
        ui->actorASlider->setValue(_data->_ActorDatas[index].A);
        ui->actorI->setText(QString("%1").arg(_data->_ActorDatas[index].I));
        ui->actorS->setText(QString("%1").arg(_data->_ActorDatas[index].S));
        ui->actorL->setText(QString("%1").arg(_data->_ActorDatas[index].L));
        ui->actorA->setText(QString("%1").arg(_data->_ActorDatas[index].A));
        
        ui->actorHasSkill->setChecked(_data->_ActorDatas[index].hasSk);
        if (_data->_ActorDatas[index].hasSk){
            ui->actorSPName->setText(_data->_ActorDatas[index].skName.c_str());
            ui->actorSPComment->blockSignals(true);
            ui->actorSPComment->setPlainText(_data->_ActorDatas[index].skComment.c_str());
            ui->actorSPComment->blockSignals(false);
            ui->actorSPMax->setText(QString("%1").arg(_data->_ActorDatas[index].skMax));
            ui->actorSPMin->setText(QString("%1").arg(_data->_ActorDatas[index].skMin));
            ui->actorAttackTimes->setText(QString("%1").arg(_data->_ActorDatas[index].skTime));
            ui->actorSPDelay->setText(_data->_ActorDatas[index].skDelay.c_str());
        }else{
            ui->actorSPName->setText("");
            ui->actorSPComment->blockSignals(true);
            ui->actorSPComment->setPlainText("");
            ui->actorSPComment->blockSignals(false);
            ui->actorSPMax->setText("");
            ui->actorSPMin->setText("");
            ui->actorAttackTimes->setText("");
            ui->actorSPDelay->setText("");
        }
        ui->actorWeaponType->setCurrentIndex(_data->_ActorDatas[index].weapontype);
        ui->actorOffset->setCurrentIndex(offsetindex);
        ui->actorOffsetX->setText(QString("%1").arg(_data->_ActorDatas[index].Xoffset[offsetindex]));
        ui->actorOffsetY->setText(QString("%1").arg(_data->_ActorDatas[index].Yoffset[offsetindex]));
    }else{
        ui->actorNameEdit->setText("");
        ui->actorName2Edit->setText("");
        ui->actorResName->setText("");
        ui->actorEquipName->setText("");
        ui->actorBattleResName->setText("");
        ui->actorI->setText("");
        ui->actorS->setText("");
        ui->actorL->setText("");
        ui->actorA->setText("");
        ui->actorSPName->setText("");
        ui->actorSPComment->blockSignals(true);
        ui->actorSPComment->setPlainText("");
        ui->actorSPComment->blockSignals(false);
        ui->actorSPMax->setText("");
        ui->actorSPMin->setText("");
        ui->actorAttackTimes->setText("");
        ui->actorSPDelay->setText("");
        ui->actorOffsetX->setText("");
        ui->actorOffsetY->setText("");
        ui->actorISlider->setValue(0);
        ui->actorSSlider->setValue(0);
        ui->actorLSlider->setValue(0);
        ui->actorASlider->setValue(0);
    }
    loadBattle(true, true);
}

void ZhouYuEditor::storeActorData()
{
    int index = ui->actorListWidget->currentRow();
    cout<<"store actor data "<<index<<endl;
    int offset = ui->actorOffset->currentIndex();
    if (index != -1){
        _data->_ActorDatas[index].dName = ui->actorNameEdit->text().toStdString();
        _data->_ActorDatas[index].dName2 = ui->actorName2Edit->text().toStdString();
        _data->_ActorDatas[index].resName = ui->actorResName->text().toStdString();
        _data->_ActorDatas[index].battleResName = ui->actorBattleResName->text().toStdString();
        _data->_ActorDatas[index].wName = ui->actorEquipName->text().toStdString();
        _data->_ActorDatas[index].weapontype = (ActorData::WeaponType)ui->actorWeaponType->currentIndex();
        _data->_ActorDatas[index].I = ui->actorISlider->value();
        _data->_ActorDatas[index].S = ui->actorSSlider->value();
        _data->_ActorDatas[index].L = ui->actorLSlider->value();
        _data->_ActorDatas[index].A = ui->actorASlider->value();
        
        _data->_ActorDatas[index].Xoffset[offset] = ui->actorOffsetX->text().toInt();
        _data->_ActorDatas[index].Yoffset[offset] = ui->actorOffsetY->text().toInt();
        
        _data->_ActorDatas[index].hasSk = ui->actorHasSkill->isChecked();
        if (_data->_ActorDatas[index].hasSk){
            _data->_ActorDatas[index].skName = ui->actorSPName->text().toStdString();
            _data->_ActorDatas[index].skComment = ui->actorSPComment->toPlainText().toStdString();
            _data->_ActorDatas[index].skMin = ui->actorSPMin->text().toInt();
            _data->_ActorDatas[index].skMax = ui->actorSPMax->text().toInt();
            _data->_ActorDatas[index].skTime = ui->actorAttackTimes->text().toInt();
            _data->_ActorDatas[index].skDelay = ui->actorSPDelay->text().toStdString();
        }
    }
    loadActorData(true, offset);
    ui->actorListWidget->setCurrentRow(index);
}

void ZhouYuEditor::actorOffsetRowChanged(int)
{
    int offsetindex = ui->actorOffset->currentIndex();
    loadActorData(false, offsetindex);
}

void ZhouYuEditor::itemClicked(QListWidgetItem *)
{
    loadActorData(false, 0);
}

void ZhouYuEditor::actorListRowChanged(int row)
{
    cout<<"actor row changed to"<<row<<endl;
    loadActorData(false, 0);
}

void ZhouYuEditor::loadNPCData(bool loadList)
{
    int index = ui->npcListWidget->currentRow();
    if (loadList){
        ui->npcListWidget->clear();
        for (size_t i=0; i<_data->_NPCDatas.size(); i++) {
            ui->npcListWidget->addItem(QString("%1\t%2")
                                       .arg(i)
                                       .arg(_data->_NPCDatas[i].dName.c_str()));
        }
    }
    if (index != -1){
        ui->npcName->setText(_data->_NPCDatas[index].dName.c_str());
        ui->npcResName->setText(_data->_NPCDatas[index].resName.c_str());
        ui->npcScriptName->setText(_data->_NPCDatas[index].scriptName.c_str());
    }else{
        ui->npcName->setText("");
        ui->npcResName->setText("");
        ui->npcScriptName->setText("");
    }
}
void ZhouYuEditor::storeNPCData()
{
    int index = ui->npcListWidget->currentRow();
    cout<<"store NPC data "<<index<<endl;
    if (index!= -1){
        _data->_NPCDatas[index].dName = ui->npcName->text().toStdString();
        _data->_NPCDatas[index].resName = ui->npcResName->text().toStdString();
        _data->_NPCDatas[index].scriptName = ui->npcScriptName->text().toStdString();
        loadNPCData(true);
        ui->npcListWidget->setCurrentRow(index);
    }
}
void ZhouYuEditor::npcListRowChanged(int)
{
    loadNPCData(false);
}
void ZhouYuEditor::loadItemData(bool loadList)
{
    int index = ui->itemListWidget->currentRow();
    if (loadList){
        ui->itemListWidget->clear();
        for (size_t i=0; i<_data->_ItemDatas.size(); i++){
            ui->itemListWidget->addItem(QString("%1  %2")
                                        .arg(i)
                                        .arg(_data->_ItemDatas[i].dName.c_str()));
        }
    }
    if (index != -1){
        ItemData d = _data->_ItemDatas[index];
        ui->itemName->setText(d.dName.c_str());
        ui->itemComment->blockSignals(true);
        ui->itemComment->setText(d.comment.c_str());
        ui->itemComment->blockSignals(false);
        //hide all first
        {
            ui->equipAttack->setText("");
            ui->equipDefend->setText("");
            ui->equipDefend->setText("");
            ui->equipL->setText("");
            ui->equipA->setText("");
            ui->equipI->setText("");
            ui->equipS->setText("");
            ui->equipNeedI->setText("");
            ui->equipNeedS->setText("");
            ui->equipNeedL->setText("");
            ui->equipNeedA->setText("");
            ui->supplyHP->setText("");
            ui->supplyMP->setText("");
            ui->supplyHPRound->setText("");
            ui->supplyMPRound->setText("");
            ui->attackHP->setText("");
            ui->attackHPRound->setText("");
            ui->supplyCheckBox->setChecked(false);
            ui->attackCheckBox->setChecked(false);
        }
        if (d.itemType == ItemData::kTypeEquip){
            ui->equipRadio->setChecked(true);
            ui->equipAttack->setText(QString("%1").arg(d.equipAttack));
            ui->equipDefend->setText(QString("%1").arg(d.equipDefend));
            ui->equipDefend->setText(QString("%1").arg(d.equipDefend));
            ui->equipI->setText(QString("%1").arg(d.equipI));
            ui->equipS->setText(QString("%1").arg(d.equipS));
            ui->equipL->setText(QString("%1").arg(d.equipL));
            ui->equipA->setText(QString("%1").arg(d.equipA));
            ui->equipNeedI->setText(QString("%1").arg(d.equipNeedI));
            ui->equipNeedS->setText(QString("%1").arg(d.equipNeedS));
            ui->equipNeedL->setText(QString("%1").arg(d.equipNeedL));
            ui->equipNeedA->setText(QString("%1").arg(d.equipNeedA));
            ui->itemType->setCurrentIndex(_data->_ItemDatas[index].itemEquipType);
        } else if (d.itemType == ItemData::kTypeSupply){
            ui->supplyRadio->setChecked(true);
            if (d.supplyAll) ui->supplyCheckBox->setChecked(true);
            else ui->supplyCheckBox->setChecked(false);
            ui->supplyHP->setText(QString("%1").arg(d.supplyHP));
            ui->supplyMP->setText(QString("%1").arg(d.supplyMP));
            ui->supplyHPRound->setText(QString("%1").arg(d.supplyHPRound));
            ui->supplyMPRound->setText(QString("%1").arg(d.supplyMPRound));
        } else if (d.itemType == ItemData::kTypeAttack){
            ui->attackRadio->setChecked(true);
            if (d.attackAll) ui->attackCheckBox->setChecked(true);
            else ui->attackCheckBox->setChecked(false);
            ui->attackHP->setText(QString("%1").arg(d.attackHP));
            ui->attackHPRound->setText(QString("%1").arg(d.attackHPRound));
        } else if (d.itemType == ItemData::kTypeOther){
            ui->otherRadio->setChecked(true);
        }
        ui->itemPrice->setText(QString("%1").arg(d.price));
        ui->itemEnv->setCurrentIndex(_data->_ItemDatas[index].env);
    }else{
        ui->equipAttack->setText("");
        ui->equipDefend->setText("");
        ui->equipDefend->setText("");
        ui->equipL->setText("");
        ui->equipA->setText("");
        ui->equipI->setText("");
        ui->equipS->setText("");
        ui->equipNeedI->setText("");
        ui->equipNeedS->setText("");
        ui->equipNeedL->setText("");
        ui->equipNeedA->setText("");
        ui->supplyHP->setText("");
        ui->supplyMP->setText("");
        ui->supplyHPRound->setText("");
        ui->supplyMPRound->setText("");
        ui->attackHP->setText("");
        ui->attackHPRound->setText("");
    }
}
void ZhouYuEditor::storeItemData()
{
    int index = ui->itemListWidget->currentRow();
    if (index != -1){
        _data->_ItemDatas[index].dName = ui->itemName->text().toStdString();
        _data->_ItemDatas[index].comment = ui->itemComment->toPlainText().toStdString();
        _data->_ItemDatas[index].price = ui->itemPrice->text().toInt();
        _data->_ItemDatas[index].env = (ItemData::ItemEnv)ui->itemEnv->currentIndex();
        if (ui->equipRadio->isChecked()){
            _data->_ItemDatas[index].itemType = ItemData::kTypeEquip;
            _data->_ItemDatas[index].itemEquipType = (ItemData::EquipType)ui->itemType->currentIndex();
            _data->_ItemDatas[index].equipAttack = ui->equipAttack->text().toInt();
            _data->_ItemDatas[index].equipDefend = ui->equipDefend->text().toInt();
            _data->_ItemDatas[index].equipI = ui->equipI->text().toInt();
            _data->_ItemDatas[index].equipS = ui->equipS->text().toInt();
            _data->_ItemDatas[index].equipL = ui->equipL->text().toInt();
            _data->_ItemDatas[index].equipNeedA = ui->equipNeedA->text().toInt();
            _data->_ItemDatas[index].equipNeedI = ui->equipNeedI->text().toInt();
            _data->_ItemDatas[index].equipNeedS = ui->equipNeedS->text().toInt();
            _data->_ItemDatas[index].equipNeedL = ui->equipNeedL->text().toInt();
            _data->_ItemDatas[index].equipNeedA = ui->equipNeedA->text().toInt();
        }else if (ui->supplyRadio->isChecked()){
            _data->_ItemDatas[index].itemType = ItemData::kTypeSupply;
            _data->_ItemDatas[index].supplyAll = ui->supplyCheckBox->isChecked();
            _data->_ItemDatas[index].supplyHP = ui->supplyHP->text().toInt();
            _data->_ItemDatas[index].supplyMP = ui->supplyMP->text().toInt();
            _data->_ItemDatas[index].supplyHPRound = ui->supplyHPRound->text().toInt();
            _data->_ItemDatas[index].supplyMPRound = ui->supplyMPRound->text().toInt();
        }else if (ui->attackRadio->isChecked()){
            _data->_ItemDatas[index].itemType = ItemData::kTypeAttack;
            _data->_ItemDatas[index].attackAll = ui->attackCheckBox->isChecked();
            _data->_ItemDatas[index].attackHP = ui->attackHP->text().toInt();
            _data->_ItemDatas[index].attackHPRound = ui->attackHPRound->text().toInt();
        }else if (ui->otherRadio->isChecked()){
            _data->_ItemDatas[index].itemType = ItemData::kTypeAttack;
        }
    }
    loadItemData(true);
    ui->itemListWidget->setCurrentRow(index);
}

void ZhouYuEditor::itemListRowChanged(int)
{
    loadItemData(false);
}
void ZhouYuEditor::loadStory()
{
    ui->storyName->setText(_data->storyName.c_str());
    ui->storyComment->blockSignals(true);
    ui->storyComment->setText(_data->storyComment.c_str());
    ui->storyComment->blockSignals(false);
}
void ZhouYuEditor::loadChapter(bool loadList)
{
    int index = ui->chapterListWidget->currentRow();
    cout<<"load chapter "<<ui->chapterListWidget->currentRow()<<",index "<<index<<endl;
    if (loadList){
        ui->chapterListWidget->clear();
        for (size_t i=0; i<_data->_chapters.size(); i++) {
            ui->chapterListWidget->addItem(QString("%1 %2")
                                           .arg(i)
                                           .arg(_data->_chapters[i].chapterName.c_str()));
        }
    }
    if (index != -1){
        ui->chapterName->setText(_data->_chapters[index].chapterName.c_str());
        ui->chapterComment->blockSignals(true);
        ui->chapterComment->setText(_data->_chapters[index].chapterComment.c_str());
        ui->chapterComment->blockSignals(false);
        if (_data->_chapters[index]._segments.size() >0){
            loadSegment(true);
            ui->segmentListWidget->setCurrentRow(0);
        }
    }else{
        ui->segmentListWidget->clear();
        ui->chapterName->setText("");
        ui->chapterComment->setText("");
    }
}
void ZhouYuEditor::loadSegment(bool loadList)
{
    int chapter = ui->chapterListWidget->currentRow();
    if (loadList && chapter!=-1){
        ui->segmentListWidget->clear();
        for (size_t i=0; i<_data->_chapters[chapter]._segments.size(); i++) {
            ui->segmentListWidget->addItem(QString("%1 %2")
                                           .arg(i)
                                           .arg(_data->_chapters[chapter]._segments[i].title.c_str()));
        }
        ui->segmentListWidget->setCurrentRow(0);
    }
    int index = ui->segmentListWidget->currentRow();
    if (chapter != -1 && index!=-1){
        cout<<"chapter "<<chapter<<", index "<<index<<endl;
        ui->segmentName->setText(_data->_chapters[chapter]._segments[index].title.c_str());
        ui->segmentStart->setText(QString("%1").arg(_data->_chapters[chapter]._segments[index].start));
        ui->segmentEnd->setText(QString("%1").arg(_data->_chapters[chapter]._segments[index].end));
    }else{
        ui->segmentName->setText("");
        ui->segmentStart->setText("");
        ui->segmentEnd->setText("");
    }
}
void ZhouYuEditor::loadSubStory(bool loadList)
{
    int index = ui->subStoryListWidget->currentRow();
    if (loadList){
        ui->subStoryListWidget->clear();
        for (size_t i=0; i<_data->_stories.size(); i++){
            ui->subStoryListWidget->addItem(QString("%1 %2")
                                            .arg(i)
                                            .arg(_data->_stories[i].comment.c_str()));
        }
    }
    if (index != -1){
        ui->subStoryDetail->setText(_data->_stories[index].comment.c_str());
    }else{
        ui->subStoryDetail->setText("");
    }
}
void ZhouYuEditor::storeStory()
{
    _data->storyName = ui->storyName->text().toStdString();
    _data->storyComment = ui->storyComment->toPlainText().toStdString();
    
    int chapterIndex = ui->chapterListWidget->currentRow();
    int subStoryIndex = ui->subStoryListWidget->currentRow();
    int segmentIndex = ui->segmentListWidget->currentRow();
    cout<<"------------------------"<<endl;
    cout<<"store chapter:"<<chapterIndex<<endl;
    cout<<"store segment:"<<segmentIndex<<endl;
    cout<<"store subStory:"<<subStoryIndex<<endl;
    if (chapterIndex!=-1){
        _data->_chapters[chapterIndex].chapterName = ui->chapterName->text().toStdString();
        _data->_chapters[chapterIndex].chapterComment = ui->chapterComment->toPlainText().toStdString();
        if (segmentIndex !=-1){
            _data->_chapters[chapterIndex]._segments[segmentIndex].title = ui->segmentName->text().toStdString();
            _data->_chapters[chapterIndex]._segments[segmentIndex].start = ui->segmentStart->text().toInt();
            _data->_chapters[chapterIndex]._segments[segmentIndex].end = ui->segmentEnd->text().toInt();
            loadSegment(true);
            ui->segmentListWidget->setCurrentRow(segmentIndex);
        }else{
            ui->segmentName->setText("");
            ui->segmentStart->setText("");
            ui->segmentEnd->setText("");
        }
        loadChapter(true);
        ui->chapterListWidget->setCurrentRow(chapterIndex);
    }else{
        ui->chapterName->setText("");
        ui->chapterComment->setText("");
    }
    if (subStoryIndex!=-1){
        _data->_stories[subStoryIndex].comment = ui->subStoryDetail->text().toStdString();
        loadStory();
        ui->subStoryListWidget->setCurrentRow(subStoryIndex);
    }else{
        ui->subStoryDetail->setText("");
    }
}
void ZhouYuEditor::chapterListRowChanged(int row)
{
    cout<<"chapter list row changed to "<<row<<endl;
    loadChapter(false);
}
void ZhouYuEditor::segmentListRowChanged(int row)
{
    cout<<"segment List row changed to "<<row<<endl;
    loadSegment(false);
}
void ZhouYuEditor::storyListRowChanged(int row)
{
    cout<<"sotry list row changed to "<<row<<endl;
    loadSubStory(false);
}
void ZhouYuEditor::arrayPropRowChanged(int)
{
    loadMagic(false, false);
}
void ZhouYuEditor::battleListRowChanged(int)
{
    loadBattle(false, true);
}
void ZhouYuEditor::loadMagic(bool loadList, bool defaultArray)
{
    int index = ui->magicListWidget->currentRow();
    if (loadList){
        ui->magicListWidget->clear();
        for (size_t i=0; i<_data->_magics.size(); i++){
            ui->magicListWidget->addItem(QString("%1 %2")
                                         .arg(i)
                                         .arg(_data->_magics[i].dName.c_str()));
        }
    }
    if (index != -1){
        ui->magicName->setText(_data->_magics[index].dName.c_str());
        ui->magicComment->blockSignals(true);
        ui->magicComment->setText(_data->_magics[index].comment.c_str());
        ui->magicComment->blockSignals(false);
        ui->magicMP->setText(QString("%1").arg(_data->_magics[index].mp));
        ui->magicRound1->setText(QString("%1").arg(_data->_magics[index].round));
        ui->magicRound2->setText(QString("%1").arg(_data->_magics[index].roundM));
        
        Magic &m = _data->_magics[index];
        if (m.type == Magic::kMagic){
            ui->magicRadio->setChecked(true);
            ui->magicTarget->setCurrentIndex(m.target);
            ui->magicHP->setText(QString("%1").arg(m.HPp));
            ui->magicType->setCurrentIndex(m.magicType);
            ui->arrayValue0->setText("");
            ui->arrayValueMax0->setText("");
            ui->arrayValue1->setText("");
            ui->arrayValueMax1->setText("");
            ui->arrayValue2->setText("");
            ui->arrayValueMax2->setText("");
            ui->arrayValue3->setText("");
            ui->arrayValueMax3->setText("");
            ui->arrayValue4->setText("");
            ui->arrayValueMax4->setText("");
            ui->arraySlider0->setValue(0);
            ui->arraySlider1->setValue(0);
            ui->arraySlider2->setValue(0);
            ui->arraySlider3->setValue(0);
            ui->arraySlider4->setValue(0);
        }else{
            ui->magicRound1->setText("");
            ui->magicRound2->setText("");
            ui->arrayRadio->setChecked(true);
            ui->arraySlider0->setMaximum(2);
            ui->arraySlider1->setMaximum(2);
            ui->arraySlider2->setMaximum(2);
            ui->arraySlider3->setMaximum(2);
            ui->arraySlider4->setMaximum(2);
            ui->arraySlider0->setValue(m.pos[0]);
            ui->arraySlider1->setValue(m.pos[1]);
            ui->arraySlider2->setValue(m.pos[2]);
            ui->arraySlider3->setValue(m.pos[3]);
            ui->arraySlider4->setValue(m.pos[4]);
            
            //
            vector<QComboBox*> comb;
            comb.push_back(ui->arrayProp0);
            comb.push_back(ui->arrayProp1);
            comb.push_back(ui->arrayProp2);
            comb.push_back(ui->arrayProp3);
            comb.push_back(ui->arrayProp4);
            vector<QLineEdit*> vMin,vMax;
            vMin.push_back(ui->arrayValue0);
            vMin.push_back(ui->arrayValue1);
            vMin.push_back(ui->arrayValue2);
            vMin.push_back(ui->arrayValue3);
            vMin.push_back(ui->arrayValue4);
            vMax.push_back(ui->arrayValueMax0);
            vMax.push_back(ui->arrayValueMax1);
            vMax.push_back(ui->arrayValueMax2);
            vMax.push_back(ui->arrayValueMax3);
            vMax.push_back(ui->arrayValueMax4);
            if (defaultArray){
                ui->arrayProp0->setCurrentIndex(0);
                ui->arrayProp1->setCurrentIndex(0);
                ui->arrayProp2->setCurrentIndex(0);
                ui->arrayProp3->setCurrentIndex(0);
                ui->arrayProp4->setCurrentIndex(0);
                for (size_t i=0; i<comb.size(); i++) {
                    comb[i]->setCurrentIndex(0);
                    vMin[i]->setText("0");
                    vMax[i]->setText("0");
                    if (m.O_[i] != 0){
                        comb[i]->setCurrentIndex(0);
                        vMin[i]->setText(QString("%1").arg(m.O_[i]));
                        vMax[i]->setText(QString("%1").arg(m.OM_[i]));
                        continue;
                    }
                    if (m.D_[i] != 0){
                        comb[i]->setCurrentIndex(1);
                        vMin[i]->setText(QString("%1").arg(m.D_[i]));
                        vMax[i]->setText(QString("%1").arg(m.DM_[i]));
                        continue;
                    }
                    if (m.A_[i] != 0){
                        comb[i]->setCurrentIndex(2);
                        vMin[i]->setText(QString("%1").arg(m.A_[i]));
                        vMax[i]->setText(QString("%1").arg(m.AM_[i]));
                        continue;
                    }
                    if (m.F_[i] != 0){
                        comb[i]->setCurrentIndex(3);
                        vMin[i]->setText(QString("%1").arg(m.F_[i]));
                        vMax[i]->setText(QString("%1").arg(m.FM_[i]));
                        continue;
                    }
                    if (m.AD_[i] != 0){
                        comb[i]->setCurrentIndex(4);
                        vMin[i]->setText(QString("%1").arg(m.AD_[i]));
                        vMax[i]->setText(QString("%1").arg(m.ADM_[i]));
                        continue;
                    }
                    if (m.SD_[i] != 0){
                        comb[i]->setCurrentIndex(5);
                        vMin[i]->setText(QString("%1").arg(m.SD_[i]));
                        vMax[i]->setText(QString("%1").arg(m.SDM_[i]));
                        continue;
                    }
                }
            }else{
                for (size_t i=0; i<vMin.size(); i++){
                    int index = comb[i]->currentIndex();
                    if (index==0){
                        vMin[i]->setText(QString("%1").arg(m.O_[i]));
                        vMax[i]->setText(QString("%1").arg(m.OM_[i]));
                        continue;
                    }else if (index==1){
                        vMin[i]->setText(QString("%1").arg(m.D_[i]));
                        vMax[i]->setText(QString("%1").arg(m.DM_[i]));
                        continue;
                    }else if (index==2){
                        vMin[i]->setText(QString("%1").arg(m.A_[i]));
                        vMax[i]->setText(QString("%1").arg(m.AM_[i]));
                        continue;
                    } else if (index==3){
                        vMin[i]->setText(QString("%1").arg(m.F_[i]));
                        vMax[i]->setText(QString("%1").arg(m.FM_[i]));
                        continue;
                    } else if (index==4){
                        vMin[i]->setText(QString("%1").arg(m.AD_[i]));
                        vMax[i]->setText(QString("%1").arg(m.ADM_[i]));
                    } else if (index==5){
                        vMin[i]->setText(QString("%1").arg(m.SD_[i]));
                        vMax[i]->setText(QString("%1").arg(m.SDM_[i]));
                    }
                }
            }
        }
    }else{
        ui->magicName->setText("");
        ui->magicComment->setText("");
        ui->magicMP->setText("");
        ui->magicRound1->setText("");
        ui->magicRound2->setText("");
        ui->arrayValue0->setText("");
        ui->arrayValueMax0->setText("");
        ui->arrayValue1->setText("");
        ui->arrayValueMax1->setText("");
        ui->arrayValue2->setText("");
        ui->arrayValueMax2->setText("");
        ui->arrayValue3->setText("");
        ui->arrayValueMax3->setText("");
        ui->arrayValue4->setText("");
        ui->arrayValueMax4->setText("");
        ui->magicHP->setText("");
    }
}

void ZhouYuEditor::storeMagic()
{
    int index = ui->magicListWidget->currentRow();
    if (index != -1){
        _data->_magics[index].dName = ui->magicName->text().toStdString();
        _data->_magics[index].comment = ui->magicComment->toPlainText().toStdString();
        _data->_magics[index].mp = ui->magicMP->text().toInt();
        if (ui->magicRadio->isChecked()){
            _data->_magics[index].type = Magic::kMagic;
            _data->_magics[index].target = (Magic::MagicTarget)ui->magicTarget->currentIndex();
            _data->_magics[index].HPp = ui->magicHP->text().toInt();
            _data->_magics[index].magicType = (Magic::MagicType)ui->magicType->currentIndex();
            _data->_magics[index].round = ui->magicRound1->text().toInt();
            _data->_magics[index].roundM = ui->magicRound2->text().toInt();
        }else if (ui->arrayRadio->isChecked()){
            _data->_magics[index].type = Magic::kArray;
            _data->_magics[index].pos[0] = ui->arraySlider0->value();
            _data->_magics[index].pos[1] = ui->arraySlider1->value();
            _data->_magics[index].pos[2] = ui->arraySlider2->value();
            _data->_magics[index].pos[3] = ui->arraySlider3->value();
            _data->_magics[index].pos[4] = ui->arraySlider4->value();
            for (size_t j=0; j<arrayComb.size(); j++) {
                int i = arrayComb[j]->currentIndex();
                if (i==0){
                    _data->_magics[index].O_[j] = arrayMin[j]->text().toInt();
                    _data->_magics[index].OM_[j] = arrayMax[j]->text().toInt();
                }else if (i==1){
                    _data->_magics[index].D_[j] = arrayMin[j]->text().toInt();
                    _data->_magics[index].DM_[j] = arrayMax[j]->text().toInt();
                }else if (i==2){
                    _data->_magics[index].A_[j] = arrayMin[j]->text().toInt();
                    _data->_magics[index].AM_[j] = arrayMax[j]->text().toInt();
                }else if (i==3){
                    _data->_magics[index].F_[j] = arrayMin[j]->text().toInt();
                    _data->_magics[index].FM_[j] = arrayMax[j]->text().toInt();
                }else if (i==4){
                    _data->_magics[index].AD_[j] = arrayMin[j]->text().toInt();
                    _data->_magics[index].ADM_[j] = arrayMax[j]->text().toInt();
                }else if (i==5){
                    _data->_magics[index].SD_[j] = arrayMin[j]->text().toInt();
                    _data->_magics[index].SDM_[j] = arrayMax[j]->text().toInt();
                }
            }
        }
    }
    loadMagic(true, false);
    ui->magicListWidget->setCurrentRow(index);
}
void ZhouYuEditor::magicListRowChanged(int)
{
    loadMagic(false, true);
}
void ZhouYuEditor::loadMap(bool loadList)
{
    cout<<"load map call"<<endl;
    int index = ui->mapListWidget->currentRow();
    if (loadList){
        ui->mapListWidget->clear();
        for (size_t i = 0; i<_data->_maps.size(); i++) {
            ui->mapListWidget->addItem(QString("%1 %2")
                                       .arg(i)
                                       .arg(_data->_maps[i].name.c_str()));
        }
    }
    if (index != -1){
        ui->mapName->setText(_data->_maps[index].name.c_str());
        ui->mapResName->setText(_data->_maps[index].resName.c_str());
        ui->mapComment->blockSignals(true);
        ui->mapComment->setText(_data->_maps[index].comment.c_str());
        ui->mapComment->blockSignals(false);
    }else{
        ui->mapName->setText("");
        ui->mapResName->setText("");
        ui->mapComment->blockSignals(true);
        ui->mapComment->setText("");
        ui->mapComment->blockSignals(false);
    }
}
void ZhouYuEditor::storeMap()
{
    int index = ui->mapListWidget->currentRow();
    cout<<"store map "<<index<<endl;
    if (index != -1){
        _data->_maps[index].name = ui->mapName->text().toStdString();
        _data->_maps[index].resName = ui->mapResName->text().toStdString();
        _data->_maps[index].comment = ui->mapComment->toPlainText().toStdString();
        loadMap(true);
    }
    ui->mapListWidget->setCurrentRow(index);
}

void ZhouYuEditor::mapListRowChanged(int)
{
    loadMap(false);
}

void ZhouYuEditor::loadButtonClicked(bool)
{
    _data->_path = ui->pathEdit->text().toStdString();
    bool loadOK = _data->reLoad();
    
    if (loadOK){
        loadStory();
        loadChapter(true);
        loadSegment(true);
        loadSubStory(true);
        loadMagic(true, true);
        loadMap(true);
        loadActorData(true, 0);
        loadTable();
        loadNPCData(true);
        loadItemData(true);
        loadBattle(true, true);
        ui->actorListWidget->setCurrentRow(0);
        ui->actorOffset->setCurrentIndex(0);
        ui->itemListWidget->setCurrentRow(0);
        ui->magicListWidget->setCurrentRow(0);
        ui->npcListWidget->setCurrentRow(0);
        ui->mapListWidget->setCurrentRow(0);
        ui->chapterListWidget->setCurrentRow(0);
        ui->segmentListWidget->setCurrentRow(0);
        ui->subStoryListWidget->setCurrentRow(0);
        ui->battleListWidget->setCurrentRow(0);
    }else{
        QMessageBox::warning(NULL, "加载失败", "请重新选择文件夹路径", 0, 0);
    }
}
void ZhouYuEditor::getDir()
{
    QString str = QFileDialog::getExistingDirectory();
    cout<<"select path:"<<str.toStdString().c_str()<<endl;
    ui->pathEdit->setText(str);
    loadButtonClicked(true);
}

void ZhouYuEditor::loadBattle(bool loadList, bool loadActors)
{
    int index = ui->battleListWidget->currentRow();
    vector<QComboBox*> combs;
    vector<QLineEdit*> editors;
    combs.push_back(ui->battleEnemiesComb0);
    combs.push_back(ui->battleEnemiesComb1);
    combs.push_back(ui->battleEnemiesComb2);
    combs.push_back(ui->battleEnemiesComb3);
    combs.push_back(ui->battleEnemiesComb4);
    editors.push_back(ui->battleLevel0);
    editors.push_back(ui->battleLevel1);
    editors.push_back(ui->battleLevel2);
    editors.push_back(ui->battleLevel3);
    editors.push_back(ui->battleLevel4);
    
    if (loadList){
        ui->battleListWidget->clear();
        for (size_t i=0; i<_data->_battles.size(); i++){
            ui->battleListWidget->addItem(QString("%1 %2")
                                          .arg(i)
                                          .arg(_data->_battles[i].dName.c_str()));
        }
    }
    if (index != -1) {
        ui->battleName->setText(_data->_battles[index].dName.c_str());
        ui->battleComment->blockSignals(true);
        ui->battleComment->setText(_data->_battles[index].comment.c_str());
        ui->battleComment->blockSignals(false);
        if (strcmp(_data->_battles[index].script.c_str(), "null") == 0){
            ui->battleScriptName->setText("无");
        }
        else ui->battleScriptName->setText(_data->_battles[index].script.c_str());
        if (loadActors){
            ui->battleEnemiesComb0->clear();
            ui->battleEnemiesComb1->clear();
            ui->battleEnemiesComb2->clear();
            ui->battleEnemiesComb3->clear();
            ui->battleEnemiesComb4->clear();
            
            ui->battleEnemiesComb0->addItem(QString("无"));
            ui->battleEnemiesComb1->addItem(QString("无"));
            ui->battleEnemiesComb2->addItem(QString("无"));
            ui->battleEnemiesComb3->addItem(QString("无"));
            ui->battleEnemiesComb4->addItem(QString("无"));
            for (size_t i=0; i<_data->_ActorDatas.size(); i++) {
                ui->battleEnemiesComb0->addItem(QString("%1 %2")
                                                .arg(i)
                                                .arg(_data->_ActorDatas[i].dName.c_str()));
                ui->battleEnemiesComb1->addItem(QString("%1 %2")
                                                .arg(i)
                                                .arg(_data->_ActorDatas[i].dName.c_str()));
                ui->battleEnemiesComb2->addItem(QString("%1 %2")
                                                .arg(i)
                                                .arg(_data->_ActorDatas[i].dName.c_str()));
                ui->battleEnemiesComb3->addItem(QString("%1 %2")
                                                .arg(i)
                                                .arg(_data->_ActorDatas[i].dName.c_str()));
                ui->battleEnemiesComb4->addItem(QString("%1 %2")
                                                .arg(i)
                                                .arg(_data->_ActorDatas[i].dName.c_str()));
            }
        }
        for (size_t i=0; i<combs.size(); i++){
            combs[i]->setCurrentIndex(_data->_battles[index].enemies_[i]+1);
            editors[i]->setText(QString("%1").arg(_data->_battles[index].level_[i]));
        }
    }else{
        ui->battleName->setText("");
        ui->battleComment->setText("");
    }
}

void ZhouYuEditor::storeBattle()
{
    int index = ui->battleListWidget->currentRow();
    vector<QComboBox*> combs;
    vector<QLineEdit*> editors;
    combs.push_back(ui->battleEnemiesComb0);
    combs.push_back(ui->battleEnemiesComb1);
    combs.push_back(ui->battleEnemiesComb2);
    combs.push_back(ui->battleEnemiesComb3);
    combs.push_back(ui->battleEnemiesComb4);
    editors.push_back(ui->battleLevel0);
    editors.push_back(ui->battleLevel1);
    editors.push_back(ui->battleLevel2);
    editors.push_back(ui->battleLevel3);
    editors.push_back(ui->battleLevel4);
    if (index != -1){
        _data->_battles[index].dName = ui->battleName->text().toStdString();
        _data->_battles[index].script = ui->battleScriptName->text().toStdString();
        _data->_battles[index].comment = ui->battleComment->toPlainText().toStdString();
        for (size_t i = 0; i<combs.size(); i++){
            _data->_battles[index].enemies_[i]=combs[i]->currentIndex()-1;
            _data->_battles[index].level_[i]=editors[i]->text().toInt();
        }
    }
    loadBattle(true, true);
    ui->battleListWidget->setCurrentRow(index);
}

void ZhouYuEditor::saveAll()
{
    cout<<"update and save data"<<endl;
    storeConfig();
    _data->saveData();
    setNeedSave(false);
}

void ZhouYuEditor::addButton()
{
    int tabIndex = ui->tabWidget->currentIndex();
    cout<<"tab index: "<<tabIndex<<endl;
    if (tabIndex==1){
        int row = ui->actorListWidget->currentRow();
        if (row<0) row=_data->_ActorDatas.size()-1;
        ActorData n = _data->_ActorDatas[row];
        n.index = _data->_ActorDatas.size();
        n.dName += "_2";
        _data->_ActorDatas.push_back(n);
        loadTable();
        loadActorData(true, 0);
        ui->actorListWidget->setCurrentRow(_data->_ActorDatas.size()-1);
    }else if (tabIndex==2){
        int row = ui->npcListWidget->currentRow();
        if (row<0) row=_data->_NPCDatas.size()-1;
        NPCData n = _data->_NPCDatas[row];
        _data->_NPCDatas.push_back(n);
        loadNPCData(true);
        ui->npcListWidget->setCurrentRow(_data->_NPCDatas.size()-1);
        loadTable();
    }else if (tabIndex==3){
        int row = ui->itemListWidget->currentRow();
        if (row<0) row=_data->_ItemDatas.size()-1;
        ItemData n = _data->_ItemDatas[row];
        _data->_ItemDatas.push_back(n);
        loadItemData(true);
        ui->itemListWidget->setCurrentRow(_data->_ItemDatas.size()-1);
    }else if (tabIndex==5){
        int row = ui->magicListWidget->currentRow();
        if (row<0) row=_data->_magics.size()-1;
        Magic n = _data->_magics[row];
        _data->_magics.push_back(n);
        loadMagic(true, true);
        ui->magicListWidget->setCurrentRow(_data->_magics.size()-1);
    }else if (tabIndex==6){
        int row = ui->mapListWidget->currentRow();
        if (row<0) row=_data->_maps.size()-1;
        Map n = _data->_maps[row];
        _data->_maps.push_back(n);
        loadMap(true);
        ui->mapListWidget->setCurrentRow(_data->_maps.size()-1);
    }else if (tabIndex==7){
        int row = ui->battleListWidget->currentRow();
        if (row<0) row=_data->_battles.size()-1;
        Battle n = _data->_battles[row];
        _data->_battles.push_back(n);
        loadBattle(true, true);
        ui->battleListWidget->setCurrentRow(_data->_battles.size()-1);
    }
}
void ZhouYuEditor::delButton()
{
    int tabIndex = ui->tabWidget->currentIndex();
    cout<<"tab index: "<<tabIndex<<endl;
    if (tabIndex==1){//actor
        int row = ui->actorListWidget->currentRow();
        if (row < 0) return;
        _data->_ActorDatas.erase(_data->_ActorDatas.begin()+row);
        int r = ((row-1) > 0)? row-1: 0;
        loadActorData(true, 0);
        ui->actorListWidget->setCurrentRow(r);
    }else if (tabIndex==2){//npc
        int row = ui->npcListWidget->currentRow();
        if (row < 0) return;
        _data->_NPCDatas.erase(_data->_NPCDatas.begin()+row);
        int r = ((row-1) > 0)? row-1: 0;
        loadNPCData(true);
        ui->npcListWidget->setCurrentRow(r);
    }else if (tabIndex==3){//item
        int row = ui->itemListWidget->currentRow();
        if (row < 0) return;
        _data->_ItemDatas.erase(_data->_ItemDatas.begin()+row);
        int r = ((row-1) > 0)? row-1: 0;
        loadItemData(true);
        ui->itemListWidget->setCurrentRow(r);
    }else if (tabIndex==5){//magic
        int row = ui->magicListWidget->currentRow();
        if (row < 0) return;
        _data->_magics.erase(_data->_magics.begin()+row);
        int r = ((row-1) > 0)? row-1: 0;
        loadMagic(true, true);
        ui->magicListWidget->setCurrentRow(r);
    }else if (tabIndex==6){//map
        int row = ui->mapListWidget->currentRow();
        if (row < 0) return;
        _data->_maps.erase(_data->_maps.begin()+row);
        int r = ((row-1) > 0)? row-1: 0;
        loadMap(true);
        ui->mapListWidget->setCurrentRow(r);
    }else if (tabIndex==7){//battle
        int row = ui->battleListWidget->currentRow();
        if (row < 0) return;
        _data->_battles.erase(_data->_battles.begin()+row);
        int r = ((row-1) > 0)? row-1: 0;
        loadBattle(true, true);
        ui->battleListWidget->setCurrentRow(r);
    }
}
void ZhouYuEditor::upButton()
{
    int tabIndex = ui->tabWidget->currentIndex();
    cout<<"tab index: "<<tabIndex<<endl;
    if (tabIndex==1){//actor
        int row = ui->actorListWidget->currentRow();
        if (row <= 0) return;
        ActorData a = _data->_ActorDatas[row];
        _data->_ActorDatas[row]=_data->_ActorDatas[row-1];
        _data->_ActorDatas[row-1]=a;
        _data->_ActorDatas[row].index = row;
        _data->_ActorDatas[row-1].index = row-1;
        loadActorData(true, 0);
        loadTable();
        ui->actorListWidget->setCurrentRow(row-1);
    }else if (tabIndex==2){//npc
        int row = ui->npcListWidget->currentRow();
        if (row <= 0) return;
        NPCData a = _data->_NPCDatas[row];
        _data->_NPCDatas[row]=_data->_NPCDatas[row-1];
        _data->_NPCDatas[row-1]=a;
        loadNPCData(true);
        ui->npcListWidget->setCurrentRow(row-1);
    }else if (tabIndex==3){//item
        int row = ui->itemListWidget->currentRow();
        if (row <= 0) return;
        ItemData a = _data->_ItemDatas[row];
        _data->_ItemDatas[row]=_data->_ItemDatas[row-1];
        _data->_ItemDatas[row-1]=a;
        loadItemData(true);
        ui->itemListWidget->setCurrentRow(row-1);
    }else if (tabIndex==5){//magic
        int row = ui->magicListWidget->currentRow();
        if (row <= 0) return;
        Magic a = _data->_magics[row];
        _data->_magics[row]=_data->_magics[row-1];
        _data->_magics[row-1]=a;
        loadMagic(true, true);
        ui->magicListWidget->setCurrentRow(row-1);
    }else if (tabIndex==6){//map
        int row = ui->mapListWidget->currentRow();
        if (row <= 0) return;
        Map a = _data->_maps[row];
        _data->_maps[row]=_data->_maps[row-1];
        _data->_maps[row-1]=a;
        loadMap(true);
        ui->mapListWidget->setCurrentRow(row-1);
    }else if (tabIndex==7){//battle
        int row = ui->battleListWidget->currentRow();
        if (row <= 0) return;
        Battle a = _data->_battles[row];
        _data->_battles[row]=_data->_battles[row-1];
        _data->_battles[row-1]=a;
        loadBattle(true, true);
        ui->battleListWidget->setCurrentRow(row-1);
    }
}
void ZhouYuEditor::downButton()
{
    int tabIndex = ui->tabWidget->currentIndex();
    cout<<"tab index: "<<tabIndex<<endl;
    if (tabIndex==1){//actor
        int row = ui->actorListWidget->currentRow();
        if (row == (int)_data->_ActorDatas.size()-1 || row==-1) return;
        ActorData a = _data->_ActorDatas[row];
        _data->_ActorDatas[row]=_data->_ActorDatas[row+1];
        _data->_ActorDatas[row+1]=a;
        _data->_ActorDatas[row].index = row;
        _data->_ActorDatas[row+1].index = row+1;
        loadActorData(true, 0);
        loadTable();
        ui->actorListWidget->setCurrentRow(row+1);
    }else if (tabIndex==2){//npc
        int row = ui->npcListWidget->currentRow();
        if (row == (int)_data->_NPCDatas.size()-1 || row==-1) return;
        cout<<"down npc"<<endl;
        NPCData a = _data->_NPCDatas[row];
        _data->_NPCDatas[row]=_data->_NPCDatas[row+1];
        _data->_NPCDatas[row+1]=a;
        loadNPCData(true);
        ui->npcListWidget->setCurrentRow(row+1);
    }else if (tabIndex==3){//item
        int row = ui->itemListWidget->currentRow();
        if (row == (int)_data->_ItemDatas.size()-1 || row==-1) return;
        ItemData a = _data->_ItemDatas[row];
        _data->_ItemDatas[row]=_data->_ItemDatas[row+1];
        _data->_ItemDatas[row+1]=a;
        loadItemData(true);
        ui->itemListWidget->setCurrentRow(row+1);
    }else if (tabIndex==5){//magic
        int row = ui->magicListWidget->currentRow();
        if (row == (int)_data->_magics.size()-1 || row==-1) return;
        Magic a = _data->_magics[row];
        _data->_magics[row]=_data->_magics[row+1];
        _data->_magics[row+1]=a;
        loadMagic(true, true);
        ui->magicListWidget->setCurrentRow(row+1);
    }else if (tabIndex==6){//map
        int row = ui->mapListWidget->currentRow();
        if (row == (int)_data->_maps.size()-1 || row==-1) return;
        Map a = _data->_maps[row];
        _data->_maps[row]=_data->_maps[row+1];
        _data->_maps[row+1]=a;
        loadMap(true);
        ui->mapListWidget->setCurrentRow(row+1);
    }else if (tabIndex==7){//battle
        int row = ui->battleListWidget->currentRow();
        if (row == (int)_data->_battles.size()-1 || row==-1) return;
        Battle a = _data->_battles[row];
        _data->_battles[row]=_data->_battles[row+1];
        _data->_battles[row+1]=a;
        loadBattle(true, true);
        ui->battleListWidget->setCurrentRow(row+1);
    }
}

void ZhouYuEditor::addChapterButton()
{
    int row = ui->chapterListWidget->currentRow();
    if (row<0) row=_data->_chapters.size()-1;
    chapter n = _data->_chapters[row];
    _data->_chapters.push_back(n);
    loadChapter(true);
    ui->chapterListWidget->setCurrentRow(_data->_chapters.size()-1);
}
void ZhouYuEditor::delChapterButton()
{
    int row = ui->chapterListWidget->currentRow();
    if (row < 0) return;
    _data->_chapters.erase(_data->_chapters.begin()+row);
    int r = ((row-1) > 0)? row-1: 0;
    loadChapter(true);
    ui->chapterListWidget->setCurrentRow(r);
}
void ZhouYuEditor::upChapterButton()
{
    int row = ui->chapterListWidget->currentRow();
    if (row <= 0) return;
    chapter a = _data->_chapters[row];
    _data->_chapters[row]=_data->_chapters[row-1];
    _data->_chapters[row-1]=a;
    loadChapter(true);
    ui->chapterListWidget->setCurrentRow(row-1);
}
void ZhouYuEditor::downChapterButton()
{
    int row = ui->chapterListWidget->currentRow();
    if (row == (int)_data->_chapters.size()-1 || row==-1) return;
    chapter a = _data->_chapters[row];
    _data->_chapters[row]=_data->_chapters[row+1];
    _data->_chapters[row+1]=a;
    loadChapter(true);
    ui->chapterListWidget->setCurrentRow(row+1);
}
//segment
void ZhouYuEditor::addSegmentButton()
{
    int ch = ui->chapterListWidget->currentRow();
    int row = ui->segmentListWidget->currentRow();
    if (row<0) row=_data->_chapters[ch]._segments.size()-1;
    segment n = _data->_chapters[ch]._segments[row];
    _data->_chapters[ch]._segments.push_back(n);
    loadSegment(true);
    ui->segmentListWidget->setCurrentRow(_data->_chapters[ch]._segments.size()-1);
}
void ZhouYuEditor::delSegmentButton()
{
    int ch = ui->chapterListWidget->currentRow();
    int row = ui->segmentListWidget->currentRow();
    if (row < 0) return;
    _data->_chapters[ch]._segments.erase(_data->_chapters[ch]._segments.begin()+row);
    int r = ((row-1) > 0)? row-1: 0;
    loadSegment(true);
    ui->segmentListWidget->setCurrentRow(r);
}
void ZhouYuEditor::upSegmentButton()
{
    int ch = ui->chapterListWidget->currentRow();
    int row = ui->segmentListWidget->currentRow();
    if (row <= 0) return;
    segment a = _data->_chapters[ch]._segments[row];
    _data->_chapters[ch]._segments[row]=_data->_chapters[ch]._segments[row-1];
    _data->_chapters[ch]._segments[row-1]=a;
    loadSegment(true);
    ui->segmentListWidget->setCurrentRow(row-1);
}
void ZhouYuEditor::downSegmentButton()
{
    size_t ch = ui->chapterListWidget->currentRow();
    int row = ui->segmentListWidget->currentRow();
    if (row == (int)_data->_chapters[ch]._segments.size()-1 || row==-1) return;
    segment a = _data->_chapters[ch]._segments[row];
    _data->_chapters[ch]._segments[row]=_data->_chapters[ch]._segments[row+1];
    _data->_chapters[ch]._segments[row+1]=a;
    loadSegment(true);
    ui->segmentListWidget->setCurrentRow(row+1);
}
//story
void ZhouYuEditor::addStoryButton()
{
    int row = ui->subStoryListWidget->currentRow();
    if (row<0) row=_data->_stories.size()-1;
    subStory n = _data->_stories[row];
    _data->_stories.push_back(n);
    loadSubStory(true);
    ui->subStoryListWidget->setCurrentRow(_data->_stories.size()-1);
}
void ZhouYuEditor::delStoryButton()
{
    int row = ui->subStoryListWidget->currentRow();
    if (row < 0) return;
    _data->_stories.erase(_data->_stories.begin()+row);
    int r = ((row-1) > 0)? row-1: 0;
    loadSubStory(true);
    ui->subStoryListWidget->setCurrentRow(r);
}
void ZhouYuEditor::upStoryButton()
{
    int row = ui->subStoryListWidget->currentRow();
    if (row <= 0) return;
    subStory a = _data->_stories[row];
    _data->_stories[row]=_data->_stories[row-1];
    _data->_stories[row-1]=a;
    loadSubStory(true);
    ui->subStoryListWidget->setCurrentRow(row-1);
}
void ZhouYuEditor::downStoryButton()
{
    int row = ui->subStoryListWidget->currentRow();
    if (row == (int)_data->_stories.size()-1 || row==-1) return;
    subStory a = _data->_stories[row];
    _data->_stories[row]=_data->_stories[row+1];
    _data->_stories[row+1]=a;
    loadSubStory(true);
    ui->subStoryListWidget->setCurrentRow(row+1);
}

void ZhouYuEditor::updateActorValue()
{
    ui->actorI->setText(QString("%1").arg(ui->actorISlider->value()));
    ui->actorS->setText(QString("%1").arg(ui->actorSSlider->value()));
    ui->actorL->setText(QString("%1").arg(ui->actorLSlider->value()));
    ui->actorA->setText(QString("%1").arg(ui->actorASlider->value()));
}

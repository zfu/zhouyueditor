//
//  Data.cpp
//  ZhouYuEditor
//
//  Created by zfu on 14/8/25.
//
//

#include "Data.h"
#include <assert.h>


void Data::split(std::string& s, std::string delim,std::vector<std::string >* ret)
{
    size_t last = 0;
    size_t index=s.find_first_of(delim,last);
    while (index!=std::string::npos)
    {
        ret->push_back(s.substr(last,index-last));
        last=index+1;
        index=s.find_first_of(delim,last);
    }
    if (index-last>0)
    {
        ret->push_back(s.substr(last,index-last));
    }
}

void Data::loadActor()
{
    _ActorDatas.clear();
    char index[56];
    for (int i=0; ; i++) {
        sprintf(index, "actor_%d", i);
        ActorData data;
        data.index = actorINI->getIntValue(index, "ID");
        if (data.index != i) break;
        data.dName = actorINI->getStringValue(index, "dName");
        data.dName2 = actorINI->getStringValue(index, "dName2");
        data.resName = actorINI->getStringValue(index, "name");
        data.battleResName = actorINI->getStringValue(index, "battleName");
        data.wName = actorINI->getStringValue(index, "weaponName");
        string wType = actorINI->getStringValue(index, "weaponType");
        //cout<<"actor_"<<i<<" "<<wType<<endl;
        if (strcmp(wType.c_str(), "light")==0){
            data.weapontype = ActorData::kWeaponLight;
        }else if (strcmp(wType.c_str(), "long")==0){
            data.weapontype = ActorData::kWeaponLong;
        }else if (strcmp(wType.c_str(), "far")==0){
            data.weapontype = ActorData::kWeaponFar;
        }else if (strcmp(wType.c_str(), "heavy")==0){
            data.weapontype = ActorData::kWeaponHeavy;
        }else if (strcmp(wType.c_str(), "double")==0){
            data.weapontype = ActorData::kWeaponDouble;
        }
        
        data.I = actorINI->getIntValue(index, "I");
        data.S = actorINI->getIntValue(index, "S");
        data.L = actorINI->getIntValue(index, "L");
        data.A = actorINI->getIntValue(index, "A");
        
        data.hasSk = actorINI->getBoolValue(index, "hasSkill");
        if (data.hasSk){
            data.skName = actorINI->getStringValue(index, "skillName");
            data.skComment = actorINI->getStringValue(index, "skillComment");
            data.skDelay = actorINI->getStringValue(index, "spattackDelay");
            data.skMax = actorINI->getIntValue(index, "skillValue");
            data.skMin = actorINI->getIntValue(index, "skillValue1");
            data.skTime = actorINI->getIntValue(index, "attackTimes");
        }
        
        data.magic = actorINI->getStringValue(index, "magic");
        data.hpMax = actorINI->getStringValue(index, "hpMax");
        
        string offsetx = actorINI->getStringValue(index, "offset");
        string offsety = actorINI->getStringValue(index, "offsety");
        vector<string> xs,ys;
        split(offsetx, " ", &xs);
        split(offsety, " ", &ys);
        for (int i=0; i<8; i++) {
            data.Xoffset[i]=atoi(xs[i].c_str());
            data.Yoffset[i]=atoi(ys[i].c_str());
        }
        _ActorDatas.push_back(data);
    }
}
void Data::storeActor()
{
    actorINI->clear();
    for (size_t i=0; i<_ActorDatas.size(); i++) {
        ActorData d = _ActorDatas[i];
        char index[64];
        sprintf(index, "actor_%lu", i);
        actorINI->setIntValue(index, "ID", i);
        actorINI->setStringValue(index, "name", d.resName.c_str());
        actorINI->setStringValue(index, "battleName", d.battleResName.c_str());
        char xstr[1024],ystr[1024];
        sprintf(xstr, "%d %d %d %d %d %d %d %d",
                d.Xoffset[0],
                d.Xoffset[1],
                d.Xoffset[2],
                d.Xoffset[3],
                d.Xoffset[4],
                d.Xoffset[5],
                d.Xoffset[6],
                d.Xoffset[7]
                );
        sprintf(ystr, "%d %d %d %d %d %d %d %d",
                d.Yoffset[0],
                d.Yoffset[1],
                d.Yoffset[2],
                d.Yoffset[3],
                d.Yoffset[4],
                d.Yoffset[5],
                d.Yoffset[6],
                d.Yoffset[7]
                );
        actorINI->setStringValue(index, "offset", xstr);
        actorINI->setStringValue(index, "offsety", ystr);
        actorINI->setStringValue(index, "dName", d.dName.c_str());
        actorINI->setStringValue(index, "dName2", d.dName2.c_str());
        actorINI->setStringValue(index, "magic", d.magic.c_str());
        actorINI->setStringValue(index, "hpMax", d.hpMax.c_str());
        actorINI->setStringValue(index, "weaponName", d.wName.c_str());
        string wTypeStr[]={"light", "far", "long", "heavy", "double"};
        actorINI->setStringValue(index, "weaponType", wTypeStr[d.weapontype].c_str());
        if (d.hasSk){
            actorINI->setBoolValue(index, "hasSkill", true);
            actorINI->setStringValue(index, "skillName", d.skName.c_str());
            actorINI->setStringValue(index, "skillComment", d.skComment.c_str());
            actorINI->setIntValue(index, "skillValue1", d.skMin);
            actorINI->setIntValue(index, "skillValue", d.skMax);
            actorINI->setIntValue(index, "attackTimes", d.skTime);
            actorINI->setStringValue(index, "spattackDelay", d.skDelay.c_str());
        }else{
            actorINI->setBoolValue(index, "hasSkill", false);
        }
        actorINI->setIntValue(index, "I", d.I);
        actorINI->setIntValue(index, "S", d.S);
        actorINI->setIntValue(index, "L", d.L);
        actorINI->setIntValue(index, "A", d.A);
    }
    actorINI->writeConfigFile();
}

void Data::loadNPC()
{
    _NPCDatas.clear();
    char index[56];
    for (int i=0; ; i++) {
        sprintf(index, "npc_%d", i);
        NPCData data;
        const char *name = npcINI->getStringValue(index, "displayName");
        if (name == NULL) break;
        data.dName = name;//npcINI->getStringValue(index, "displayName");
        data.resName = npcINI->getStringValue(index, "name");
        data.scriptName = npcINI->getStringValue(index, "scriptName");
        _NPCDatas.push_back(data);
    }
}

void Data::storeNPC()
{
    npcINI->clear();
    char index[64];
    for (size_t i=0; i<_NPCDatas.size(); i++) {
        NPCData n = _NPCDatas[i];
        sprintf(index, "npc_%lu", i);
        npcINI->setStringValue(index, "displayName", n.dName.c_str());
        npcINI->setStringValue(index, "name", n.resName.c_str());
        npcINI->setStringValue(index, "scriptName", n.scriptName.c_str());
    }
    npcINI->writeConfigFile();
}

void Data::loadItem()
{
    _ItemDatas.clear();
    char index[56];
    for (int i=0; ; i++) {
        sprintf(index, "item_%d", i);
        ItemData data;
        const char *name = itemINI->getStringValue(index, "dName");
        if (name == NULL) break;
        data.dName = name;
        data.comment = itemINI->getStringValue(index, "comment");
        
        const char *type = itemINI->getStringValue(index, "type");
        const char *type1 = itemINI->getStringValue(index, "type1");
        data.itemType = ItemData::kTypeEquip;
        if (strcmp(type, "hat")==0) data.itemEquipType = ItemData::kEquipHat;
        else if (strcmp(type, "shield")==0) data.itemEquipType = ItemData::kEquipShield;
        else if (strcmp(type, "horse")==0) data.itemEquipType = ItemData::kEquipHorse;
        else if (strcmp(type, "gown")==0) data.itemEquipType = ItemData::kEquipGown;
        else if (strcmp(type, "shoe")==0) data.itemEquipType = ItemData::kEquipShoe;
        else if (strcmp(type, "special")==0) data.itemEquipType = ItemData::kEquipSpecial;
        else if (strcmp(type, "input")==0){
            data.itemType = ItemData::kTypeSupply;
            if (strcmp(type1, "one")==0) data.supplyAll=false;
            else data.supplyAll=true;
        }else if (strcmp(type, "output")==0){
            data.itemType = ItemData::kTypeAttack;
            if (strcmp(type1, "one")==0) data.attackAll=false;
            else data.attackAll=true;
        }else if (strcmp(type, "other")==0){
            data.itemType = ItemData::kTypeOther;
        }else{
            assert("Error, impossible");
        }
        
        if (data.itemType == ItemData::kTypeEquip){
            data.equipAttack = 0;
            data.equipDefend = itemINI->getIntValue(index, "D");
            data.equipI = itemINI->getIntValue(index, "I");
            data.equipS = itemINI->getIntValue(index, "S");
            data.equipL = itemINI->getIntValue(index, "L");
            data.equipA = itemINI->getIntValue(index, "A");
            data.equipNeedI = itemINI->getIntValue(index, "nI");
            data.equipNeedS = itemINI->getIntValue(index, "nS");
            data.equipNeedL = itemINI->getIntValue(index, "nL");
            data.equipNeedA = itemINI->getIntValue(index, "nA");
            data.env = ItemData::kEnvEquip;
            const char *weaponType = itemINI->getStringValue(index, "weaponType");
            if (strcmp(weaponType, "light") == 0) data.weaponType = ItemData::kWeaponLight;
            else if (strcmp(weaponType, "heavy") == 0) data.weaponType = ItemData::kWeaponHeavy;
            else if (strcmp(weaponType, "light,long") == 0) data.weaponType = ItemData::kWeaponLightLong;
        }else if (data.itemType == ItemData::kTypeSupply){
            data.supplyHP = itemINI->getIntValue(index, "HP");
            data.supplyMP = itemINI->getIntValue(index, "MP");
            data.supplyHPRound = 1;
            data.supplyMPRound = 1;
            string env = itemINI->getStringValue(index, "env");
            if (strcmp(env.c_str(), "all")==0){
                data.env = ItemData::kEnvAll;
            }else if (strcmp(env.c_str(), "nobattle")==0){
                data.env = ItemData::kEnvNoBattle;
            }else if (strcmp(env.c_str(), "battle")==0){
                data.env = ItemData::kEnvBattle;
            }
        }else if (data.itemType == ItemData::kTypeAttack){
            data.attackHP = itemINI->getIntValue(index, "HP");
            data.attackHPRound = 1;
            data.env = ItemData::kEnvBattle;
        }else if (data.itemType == ItemData::kTypeOther){
            data.env = ItemData::kEnvNoBattle;
        }
        data.price = 10;
        _ItemDatas.push_back(data);
    }
}

void Data::storeItem()
{
    string typestr[]={"hat", "gown", "shoe", "shield", "horse", "special", "input", "output"};
    string envstr[]={"battle", "nobattle", "all", "equip"};
    string weapontypestr[]={"light", "heavy", "long", "lightlong"};
    itemINI->clear();
    char index[64];
    for (size_t i=0; i<_ItemDatas.size(); i++) {
        ItemData item = _ItemDatas[i];
        sprintf(index, "item_%lu", i);
        itemINI->setStringValue(index, "env", envstr[item.env].c_str());
        itemINI->setStringValue(index, "dName", item.dName.c_str());
        itemINI->setStringValue(index, "comment", item.comment.c_str());
        if (item.itemType == ItemData::kTypeEquip) {
            itemINI->setStringValue(index, "type", typestr[item.itemEquipType].c_str());
            itemINI->setStringValue(index, "weaponType", weapontypestr[item.weaponType].c_str());
            itemINI->setIntValue(index, "D", item.equipDefend);
            itemINI->setIntValue(index, "I", item.equipI);
            itemINI->setIntValue(index, "S", item.equipS);
            itemINI->setIntValue(index, "L", item.equipL);
            itemINI->setIntValue(index, "A", item.equipA);
            itemINI->setIntValue(index, "nI", item.equipNeedI);
            itemINI->setIntValue(index, "nS", item.equipNeedS);
            itemINI->setIntValue(index, "nL", item.equipNeedL);
            itemINI->setIntValue(index, "nA", item.equipNeedA);
        }else if (item.itemType == ItemData::kTypeSupply) {
            itemINI->setStringValue(index, "type", "input");
            if (item.supplyAll) itemINI->setStringValue(index, "type1", "all");
            else itemINI->setStringValue(index, "type1", "one");
            itemINI->setIntValue(index, "HP", item.supplyHP);
            itemINI->setIntValue(index, "MP", item.supplyMP);
        }else if (item.itemType == ItemData::kTypeAttack) {
            itemINI->setStringValue(index, "type", "output");
            if (item.attackAll) itemINI->setStringValue(index, "type1", "all");
            else itemINI->setStringValue(index, "type1", "one");
        }else if (item.itemType == ItemData::kTypeOther) {
            itemINI->setStringValue(index, "type", "other");
            itemINI->setStringValue(index, "type1", "all");
        }
    }
    itemINI->writeConfigFile();
}

void Data::loadStory()
{
    _chapters.clear();
    _stories.clear();
    storyName = storyINI->getStringValue("story", "title");
    storyComment = storyINI->getStringValue("story", "comment");
    
    int chapterCount = storyINI->getIntValue("story", "size");
    char index[56];
    for (int i=0; i<chapterCount; i++){
        chapter ch;
        sprintf(index, "chapter_%d", i);
        ch.chapterName = storyINI->getStringValue(index, "title");
        ch.chapterComment = ch.chapterName;
        ch.segmentSize = storyINI->getIntValue(index, "size");
        for (int j=0; j<ch.segmentSize; j++){
            segment seg;
            sprintf(index, "story_%d_%d", i, j);
            seg.title = storyINI->getStringValue(index, "title");
            seg.start = storyINI->getIntValue(index, "start");
            seg.end = storyINI->getIntValue(index, "end");
            ch._segments.push_back(seg);
        }
        _chapters.push_back(ch);
    }
    for (int i=0; ; i++) {
        sprintf(index, "story_%d", i);
        subStory s;
        const char *comment = storyINI->getStringValue(index, "comment");
        if (comment==NULL) break;
        s.comment = comment;
        _stories.push_back(s);
    }
}

void Data::storeStory()
{
    storyINI->clear();
    storyINI->setStringValue("story", "title", storyName.c_str());
    storyINI->setStringValue("story", "comment", storyComment.c_str());
    storyINI->setIntValue("story", "size", _chapters.size());
    char index[64];
    for (size_t i=0; i<_chapters.size(); i++) {
        sprintf(index, "chapter_%lu", i);
        storyINI->setStringValue(index, "title", _chapters[i].chapterName.c_str());
        storyINI->setStringValue(index, "comment", _chapters[i].chapterComment.c_str());
        storyINI->setIntValue(index, "size", _chapters[i].segmentSize);
    }
    for (size_t i=0; i<_chapters.size(); i++) {
        for (size_t j=0; j<_chapters[i]._segments.size(); j++) {
            sprintf(index, "story_%lu_%lu", i, j);
            storyINI->setStringValue(index, "title", _chapters[i]._segments[j].title.c_str());
            storyINI->setIntValue(index, "start", _chapters[i]._segments[j].start);
            storyINI->setIntValue(index, "end", _chapters[i]._segments[j].end);
        }
    }
    for (size_t i=0; i<_stories.size(); i++) {
        sprintf(index, "story_%lu", i);
        storyINI->setStringValue(index, "comment", _stories[i].comment.c_str());
    }
    storyINI->writeConfigFile();
}

void Data::loadMagic()
{
    _magics.clear();
    char index[64];
    for (int i=0; ; i++) {
        sprintf(index, "magic_%d", i);
        const char *dName = magicINI->getStringValue(index, "dName");
        if (dName == NULL) break;
        Magic m;
        m.dName = dName;
        m.comment = magicINI->getStringValue(index, "comment");
        const char *round = magicINI->getStringValue(index, "round");
        const char *roundM = magicINI->getStringValue(index, "roundM");
        if (strcmp(round, "none") == 0) m.round=1;
        else m.round=atoi(round);
        if (strcmp(roundM, "none") == 0) m.roundM=m.round;
        else m.roundM=atoi(roundM);
        const char *type = magicINI->getStringValue(index, "type");
        if (strcmp(type, "magic")==0){
            m.type = Magic::kMagic;
            const char *magicType = magicINI->getStringValue(index, "magicType");
            if (strcmp(magicType, "fire")==0) m.magicType = Magic::kMagicFire;
            else if (strcmp(magicType, "water")==0) m.magicType = Magic::kMagicWater;
            else if (strcmp(magicType, "walk")==0) m.magicType = Magic::kMagicWalk;
            else if (strcmp(magicType, "earth")==0) m.magicType = Magic::kMagicEarth;
            else if (strcmp(magicType, "run")==0) m.magicType = Magic::kMagicRun;
            else if (strcmp(magicType, "addition")==0) m.magicType = Magic::kMagicAddition;
            else if (strcmp(magicType, "all")==0) m.magicType = Magic::kMagicDecrease;
            else{
                assert("Error, magic type");
            }
            m.HPp = magicINI->getIntValue(index, "HPp");
            m.O = magicINI->getStringValue(index, "O");
            m.OM = magicINI->getStringValue(index, "OM");
            m.D = magicINI->getStringValue(index, "D");
            m.AD = magicINI->getStringValue(index, "AD");
            m.SD = magicINI->getStringValue(index, "SD");
            const char *magicTarget = magicINI->getStringValue(index, "target");
            if (strcmp(magicTarget, "inOne")==0) m.target = Magic::kMagicHeroOne;
            else if (strcmp(magicTarget, "inAll")==0) m.target = Magic::kMagicHeroAll;
            else if (strcmp(magicTarget, "outOne")==0) m.target = Magic::kMagicEnemyOne;
            else if (strcmp(magicTarget, "outAll")==0) m.target = Magic::kMagicEnemyAll;
            else assert("Error, bad magic target");
        }else{
            m.type = Magic::kArray;
            m.O = magicINI->getStringValue(index, "O");
            m.OM = magicINI->getStringValue(index, "OM");
            m.A = magicINI->getStringValue(index, "A");
            m.AM = magicINI->getStringValue(index, "AM");
            m.D = magicINI->getStringValue(index, "D");
            m.DM = magicINI->getStringValue(index, "DM");
            m.F = magicINI->getStringValue(index, "F");
            m.FM = magicINI->getStringValue(index, "FM");
            m.AD = magicINI->getStringValue(index, "AD");
            m.ADM = magicINI->getStringValue(index, "ADM");
            m.SD = magicINI->getStringValue(index, "SD");
            m.SDM = magicINI->getStringValue(index, "SDM");
            string position = magicINI->getStringValue(index, "position");
            getMM(m.pos, position);
            getMM(m.O_, m.O);
            getMM(m.A_, m.A);
            getMM(m.D_, m.D);
            getMM(m.F_, m.F);
            getMM(m.AD_, m.AD);
            getMM(m.SD_, m.SD);
            getMM(m.OM_, m.OM);
            getMM(m.AM_, m.AM);
            getMM(m.DM_, m.DM);
            getMM(m.FM_, m.FM);
            getMM(m.ADM_, m.ADM);
            getMM(m.SDM_, m.SDM);
        }
        const char *env = magicINI->getStringValue(index, "env");
        if (strcmp(env, "all")==0) m.env=Magic::kEnvAll;
        else if (strcmp(env, "map")==0) m.env=Magic::kEnvMap;
        else m.env=Magic::kEnvBattle;
        m.mp = 10;
        _magics.push_back(m);
    }
    cout<<"read magics size:"<<_magics.size()<<endl;
}

void Data::storeMagic()
{
    magicINI->clear();
    for (size_t i=0; i<_magics.size(); i++) {
        Magic m = _magics[i];
        char index[64];
        sprintf(index, "magic_%lu", i);
        magicINI->setStringValue(index, "dName", m.dName.c_str());
        magicINI->setStringValue(index, "comment", m.comment.c_str());
        string targetstr[]={"inOne", "inAll", "outOne", "outAll"};
        string envstr[]={"all", "map", "battle"};
        string magictypestr[]={"water", "fire", "earth", "walk", "run", "addition", "all"};
        if (m.type == Magic::kMagic) {
            magicINI->setStringValue(index, "type", "magic");
            magicINI->setStringValue(index, "env", envstr[m.env].c_str());
            magicINI->setStringValue(index, "target", targetstr[m.target].c_str());
            magicINI->setStringValue(index, "magicType", magictypestr[m.magicType].c_str());
            magicINI->setIntValue(index, "HPp", m.HPp);
            magicINI->setIntValue(index, "O", atoi(m.O.c_str()));
            magicINI->setIntValue(index, "OM", atoi(m.OM.c_str()));
            magicINI->setIntValue(index, "D", atoi(m.D.c_str()));
            magicINI->setIntValue(index, "SD", atoi(m.SD.c_str()));
            magicINI->setIntValue(index, "AD", atoi(m.AD.c_str()));
            magicINI->setIntValue(index, "round", m.round);
            magicINI->setIntValue(index, "roundM", m.roundM);
        }else if (m.type == Magic::kArray){
            magicINI->setStringValue(index, "type", "array");
            magicINI->setStringValue(index, "env", envstr[m.env].c_str());
            setMM(m.O_, m.O);
            setMM(m.D_, m.D);
            setMM(m.A_, m.A);
            setMM(m.F_, m.F);
            setMM(m.AD_, m.AD);
            setMM(m.SD_, m.SD);
            setMM(m.OM_, m.OM);
            setMM(m.AM_, m.AM);
            setMM(m.DM_, m.DM);
            setMM(m.FM_, m.FM);
            setMM(m.ADM_, m.ADM);
            setMM(m.SDM_, m.SDM);
            string pos;
            setMM(m.pos, pos);
            magicINI->setStringValue(index, "position", pos.c_str());
            
            magicINI->setStringValue(index, "O", m.O.c_str());
            magicINI->setStringValue(index, "OM", m.OM.c_str());
            
            magicINI->setStringValue(index, "D", m.D.c_str());
            magicINI->setStringValue(index, "DM", m.DM.c_str());
            
            magicINI->setStringValue(index, "A", m.A.c_str());
            magicINI->setStringValue(index, "AM", m.AM.c_str());
            
            magicINI->setStringValue(index, "F", m.F.c_str());
            magicINI->setStringValue(index, "FM", m.FM.c_str());
            
            magicINI->setStringValue(index, "AD", m.AD.c_str());
            magicINI->setStringValue(index, "ADM", m.ADM.c_str());
            
            magicINI->setStringValue(index, "SD", m.SD.c_str());
            magicINI->setStringValue(index, "SDM", m.SDM.c_str());
        }
    }
    magicINI->writeConfigFile();
}

void Data::loadMaps()
{
    _maps.clear();
    char entry[64];
    for (int i=0; ; i++) {
        Map m;
        sprintf(entry, "map_%d", i);
        const char *name = mapINI->getStringValue("mapName", entry);
        if (name == NULL) break;
        m.name = mapINI->getStringValue("mapName", entry);
        sprintf(entry, "map_%d_name", i);
        m.resName = mapINI->getStringValue("mapName", entry);
        sprintf(entry, "map_%d_comment", i);
        m.comment = mapINI->getStringValue("mapName", entry);
        _maps.push_back(m);
    }
}
void Data::storeMaps()
{
    mapINI->clear();
    for (size_t i=0; i<_maps.size(); i++) {
        Map m = _maps[i];
        char entry[64];
        sprintf(entry, "map_%lu", i);
        mapINI->setStringValue("mapName", entry, m.name.c_str());
        
        sprintf(entry, "map_%lu_name", i);
        mapINI->setStringValue("mapName", entry, m.resName.c_str());
        
        sprintf(entry, "map_%lu_comment", i);
        mapINI->setStringValue("mapName", entry, m.comment.c_str());
    }
    mapINI->writeConfigFile();
}

void Data::loadBattle()
{
    _battles.clear();
    char index[64];
    for (int i=0; ; i++) {
        Battle b;
        sprintf(index, "battle_%d", i);
        const char *name = battleINI->getStringValue(index, "dName");
        if (name == NULL) break;
        b.dName = name;
        string enemies = battleINI->getStringValue(index, "enemies");
        string level = battleINI->getStringValue(index, "level");
        b.script = battleINI->getStringValue(index, "script");
        b.comment = battleINI->getStringValue(index, "comment");
        getMM(b.enemies_, enemies);
        getMM(b.level_, level);
        _battles.push_back(b);
    }
}
void Data::storeBattle()
{
    battleINI->clear();
    for (size_t i=0; i<_battles.size(); i++) {
        Battle &b = _battles[i];
        char index[64];
        sprintf(index, "battle_%lu", i);
        string enemies, level;
        setMM(b.enemies_, enemies);
        setMM(b.level_, level);
        battleINI->setStringValue(index, "dName", b.dName.c_str());
        battleINI->setStringValue(index, "comment", b.comment.c_str());
        battleINI->setStringValue(index, "enemies", enemies.c_str());
        battleINI->setStringValue(index, "level", level.c_str());
        battleINI->setStringValue(index, "script", b.script.c_str());
    }
    battleINI->writeConfigFile();
}

void Data::getMM(int *arr, string &str)
{
    if (strcmp(str.c_str(), "none")==0) {
        for (int i=0; i<5; i++) {
            arr[i]=0;
        }
        return;
    }
    vector<string> en;
    split(str, " ", &en);
    //cout<<"["<<str<<"], value:";
    for (size_t i=0; i<en.size(); i++){
        arr[i]=atoi(en[i].c_str());
        //cout<<arr[i]<<",";
    }
    //cout<<endl;
}

void Data::setMM(int arr[], string &str)
{
    if (arr[0]==-1) {
        str="none";
        return;
    }
    char str2[128];
    sprintf(str2, "%d %d %d %d %d", arr[0], arr[1], arr[2], arr[3], arr[4]);
    str=str2;
}                                                                   

void Data::saveData()
{
    storeActor();
    storeNPC();
    storeItem();
    storeStory();
    storeMagic();
    storeMaps();
    storeBattle();
}

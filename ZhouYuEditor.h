#ifndef ZHOUYUEDITOR_H
#define ZHOUYUEDITOR_H

#include <QtWidgets/QMainWindow>
#include "Data.h"
#include <QtWidgets/QListWidget>
#include <QtWidgets/QComboBox>

namespace Ui {
class ZhouYuEditor;
}

class ZhouYuEditor : public QMainWindow
{
    Q_OBJECT

public:
    explicit ZhouYuEditor(QWidget *parent = 0);
    ~ZhouYuEditor();

public slots:
    void itemClicked(QListWidgetItem *);
    void actorListRowChanged(int row);
    void npcListRowChanged(int row);
    void itemListRowChanged(int row);
    void magicListRowChanged(int row);
    void mapListRowChanged(int row);
    void loadButtonClicked(bool);
    void actorOffsetRowChanged(int row);
    void chapterListRowChanged(int row);
    void segmentListRowChanged(int row);
    void storyListRowChanged(int);//{loadSubStory(false);}
    void battleListRowChanged(int row);//{loadBattle(false, false);}
    void arrayPropRowChanged(int row);
    void editingFinished0();
    void storeConfig();
    void editingFinished_noupdate();
    void saveAll();
    void addButton();
    void delButton();
    void upButton();
    void downButton();
    
    void addChapterButton();
    void delChapterButton();
    void upChapterButton();
    void downChapterButton();
    void addSegmentButton();
    void delSegmentButton();
    void upSegmentButton();
    void downSegmentButton();
    void addStoryButton();
    void delStoryButton();
    void upStoryButton();
    void downStoryButton();
    void updateActorValue();
    
    void getDir();
private:     
    Ui::ZhouYuEditor *ui;
    Data *_data;
    bool needSave;
    void setNeedSave(bool save);
    void connectButton();
    void loadActorData(bool loadList, size_t offsetIndex);
    void storeActorData();
    void loadNPCData(bool loadList);
    void storeNPCData();
    void loadTable();
    void loadItemData(bool loadList);
    void storeItemData();
    
    //for drama
    void loadStory();
    void loadChapter(bool loadList);
    void loadSegment(bool loadList);
    void loadSubStory(bool loadList);
    void storeStory();
    
    void loadMagic(bool loadList, bool defaultArray);
    void storeMagic();
    void loadMap(bool loadList);
    void storeMap();
    void loadBattle(bool loadList, bool loadActors);
    void storeBattle();
    void connectFinished();
    
    /*
    int actorRow;
    int NPCRow;
    int itemRow;
    int magicRow;
    int mapRow;
    int battleRow;
     */
    
    vector<QComboBox*> arrayComb;
    vector<QLineEdit*> arrayMin, arrayMax;
};

#endif // ZHOUYUEDITOR_H

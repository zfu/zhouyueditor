#-------------------------------------------------
#
# Project created by QtCreator 2014-09-02T14:07:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Editor
TEMPLATE = app


SOURCES += main.cpp\
    ConfigINI.cpp \
    Data.cpp \
    ZhouYuEditor.cpp

HEADERS  += \
    ConfigINI.h \
    Data.h \
    ZhouYuEditor.h

FORMS    += \
    ZhouYuEditor.ui
